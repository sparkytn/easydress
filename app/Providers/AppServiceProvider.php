<?php

namespace App\Providers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\StatusEvent;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;
use App\Services\BookingService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Carbon::setLocale(config('app.locale')); // fr

        view()->share('allCategories', Category::orderBy('name')->get() );
        view()->share('allbrands', Brand::orderBy('name')->get() );
        view()->share('allStatus', StatusEvent::orderBy('id')->get() );
        view()->share('booking_badge',app()->make(BookingService::class));


    }
}
