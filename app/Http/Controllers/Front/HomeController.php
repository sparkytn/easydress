<?php

namespace App\Http\Controllers\Front;

use App\Models\AttributeValues;
use App\Models\Product;
use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Category;

class HomeController extends FrontController
{
    public function __construct()
    {

    }

    public function index(){
        $booked = array();
        /* Check if the cart is empty or have alreadu items */
        $cart = session()->get('cart');

        // the cart has already items , get all bookings for that dates
        if ($cart != null && count($cart) > 0) {

            $start_date = session()->get('start_date');
            $end_date = session()->get('end_date');

            $booked = Booking::getBookedProducts($start_date,$end_date);
        }

        $all_sizes = AttributeValues::select('id', 'value')
            ->where('attribute_id',config('app.filters')['size'])
            ->orderBy('value')
            ->get();

        $products = Product::latest()
            ->with(['category','brand','images'])
            ->active()
            ->take(6)->get();
        return view('front.home',[
            'products' => $products,
            'booked' => $booked,
            'all_sizes' => $all_sizes
        ]);
    }


    public function filter(Request $request){
       // dd($request);
        $validated = $request->validate([
            'category' => 'nullable|numeric',
            'size' => 'nullable|numeric',
            'start_date'     => 'nullable'
        ]);

        $category = $validated['category'];
        $start_date = Carbon::parse($validated['start_date'])->format('Y-m-d H:i:s');
//
//        if($start_date){
//            session()->put('start_date', $start_date);
//        }

        $selected_category = null;
        if($category > 0){
            $selected_category = Category::findOrFail($category)->slug;
        }
        return redirect()->route('front.shop',['category'=>$selected_category,'filters[1]'=> $validated['size'], 'date'=> $validated['start_date']]);

    }
}
