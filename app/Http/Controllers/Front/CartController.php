<?php

namespace App\Http\Controllers\Front;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Models\Booking;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\DeliveryAddresses;
use App\Http\Controllers\Controller;
use App\Models\BookingItems;
use App\Notifications\BookingStatusNotification;

class CartController extends Controller
{

    public function show()
    {

//        $prods = Booking::getBookedProducts('2022-10-26','2022-11-15');
//        echo '<pre>';
//        foreach ($prods as $prod){
//            $p = $prod->products->toArray();
//            echo "<b> $prod->code </b> ---- ";
//            foreach ($prod->products as $p ){
//                echo $p['name'] . ' + ';
//            }
//            echo('=> ' . $prod->start_date .'  => '. $prod->end_date); echo '<br>';
//        }
        $cart = session()->get('cart') ?? array();
        $delivery_addresses = (auth()->user()) ? User::find(auth()->user()->id)->deliveryAddresses : null;
        return view(
            'front.cart',
            compact(['cart', 'delivery_addresses'])
        );

    }

    public function add(Request $request)
    {
        $cart = collect(session()->get('cart') ?? array()); // Get cart from session, if not exist create an array
        $cart = $cart->toArray();
        if ( count($cart) >= config('app.max_order') ) {
            return redirect()->route('front.showcart')->with(['card_full' => true]);
        }

        foreach ($cart as $cart_item) {
            if ($request->input_product_id == $cart_item['product_id']) {
                return redirect()->route('front.showcart');
            }
        }

        $selectedPeriod = $request->selectedPeriod;
        $start_date = $request->input_start_date;
        $end_date = $request->input_end_date;

        $product = Product::with(['images', 'category', 'brand'])->findOrFail($request->input_product_id);


        if (count($cart) == 0) { // If it is the first product , set the start date and end date in session
            session()->put('start_date', $start_date);
            session()->put('end_date', $end_date);
            session()->put('selected_period', $selectedPeriod);
        }
        $cart[] = [
            'product_id' => $request->input_product_id,
            'product_name' => $product->name,
            'image' => $product->images()->first()->file_name ?? NULL,
            'brand' => $product->brand->name,
            'category' => $product->category->name,
            'selected_period' => session()->get('selected_period'),
            'start_date' => session()->get('start_date'),
            'end_date' => session()->get('end_date'),
            'price' => $request->input_price,
            'deposit' => $product->value_price
        ];

        session()->put('cart', $cart);

        $total_amount = session()->get('total_amount') ?? 0;
        session()->put('total_amount', ($total_amount + $request->input_price));

        $total_deposit = session()->get('total_deposit') ?? 0;
        session()->put('total_deposit', ($total_deposit + $product->value_price));

        session()->put('total_items', count($cart));

        return redirect()->route('front.showcart');
    }


    public function delete(Request $request)
    {
        $cart = session()->get('cart');

        $cart_item_id = $request->cart_item_id;

        if (isset($cart[$cart_item_id])) {

            $rent_price = $cart[$cart_item_id]['price'];
            $value_price = $cart[$cart_item_id]['deposit'];

            unset($cart[$cart_item_id]);

            if (count($cart) == 0) {
                $this->cart_flush();
                return redirect()->route('front.shop');
            }

            $total_amount = session()->get('total_amount');
            session()->put('total_amount', ($total_amount - $rent_price));

            $total_deposit = session()->get('total_deposit');
            session()->put('total_deposit', ($total_deposit - $value_price));

            session()->put('total_items', count($cart));
            session()->put('cart', $cart);
        }

        return redirect()->route('front.showcart');
    }


    public function checkout(Request $request)
    {
        $cart = session()->get('cart');

        if ($cart == null)
            return redirect()->route('front.showcart');

        $delivery_addresses = User::find(auth()->user()->id)->deliveryAddresses;

        return view('front.checkout', compact('delivery_addresses'));
    }


    public function save_order(Request $request)
    {

        if (isset($request->delivery_address) && $request->delivery_address !== "" && $request->delivery_address !== "add_new_adr") {
            $validated = $request->validate(
                [
                    'delivery_address' => 'required|numeric'

                ]);
            $address_id = $request->delivery_address;
        } else {

            $validated = $request->validate(
                [
                    'contact_person' => 'required',
                    'contact_phone' => 'required',
                    'address_label' => 'required',
                    'street' => 'required',
                    'zip_code' => 'required',
                    'city' => 'required'
                ]);

            $validated['user_id'] = auth()->user()->id;

            $address = DeliveryAddresses::create($validated);

            $address_id = $address->id;
        }


        $cart = session()->get('cart');
            $bookedProducts = Booking::getBookedProducts(session()->get('start_date'),session()->get('end_date'));
        $product_not_available = array();

        foreach ($cart as $key => $item) {
            if( in_array($item['product_id'], $bookedProducts)){
                $product_not_available[] = $item ;
                if (isset($cart[$key])) {

                    $rent_price = $item['price'];
                    $value_price = $item['deposit'];

                    unset($cart[$key]);


                    $total_amount = session()->get('total_amount');
                    session()->put('total_amount', ($total_amount - $rent_price));

                    $total_deposit = session()->get('total_deposit');
                    session()->put('total_deposit', ($total_deposit - $value_price));

                    session()->put('total_items', count($cart));
                    session()->put('cart', $cart);
                }
            }
        }

        if( count($product_not_available) !== 0){
            if(count($cart)=== 0){
                $this->cart_flush();
            }
            return redirect('/panier')->with('product_not_available', count($product_not_available));
        }


        $booking_code = $this->generateBookingCode( Str::upper(Str::random(5) . auth()->user()->id) );
        $booking = Booking::create([
            'code' => $booking_code,
            'user_id' => auth()->user()->id,
            'delivery_address_id' => $address_id,
            'status_event_id' => 1,
            'start_date' => new \DateTime(session()->get('start_date')),
            'end_date' => new \DateTime(session()->get('end_date')),
            'price' => session()->get('total_amount'),
            'deposit' => session()->get('total_deposit'),
        ]);

        foreach ($cart as $item) {
            BookingItems::create([
                'booking_id' => $booking->id,
                'product_id' => $item['product_id'],
                'price' => $item['price'],
                'deposit' => $item['deposit'],
            ]);
        }
        $this->cart_flush();
        session()->put('success_commande', $booking->code);
        $booking->user->notify(new BookingStatusNotification($booking));
        return redirect()->route('front.account.order.history')->with(['success_order'=> $booking]);
    }

    public function cart_flush()
    {
        session()->forget(['cart', 'start_date', 'end_date', 'total_amount', 'total_deposit', 'total_items', 'selected_period']);
    }

    private function generateBookingCode($booking_code)
    {
        $bookings = Booking::where('code', $booking_code)->first();

        if($bookings){

            $booking_code = Str::upper(Str::random(5) . auth()->user()->id);
            return $this->generateBookingCode($booking_code);

        }

        return $booking_code;
    }
}
