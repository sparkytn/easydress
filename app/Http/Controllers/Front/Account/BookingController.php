<?php

namespace App\Http\Controllers\Front\Account;

use App\Models\User;
use App\Models\Booking;
use App\Models\Product;
use Illuminate\Support\Str;
use MongoDB\Driver\Session;
use Illuminate\Http\Request;
use App\Models\DeliveryAddresses;
use App\Http\Controllers\Controller;
use App\Notifications\BookingStatusNotification;

class BookingController extends Controller
{

    public function checkout(Request $request)
    {
        //        dd($request,session());
        //        session(['selectedPeriod' => $request->selectedPeriod]);
        //        session(['start_date' => $request->start_date]);
        //        session(['end_date' => $request->end_date]);
        //        session(['price' => $request->price]);

        $selectedPeriod = $request->selectedPeriod;
        $start_date = $request->input_start_date;
        $end_date = $request->input_end_date;
        $price = $request->input_price;
        $product = Product::with(['images', 'category', 'brand'])->findOrFail($request->input_product_id);

        $delivery_addresses = User::find(auth()->user()->id)->deliveryAddresses;

        //redirect('front.account.booking');'


        return view(
            'front.account.checkout',
            compact(['selectedPeriod', 'start_date', 'end_date', 'price', 'product', 'delivery_addresses'])
        );
    }

    public function save(Request $request)
    {
        if ($request->has('delivery_address') && $request->delivery_address != "") {
            $address_id = $request->delivery_address;
        } else {
            $address = DeliveryAddresses::create(
                [
                    'user_id' => auth()->user()->id,
                    'contact_person' => $request->contact_person,
                    'contact_phone' => $request->contact_phone,
                    'address_label' => $request->address_label,
                    'street' => $request->address,
                    'zip_code' => $request->zip_code,
                    'city' => $request->city
                ]);

            $address_id = $address->id;
        }


        $booking = Booking::create(
            [
                'code' => Str::upper(Str::random(5)) . auth()->user()->id,
                'user_id' => auth()->user()->id,
                'delivery_address_id' => $address_id,
                'status_event_id' => 1,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
                'price' => $request->price,
                'deposit' => $request->deposit,
                'payment_note' => 'Commande Créé le ' . date('d/m/Y H:i:s'),
            ]);


        //Customer notification
        $notification = $booking->generateStatusNotification();
        $user = $booking->user;
        $user->notify(new BookingStatusNotification($booking));
        session()->put('success_commande', $booking->code);
        return redirect()->route('front.account.order.history');
    }
}
