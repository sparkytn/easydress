<?php

namespace App\Http\Controllers\Front\Account;

use Illuminate\Http\Request;
use App\Models\DeliveryAddresses;
use App\Http\Controllers\Controller;
use App\Http\Requests\Front\AddressRequest;
use Illuminate\Support\Facades\Validator;


class AdressesController extends Controller
{
    public function index()
    {
        $adresses = DeliveryAddresses::where('user_id', auth()->user()->id)->get();
        $user = auth()->user();
        return view('front.account.adresses')->with('user',$user)->with('adresses',$adresses);
    }

    public function add(){

        return view('front.account.address-form')->with('user',auth()->user())
                                                    ->with('action','store');
    }

    public function store(AddressRequest $request){

        $validator = Validator::make($request->all(),$request->rules());

        if($validator->fails()){

            return back()
                        ->withErrors($validator)
                        ->withInput()->back();

        }

        $validated = $validator->validated();

        $validated['user_id'] = auth()->user()->id;

        DeliveryAddresses::create($validated);


        return redirect()->route('front.account.addresses');
    }

    public function edit($id){

        $address = DeliveryAddresses::where('user_id', auth()->user()->id)->findOrFail($id);

        return view('front.account.address-form', compact('address'))->with('user',auth()->user())
                                                                        ->with('action','update');

    }

    public function update(AddressRequest $request)
    {
        $validator = Validator::make($request->all(),$request->rules());

        if($validator->fails()){

            return back()
                        ->withErrors($validator)
                        ->withInput()->back();

        }

        $validated = $validator->validated();

        $adr = DeliveryAddresses::findOrFail($request->input('id'));
        $adr->update($validated);



        return redirect()->route('front.account.addresses');
    }

    public function destroy($id)
    {
        $address = DeliveryAddresses::where('user_id', auth()->user()->id)->findOrFail($id);

        $address->delete();
        return redirect()->route('front.account.addresses')->with('success', 'L\'adresse a été supprimée!');
    }
}
