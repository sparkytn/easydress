<?php

namespace App\Http\Controllers\Front\Account;

use App\Models\Product;
use Illuminate\Http\Request;


use App\Models\SubmitedProduct;
use App\Models\Commission;
use App\Http\Controllers\Front\FrontController;

class MyShopController extends FrontController
{
    // List all submitted products

    public function index(){

        $submittedProducts = SubmitedProduct::where('user_id','=',auth()->user()->id)
                                        ->where('migrated', '==',0)
                                        ->orderBy('id','DESC')
                                        ->with('category','brand','shop_product')
                                        ->get();

        $affectedProducts = Product::where('user_id','=',auth()->user()->id)
            ->orderBy('id','DESC')
            ->with('category','brand')
            ->get();
        $my_products = $submittedProducts->merge($affectedProducts);
        return view('front.account.my-shop',compact('my_products'));
    }

    public function add_product(){

        return view('front.account.shop');

    }

    public function store_product(Request $request){
//dd($request);
    }

    public function delete_product($id){

        SubmitedProduct::where('id','=',$id)->where('user_id','=',auth()->user()->id)->delete();

        return back();
    }

    public function my_commissions()
    {
        $collection = Commission::query();

        $collection->with('user', 'booking')
            ->orderBy('created_at')
            ->where('user_id','=',auth()->user()->id);

        $commissions = $collection->get();

        return view('front.account.commissions',compact('commissions'));

    }
}
