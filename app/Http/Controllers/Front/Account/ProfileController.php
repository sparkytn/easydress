<?php

namespace App\Http\Controllers\Front\Account;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{


    public function index()
    {
        $user = auth()->user();
        return view('front.account.profile')->with('user',$user);
    }

    public function update(UserRequest $reuqest)
    {
        $user = User::find(auth()->user()->id);
        $user->update( $reuqest->validated() );

        return view('front.account.profile')
            ->with('user',$user)
            ->with('notification_success','Enregistré avec succès');
    }
}
