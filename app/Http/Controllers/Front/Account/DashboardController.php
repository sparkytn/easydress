<?php

namespace App\Http\Controllers\Front\Account;

use App\Http\Controllers\Front\FrontController;
use App\Models\Booking;

class DashboardController extends FrontController
{
    public function __construct()
    {

    }


    public function index(){
        return view('front.account.dashboard');
    }

    public function profile()
    {
        return view('front.account.profile');
    }

    public function adresses()
    {
        return view('front.account.adresses');
    }

    public function shop()
    {
        return view('front.account.shop');
    }

    public function paymentMethods()
    {
        return view('front.account.payment-methods');
    }

    public function orderHistory()
    {
        $orders = collect(Booking::with('products','currentStatus','deliveryAddress')
            ->where('user_id', auth()->user()->id)
            ->orderBy('id','DESC')
            ->get())->groupBy('status_event_id')->sortKeys();

        return view('front.account.order-history',compact('orders'));
    }
}
