<?php

namespace App\Http\Controllers\Front\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function index()
    {
        //
    }

    public function chekout(Request $request)
    {
        session(['selectedPeriod' => $request->selectedPeriod]);
        session(['start_date' => $request->start_date]);
        session(['end_date' => $request->end_date]);
        session(['price' => $request->price]);
        redirect('front.account.booking');
    }
}
