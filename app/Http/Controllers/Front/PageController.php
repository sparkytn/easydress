<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller
{
    //

    public function leConcept()
    {
        return view('front.le-concept');
    }
    public function faq()
    {
        return view('front.faq');
    }
    public function politique()
    {
        return view('front.politique');
    }
}
