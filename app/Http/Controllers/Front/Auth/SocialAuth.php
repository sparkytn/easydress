<?php

namespace App\Http\Controllers\Front\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class SocialAuth extends Controller
{

    public function redirect($driver)
    {
        return Socialite::driver($driver)->redirect();
    }

    public function callback($driver)
    {

        $driverID = $driver . '_id';

        try {
            $socialUser = Socialite::driver($driver)->user();
        } catch (\Exception $e) {
            return redirect()->route('login');
        }

        $user = User::where('email', $socialUser->email )->first();
        if( $user ) {

            if( $user->$driverID == null ){
                $user->$driverID = $socialUser->id;
                $user->save();
            }

            Auth::login($user,true);

        } else {

            $newUser = User::create([
                'name'        => $socialUser->name,
                'email'       => $socialUser->email,
                'password'    => 'not_set_yet',
                 $driverID    => $socialUser->id
            ]);

            Auth::login($newUser, true);

        }

        return redirect(RouteServiceProvider::HOME);

    }

    public function setPassword(Request $request)
    {
        $request->validate([
            'password' => 'required|string|confirmed|min:8',
        ]);



        if( ! auth()->user() ){
            return redirect()->back();
        }

        $user = User::where('id', auth()->user()->id)->first();

        if( $user && ($user->password === 'not_set_yet' || $user->password === null || $user->password === '' ) ){

            $user->password = Hash::make($request->password);
            $user->save();


            return redirect()
                        ->back()
                        ->with('password.change.success', "Votre mot de passe est enregistré avec succès");

        }

        return redirect()->back();
    }
}
