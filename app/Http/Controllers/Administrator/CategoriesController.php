<?php

namespace App\Http\Controllers\Administrator;

use App\Models\Category;
use App\Models\Attribute;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('administrator.categories.list',['categories'=>Category::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $attributes = Attribute::all();

        return view('administrator.categories.add',compact('attributes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required'
        ]);

        $validated['slug'] = Str::slug($validated['name']);

        $category = Category::create($validated);

        $category->attributes()->sync($request->input('attributes',[]));

        return redirect()->route('admin.categories.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $$category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $attributes = Attribute::get()->map(
                        function($attribute) use ($category) {
                            $attribute->val = data_get($category->attributes->firstWhere('id', $attribute->id),'pivot.attribute_id', null);
                            return $attribute;
                        }
                    );
        return view('administrator.categories.edit',compact('category','attributes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $validated = $request->validate([
            'name' => 'required'
        ]);

        $validated['slug'] = Str::slug($validated['name']);

        $category->update($validated);

        $category->attributes()->sync($request->input('attributes',[]));

        return redirect()->route('admin.categories.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {

        $category->delete();

        return redirect()->route('admin.categories.index');
    }
}
