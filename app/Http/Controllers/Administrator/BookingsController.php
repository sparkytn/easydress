<?php

namespace App\Http\Controllers\Administrator;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Booking;
use App\Models\Product;
use App\Models\Commission;
use App\Models\StatusEvent;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\BookingStatusEvent;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Notifications\BookingStatusNotification;
use Database\Seeders\StatusEventSeeder;

class BookingsController extends AdminController
{
    public function index(Request $request)
    {
        $filterCode = $request->input('code');
        $filterProduct = $request->input('product_name');
        $filterUser = $request->input('user');
        $filterStatus = $request->input('status');
        $filterSdate = ($request->input('sdate') != null) ? Carbon::createFromFormat(
            'd/m/Y',
            $request->input('sdate')
        )->format('Y-m-d') : null;

        $collection = Booking::query();

        $collection->with([
            'products',
            'user',
            'currentStatus'
        ]);

        ////// /* Filter by code */
        $collection->when($filterCode, function ($collection, $filterCode) {
            return $collection->whereRaw("UPPER(code) LIKE '%". strtoupper($filterCode)."%'");
        });

        ////// /* Filter by product name */
        $collection->when($filterProduct, function ($collection, $filterProduct) {
            return $collection->where(
                function ($query) use ($filterProduct) {
                    $query->whereHas('products', function ($query) use ($filterProduct) {
                        $query->whereRaw("UPPER(name) LIKE '%". strtoupper($filterProduct)."%'");
                    });
                }

            );
        });

        ////// /* Filter by user */
        $collection->when($filterUser, function ($collection, $filterUser) {
            return $collection->where('user_id', '=', $filterUser);
        });

        ////// /* Filter by status */
        $collection->when($filterStatus, function ($collection, $filterStatus) {
            return $collection->where('status_event_id', '=', $filterStatus);
        });

        ////// /* Filter by start date */
        $collection->when($filterSdate, function ($collection, $filterSdate) {
            return $collection->where('start_date', '=', $filterSdate);
        });

        $bookings = $collection->orderBy('id')->paginate(2);

        $users = User::orderBy('name')->get();
        $status = StatusEvent::orderBy('id')->get();

        return view('administrator.bookings.list', compact('bookings', 'users', 'status'));
    }


    public function details($id)
    {
        $booking = Booking::with([
            'user',
            'deliveryAddress',
            'currentStatus',
            'status' => function ($query) {
                return $query->orderBy('created_at', 'DESC');
            }
        ])->findOrFail($id);

        $status = @$booking->currentStatus ?? '';

        $status_name = $status->name;
        $badge_class = '';


        $old_status = $booking->status;
        $archived = false;
        if($status->id === StatusEvent::STATUSES['canceled'] || $status->id === StatusEvent::STATUSES['ended']){
            $archived = true;
        }

        $status_events = StatusEvent::where('id', '>', $status->id)->get();
        return view('administrator.bookings.details', compact('booking', 'badge_class', 'status_name', 'status_events', 'old_status', 'archived'));
    }


    public function change_status(Request $request)
    {

        $validated = $request->validate([
            'status_event_id' => 'required',
            'booking_id' => 'required'
        ]);

        BookingStatusEvent::create($request->all());

        $booking = Booking::with('user', 'items', 'products')->findOrFail($request->input('booking_id'));



        //Customer notification
        //$notification = $booking->generateStatusNotification();
        $user = $booking->user;
        $user->notify(new BookingStatusNotification($booking));

        return redirect()->route('admin.bookings.details', $request->input('booking_id'));
    }


    public function payment_recieved(Request $request)
    {

        $validated = $request->validateWithBag('payment', [
            'payment_note' => 'required',
            'booking_id' => 'required'
        ]);

        $booking = Booking::findOrFail($request->input('booking_id'));
        $booking->payment_recieved = 1;
        $payment_date = Carbon::now()->format('d/m/Y H:i');
        $booking->payment_note = 'Date du paiement : ' . $payment_date . " <br> <b>Commentaire : </b>" . $request->input('payment_note');

        $booking->status_event_id = StatusEvent::STATUSES['confirmed'];

        $booking->update();

        $user = $booking->user;
        $user->notify(new BookingStatusNotification($booking));

        /* Add commission to the owner */
        foreach ($booking->items as $item) {
            $product = Product::findOrFail($item->product_id);
            $owner = User::findOrFail($product->user_id);

            $amount = round(($item->price * config('app.commission')), 1 );

            $commission = Commission::create([
                'user_id' => $owner->id,
                'booking_id' => $booking->id,
                'status' => "1",
                'amount' => $amount
            ]);
        }
        return redirect()->route('admin.bookings.details', $booking);
    }

    public function deposit_refunded(Request $request)
    {

        $booking = Booking::findOrFail($request->input('booking_id'));
        $booking->deposit_refunded = 1;
        $booking->update();

        return redirect()->route('admin.bookings.details', $booking);
    }
}
