<?php

namespace App\Http\Controllers\Administrator;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Commission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\CommissionPaidNotification;

class CommissionsController extends AdminController
{
    public function index(Request $request)
    {
        $filterUser = $request->input('user');
        $filterBooking = $request->input('booking');
        $filterStatus = $request->input('status');
//dd(isset($filterAccepted));
        $collection = Commission::query();

        $collection->with('user', 'booking')
            ->orderBy('created_at','DESC');
        

        $collection->when($filterUser, function ($collection, $filterUser) {
            return $collection->where('user_id', '=',  $filterUser);
        });

        $collection->when($filterBooking, function ($collection, $filterBooking) {
            return $collection->where(function ($query) use ($filterBooking) {
                $query->whereHas('booking', function ($query) use ($filterBooking) {
                    $query->where('code', 'like', '%' . $filterBooking . '%');
                });
            });
        });

        

        if(isset($filterStatus))
            $collection->where('status', '=',  $filterStatus);
        

        $commissions = $collection->paginate(25);

        
        $users = User::get();

        return view('administrator.commissions.list', compact('commissions', 'users'));
    }

    public function pay($commission_id)
    {
        $commission = Commission::with('user')->findOrFail($commission_id);

        $commission->payment_date = Carbon::now();
        $commission->status = 2;
//dd($commission);
        $commission->update();

        // Notify user
        $user = $commission->user;
        $user->notify(new CommissionPaidNotification($commission));

        return back();
    }

}
