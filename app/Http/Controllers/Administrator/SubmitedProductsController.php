<?php

namespace App\Http\Controllers\Administrator;

use App\Models\User;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\SubmitedProduct;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class SubmitedProductsController extends AdminController
{
    public function index(Request $request)
    {
        $filterProduct = $request->input('product_name');
        $filterUser = $request->input('user');
        $filterCategory = $request->input('category');
        $filterAccepted = $request->input('accepted');
//dd(isset($filterAccepted));
        $collection = SubmitedProduct::query();

        $collection->with('user', 'brand', 'category')
            ->orderBy('created_at')
            ->where('is_accepted', '!=', 2);

        $collection->when($filterProduct, function ($collection, $filterProduct) {
            return $collection->where('name', 'like', '%' . $filterProduct . '%');
        });

        $collection->when($filterUser, function ($collection, $filterUser) {
            return $collection->where('user_id', '=',  $filterUser);
        });

        $collection->when($filterCategory, function ($collection, $filterCategory) {
            return $collection->where('category_id', '=',  $filterCategory);
        });

        if(isset($filterAccepted))
            $collection->where('is_accepted', '=',  $filterAccepted);


        $products = $collection->paginate(25);

        $categories = Category::get();
        $users = User::get();

        return view('administrator.products.submitedlist', compact('products', 'categories', 'users'));
    }

    public function show($id)
    {

        $product = SubmitedProduct::with('attributes_values', 'user', 'brand', 'category')->findOrFail($id);
        return view('administrator.products.submitedshow', compact('product'));
    }

    public function accept(Request $request)
    {

        $product = SubmitedProduct::findOrFail($request->input('id'));

        if ($request->input('accept')) {
            $product->is_accepted = 1;
        }

        if ($request->input('archive')) {
            $product->is_accepted = 2;
        }

        $product->update();

        return redirect()->back()->withInput();
    }

    public function migrate(Request $request)
    {

        $submited_product = SubmitedProduct::with('attributes_values')->findOrFail($request->input('id'));

        $data['name'] = $submited_product->name;
        $data['slug'] = Str::slug($submited_product->name);
        $data['category'] = $submited_product->category_id;
        $data['user'] = $submited_product->user_id;
        $data['brand'] = $submited_product->brand_id;
        $data['rent_price'] = $submited_product->rent_price;
        $data['value_price'] = $submited_product->value_price;
        $data['submited_id'] = $submited_product->id;

        foreach ($submited_product->attributes_values as $attribute_value) {
            // dd($attribute_value);
            $data['attribute_' . $attribute_value->attribute_id] = $attribute_value->pivot->attribute_value_id;
        }
        $request->flash();
        $request->session()->put('_old_input', $data);
        //$request->flash();
        return redirect()->route('admin.products.create_new');
    }
}
