<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class CustomersController extends AdminController
{
    public function index(Request $request){

        $filterName = $request->input('name');
        $filterEmail = $request->input('email');

        $collection = User::query();

        $collection->withCount([
            'products',
            'bookings'
        ]);

        ////// /* Filter by name */
        $collection->when($filterName, function ($collection, $filterName) {
            return $collection->where('name','like','%'.$filterName.'%');
        });

        ////// /* Filter by email */
        $collection->when($filterEmail, function ($collection, $filterEmail) {
            return $collection->where('email','like','%'.$filterEmail.'%');
        });

        $customers = $collection->orderBy('created_at','ASC')->paginate(25);

        return view('administrator.customers.list', compact('customers'));
    }


    public function details($id){

        $customer = User::with([
            'products',
            'bookings',
            'deliveryAddresses'
            ])->findOrFail($id);

        $bookings   = $customer->bookings;
        $products   = $customer->products;
        $addresses  = $customer->deliveryAddresses;

        return view('administrator.customers.details',compact('customer','bookings','products','addresses'));
    }
}
