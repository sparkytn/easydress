<?php

namespace App\Http\Controllers\Administrator;


use App\Models\User;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Category;
use App\Models\ProductImgs;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Services\ProductService;

use Illuminate\Support\Facades\File;
use App\Models\ProductsAttributesValues;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\Administrator\StoreProductRequest;
use App\Models\SubmitedProduct;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Image;


class ProductsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {

        $categories = Category::all();
        $users = User::all();

        $products = Product::with(['brand','category','user','images'])
            ->orderByDesc('created_at')
            ->paginate(25);

        return view('administrator.products.list', compact('products','users','categories'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_new(Request $request)
    {
        try{
            switch($request->method()){
                case 'POST': // add product from product listing

                    $validated = $request->validate([
                        'category' => 'required|numeric',
                        'user'     => 'required|numeric'
                    ]);
                    $category = Category::findOrFail($validated['category']);
                    $selected_category = $category->id;
                    $selected_user = User::findOrFail($validated['user'])->id;

                    break;

                case 'GET': // validation redirect

                    $selected_category = old('category');
                    $selected_user = old('user');
                    //dd(old('brand'));
                    $category = Category::findOrFail($selected_category);
//dd($category);

                    break;
                default:
                    return redirect()->route('admin.products.index');
            }

        }catch(ModelNotFoundException $err){
            return  redirect()->route('admin.products.index');
        }

        $attributes_fields =[];

        $brands = Brand::get();

        foreach($category->attributes as $attribute){
            $attributes_fields[] = [
                'id'=>$attribute->id,
                'label'=>$attribute->name,
                'values' => $attribute->values
            ];
        }

        return view('administrator.products.add',compact('selected_user','selected_category','brands','attributes_fields'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {

         $validator = Validator::make($request->all(),$request->rules());

        if($validator->fails()){

            return redirect('admin.products.create_new')
                        ->withErrors($validator)
                        ->withInput();

        }

        $validated = $validator->validated();

        $validated['user_id'] = $request->input('user');
        $validated['brand_id'] = $request->input('brand');
        $validated['category_id'] = $request->input('category');
        $validated['slug'] = Str::slug($validated['slug']);
        $validated['is_online'] = $request->is_online ?? 0;

        $product = Product::create($validated);

        foreach($request->required_attributes as $attribute){
            ProductsAttributesValues::create([
                'product_id'=> $product->id,
                'attribute_value_id'=> $request->input('attribute_'.$attribute->id)
            ] );
        }

        /* If the product is a migration update submited_products line set migrated = 1 for the
        migrated product
        */
        if($request->input('submited_id')){

            $submited_product= SubmitedProduct::findOrFail($request->input('submited_id'));

            $submited_product->migrated = $product->id;
            $submited_product->update();

        }

        return redirect()->route('admin.products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {

        return view('administrator.products.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $old_attributes = [];
        foreach($product->attributes_values as $value){

            $old_attributes[$value->attribute->id] = $value->id;
        }

        $attributes_fields=[];

        foreach($product->category->attributes as $attribute){
            $attributes_fields[] = [
                'id'=>$attribute->id,
                'label'=>$attribute->name,
                'values' => $attribute->values
            ];
        }

        $brands = Brand::get();

        return view('administrator.products.edit',compact('product','attributes_fields','old_attributes','brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProductRequest $request, Product $product)
    {

        $validator = Validator::make($request->all(),$request->rules());

        if($validator->fails()){

            return back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $validated = $validator->validated();
        $validated['brand_id'] = $request->input('brand');
        $validated['is_online'] = $request->is_online ?? 0;


        $new_attributes_values=[];

        foreach($request->required_attributes as $attribute){

                $new_attributes_values[] = $request->input('attribute_'.$attribute->id);

        }


        $product->update($validated);

        $product->attributes_values()->sync($new_attributes_values);

        return redirect()->route('admin.products.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();

        return redirect()->route('admin.products.index');
    }


    public function load_gallery(Product $product){

        $product = Product::findOrFail($product->id);

        $images = $product->images()->orderBy('order')->get();

        $gallery = view('administrator.products.product_images_gallery_response',compact('images'))->render();

        return response()->json([
            'gallery'=>$gallery,
            'imgs_count'=>$images->count()
        ]);

    }


    public function upload_images(Request $request){

        $request->validate([
            'image' => 'required|image|mimes:jpeg,jpg|max:2048|dimensions:min_width=600,min_height=900',
            'product_id' => 'required|numeric'
        ]);

        $img_data=[];

        $product = Product::findOrFail( $request->input('product_id'));

        $img_data['product_id'] = $product->id;
        $img_data['order'] = $product->images()->max('order')+1;

        $image_file_name = $product->id.'_'.md5(time()).'.'.$request->image->extension();
        $img_data['file_name'] = $image_file_name;

        $product_card_thumb = 'card_thumb_'.$image_file_name;
        $gallery_thumb = 'gallery_thumb_'.$image_file_name;
        $gallery = 'gallery_'.$image_file_name;

//        $request->image->move(public_path('imgs'), $img_data['file_name']);
        $request->file('image')->storeAs('imgs', $image_file_name, 'public');
        $request->file('image')->storeAs('imgs', $product_card_thumb, 'public');
        $request->file('image')->storeAs('imgs', $gallery_thumb, 'public');
        $request->file('image')->storeAs('imgs', $gallery, 'public');

        $product_card_thumb_path = public_path('storage/imgs/'.$product_card_thumb);
        $this->createThumbnail($product_card_thumb_path, 300, 450);

        $gallery_thumb_path = public_path('storage/imgs/'.$gallery_thumb);
        $this->createThumbnail($gallery_thumb_path, 100, 150);

        $gallery_path = public_path('storage/imgs/'.$gallery);
        $this->createThumbnail($gallery_path, 600, 900);

        ProductImgs::create($img_data);


    }

    public function delete_image($image_id,$product_id){

        $image = ProductImgs::findOrFail($image_id);
        if($image->product_id != $product_id)
            return false;

        $image_path = app_path('storage/imgs').'/'.$image->file_name;

        $image->decrementOrderFrom($image->order);

        Storage::disk('public')->delete('imgs/'.$image->file_name);
        Storage::disk('public')->delete('imgs/gallery_thumb_'.$image->file_name);
        Storage::disk('public')->delete('imgs/gallery_'.$image->file_name);
        Storage::disk('public')->delete('imgs/card_thumb_'.$image->file_name);


        $image->delete();
        return true;
    }

    public function reorder_images($order_string){
        $order_array = explode(',',str_replace('sortable_','',$order_string));
        $product_imgs = new ProductImgs;
        $product_imgs->reorder($order_array);

    }


    /**
     * Create a thumbnail of specified size
     *
     * @param string $path path of thumbnail
     * @param int $width
     * @param int $height
     */
    public function createThumbnail($path, $width, $height)
    {
        $img = Image::make($path)->fit($width, $height, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $img->save($path);
    }
}
