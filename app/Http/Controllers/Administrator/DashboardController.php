<?php

namespace App\Http\Controllers\Administrator;

use App\Models\StatusEvent;
use Carbon\Carbon;
use App\Models\Booking;
use App\Models\Commission;
use Illuminate\Http\Request;
use App\Models\SubmitedProduct;
use App\Services\BookingService;
use App\Services\ProductService;

class DashboardController extends AdminController
{
    public function __construct()
    {
        /*
         * Uncomment the line below if you want to use verified middleware
         */
        //$this->middleware('verified:administrator.verification.notice');
    }


    public function index()
    {
        $stats['new_bookings'] = Booking::whereIn('status_event_id', ['', 1])
            ->get()
            ->count();

        $stats['tobe_delivered'] = Booking::getDeliveryByPeriod(Carbon::now()->addDay(), false)->count();

        $stats['tobe_returned'] = Booking::getReturnsByPeriod(Carbon::now(), false)->count();

        $stats['tobe_refund'] = Booking::where('status_event_id', '=', '5')
            ->where('payment_recieved', '=', '1')
            ->where('deposit_refunded', '=', '0')
            ->get()
            ->count();

        $stats['commissions'] = Commission::where('status', '=', '1')
            ->get()
            ->count();

        $last_bookings = Booking::orderBy('start_date')
            ->with('user', 'items')
            ->where('status_event_id', StatusEvent::STATUSES['pending'])
            ->take(10)
            ->get();

        $last_submited = SubmitedProduct::orderBy('id', 'DESC')
            ->with('user')
            ->where('migrated','=',0)
            ->take(5)
            ->get();

        return view('administrator.dashboard', compact('stats', 'last_bookings', 'last_submited'));
    }

    public function book()
    {
        /*$booking = Booking::with([
            'product',
            'user',
            'status' => function ($query) {
                $query->orderBy('id', 'DESC');
            }
        ])->paginate(5);

        return view('administrator.bookings.list', compact('booking'));*/
        return 'heyy';
    }

    public function calendarFill(Request $request)
    {
        app('debugbar')->disable();
        $from = Carbon::parse($request->input('start'))->translatedFormat('Y-m-d');
        $to = Carbon::parse($request->input('end'))->translatedFormat('Y-m-d');

        $bookings = Booking::getBookingsInRange($from, $to, StatusEvent::STATUSES['confirmed'], '>=');


        $events = array();

        foreach ($bookings as $booking) {
            $title = 'Article ('.count($booking->products).') :';

            foreach ($booking->products as $product) {
                $title .= $product->ref . ' ';

            }
            $events[] = [
                'title' => $title,
                'start' => $booking->start_date->format('Y-m-d'),
                'end' => $booking->end_date->addDay()->format('Y-m-d'),
                'code' => '#'.$booking->code.' - '.$booking->user->name .' #'.$booking->user->id.' ',
                'url' => route('admin.bookings.details', $booking),
                'status' => 'Etat : '.$booking->currentStatus->name,
                'price' => 'Prix : '.$booking->price . ' - Caution : '.$booking->deposit,
                'color' => StatusEvent::STATUSES_COLORS[$booking->status_event_id - 1],
            ];
        }


        return collect($events)->toJson();
//        return response()->json(collect($events));
    }

    public function calendarFill2(Request $request)
    {
        app('debugbar')->disable();
        $from = Carbon::parse($request->input('start'))->translatedFormat('Y-m-d');
        $to = Carbon::parse($request->input('end'))->translatedFormat('Y-m-d');


        $pendingP = Booking::getBookingsInRange($from, $to, StatusEvent::STATUSES['pending'],'=');

        $events = array();

        foreach ($pendingP as $pending) {
            $title = '#'.$pending->code.' — ';
            foreach ($pending->items as $item) {
                $title .= ProductService::generateRef($item->product);
            }
            $events[] = [
                'title' => $title,
                'start' => $pending->start_date->format('Y-m-d'),
                'end' => $pending->end_date->addDay()->format('Y-m-d'),
            ];
        }



        //return collect(($events))->toJson();
    }
}
