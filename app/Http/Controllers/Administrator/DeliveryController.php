<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PDF;

class DeliveryController extends AdminController
{
    public function delivery(Request $request)
    {
        $today = Carbon::now();
        $selected_date = $request->input('date') ?? Carbon::now()->translatedFormat('Y-m-d');
        $dates = array();

        for ($i = 0; $i < 3; $i++) {
            if ($i != 0) {
                $the_date = $today->addDays(1);
            } else {
                $the_date = $today;
            }
            $selected = ($the_date->translatedFormat('Y-m-d') == $selected_date) ?? FALSE;
            $dates[$i] = [
                'label' => $the_date->translatedFormat('d/m/Y'),
                'value' => $the_date->translatedFormat('Y-m-d'),
                'selected' => $selected
            ];
        }
//dd($dates);
        $deliveries = Booking::getDeliveryByPeriod($selected_date);

        return view('administrator.deliveries.list', compact('deliveries', 'dates','selected_date'));
    }

    public function export_list(Request $request)
    {
        app('debugbar')->disable();

        $selected_date = $request->input('date') ?? Carbon::now();

        $data = array();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPdf()->set_option("enable_php", true);

        $deliveries = Booking::getDeliveryByPeriod($selected_date);
        $data = ['deliveries' => $deliveries, 'pdf' => $pdf, 'selected_date'=>$selected_date];
        view()->share('deliveries', $deliveries);
        //return view('administrator.deliveries.list_pdf');
        $pdf->loadView('administrator.deliveries.list_pdf', $data);


        // download PDF file with download method
        return $pdf->stream('liste_expeditions_' . $selected_date . '.pdf');
    }

    public function export_invoices(Request  $request)
    {
        app('debugbar')->disable();

        $booking = $request->input('booking') ?? false;

        $selected_date = $request->input('date') ?? Carbon::now()->toDateString();

        $bookings = Booking::getDeliveryByPeriod($selected_date, false, $booking);

        view()->share('bookings', $bookings);

        $pdf = PDF::loadView('administrator.deliveries.invoice_pdf', $bookings);


        if ($booking) {
            $code = $bookings->first()->code;
            $filename = "invoice_" . $code . ".pdf";
        } else {
            $filename = "invoices_" . $selected_date . ".pdf";
        }

        return $pdf->stream($filename);

        // return view('administrator.deliveries.invoice_pdf',compact('booking'));

    }

    public function returns(Request $request)
    {
        $today = Carbon::now();
        $selected_date = $request->input('date') ?? Carbon::now()->translatedFormat('Y-m-d');
        $dates = array();

        for ($i = 0; $i < 3; $i++) {
            if ($i != 0) {
                $the_date = $today->addDays(1);
            } else {
                $the_date = $today;
            }
            $selected = ($the_date->translatedFormat('Y-m-d') == $selected_date) ?? FALSE;
            $dates[$i] = [
                'label' => $the_date->translatedFormat('d/m/Y'),
                'value' => $the_date->translatedFormat('Y-m-d'),
                'selected' => $selected
            ];
        }
//dd($dates);
        $returns = Booking::getReturnsByPeriod($selected_date,false);

        return view('administrator.deliveries.returns_list', compact('returns', 'dates','selected_date'));
    }

    public function export_returns_list(Request $request)
    {
        app('debugbar')->disable();

        $selected_date = $request->input('date') ?? Carbon::now();

        $data = array();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->getDomPdf()->set_option("enable_php", true);

        $returns = Booking::getReturnsByPeriod($selected_date,false);
        $data = ['returns' => $returns, 'pdf' => $pdf, 'selected_date'=>$selected_date];
        view()->share('returns', $returns);
        //return view('administrator.deliveries.list_pdf');
        $pdf->loadView('administrator.deliveries.returns_list_pdf', $data);


        // download PDF file with download method
        return $pdf->stream('liste_retour_' . $selected_date . '.pdf');

    }
}
