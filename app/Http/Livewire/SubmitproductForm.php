<?php

namespace App\Http\Livewire;

use App\Models\SubmitedProduct;
use App\Rules\Imagecount;
use Livewire\Component;
use Livewire\WithFileUploads;

class SubmitproductForm extends Component
{

    use WithFileUploads;

    public $categories;

    public $category, $name, $note, $marque, $color, $size, $rent_price, $value_price;

    public $img1,$img2,$img3;




    public function render()
    {
        return view('livewire.submitproduct-form');
    }

    public function submit(){
        $validated =  $this->validate();
        $validated['marque'] = $this->marque;
        $validated['size'] = $this->size;
        $validated['color'] = $this->color;

        $validated['user_id'] = auth()->user()->id;
        $validated['category_id'] = $validated['category'];

        $img1_filename = base64_encode($this->img1->getClientOriginalName()).'.'.$this->img1->guessExtension();
        $this->img1->storeAs('public/images/submited_products',$img1_filename);
        $validated['img1'] = $img1_filename;

        if($this->img2!=null){
            $img_filename = base64_encode($this->img2->getClientOriginalName()).'.'.$this->img2->guessExtension();
            $this->img2->storeAs('public/images/submited_products',$img_filename);
            $validated['img2'] = $img_filename;
        }

        if($this->img3!=null){
            $img_filename = base64_encode($this->img3->getClientOriginalName()).'.'.$this->img3->guessExtension();
            $this->img3->storeAs('public/images/submited_products',$img_filename);
            $validated['img3'] = $img_filename;
        }

        SubmitedProduct::create($validated);

        return redirect()->route('front.account.my-shop');


    }

    public function rules(){
       return [
            'category' => 'required',
            'name' => 'required',
            'note' => 'required',
            'rent_price' => 'required',
            'value_price' => 'required',
            'img1'   => 'required|image|mimes:jpeg,jpg,png|max:5100',
            'img2'   => 'nullable|image|mimes:jpeg,jpg,png|max:5100',
            'img3'   => 'nullable|image|mimes:jpeg,jpg,png|max:5100'
        ];
    }
    public function updatedImg1()
    {
        $this->validate([
            'img1' => 'image|max:5000',
        ]);
    }
    public function updatedImg2()
    {
        $this->validate([
            'img2' => 'image|max:5000',
        ]);
    }
    public function updatedImg3()
    {
        $this->validate([
            'img3' => 'image|max:5000',
        ]);
    }
}
