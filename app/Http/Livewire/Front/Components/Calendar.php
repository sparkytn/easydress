<?php

namespace App\Http\Livewire\Front\Components;

use App\Models\Booking;
use App\Models\StatusEvent;
use App\Services\ProductService;
use Livewire\Component;
use App\Models\Product;

class Calendar extends Component
{

    public Product $product;
    public bool $calendarForTry;
    public $rangeStart;
    public $rangeEnd;
    public $bookedDates;
    public $selectedPeriod;
    public $selectedDates;
    public $activeCalendar;
    public $price;
    public $available;
    public $inCart = false;

    function boot() {

    }

    function mount() {
        $this->rangeStart =  now()->addDay()->format('m/d/Y');
        $this->rangeEnd =  now()->addMonths(6)->format('m/d/Y');

        $this->activeCalendar = now();

        $this->selectedPeriod = 3;

        $this->price = (Product::findOrFail($this->product->id))->rent_price;

        $this->bookedDates = Booking::join('booking_items', 'bookings.id', '=', 'booking_items.booking_id')
                                ->select(['bookings.id','bookings.start_date','bookings.end_date','status_event_id'])
                                ->where('bookings.status_event_id','<>', StatusEvent::STATUSES['ended'])
                                ->where('bookings.status_event_id','<>', StatusEvent::STATUSES['canceled'])
                                ->where('bookings.end_date','>=', (new \DateTime())->format('Y-m-d H:i:s'))
                                ->where('booking_items.product_id', $this->product->id )
                                ->orderBy('start_date')
                                ->get()
                                ->toJson();


    }

    public function render()
    {
        if( $this->selectedPeriod == 2) {
            $this->calendarForTry = true;
        }else{
            if(session()->has('selected_period')){
                $this->selectedPeriod = session('selected_period');

                $cart  = collect(session()->get('cart') ?? array());
                foreach ($cart as $cart_item){
                    if($this->product->id == $cart_item['product_id']){
                        $this->inCart = $cart_item ;
                    }
                }
            }
        }

        return view('livewire.front.components.calendar');
    }
}
