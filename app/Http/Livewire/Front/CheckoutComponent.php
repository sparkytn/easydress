<?php

namespace App\Http\Livewire\Front;

use Livewire\Component;

class CheckoutComponent extends Component
{
    public function render()
    {
        return view('livewire.front.checkout-component')->layout('front.layouts.app', ['title'=> 'Checkout']);
    }
}
