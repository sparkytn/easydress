<?php

namespace App\Http\Livewire\Front;

use App\Models\Attribute;
use App\Models\AttributeValues;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\Booking;
use App\Models\StatusEvent;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithPagination;

class ShopComponent extends Component
{

    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $categories;
    public Category $category;
    public $brands;
    public $brand;
    public $productPerPage = 12;
    public $attributes;
    public $date;
    public $booked = [];
    public $filters = [];

    protected $queryString = [
        'filters',
        'date',
    ];

    // Before the component is rendered
    //-----------------
    public function mount(?Category $category)
    {
        $this->brands = Brand::orderBy('name')->get();
        $this->category = $category;


    }


    public function render()
    {
        /* Check if the cart is empty or have alreadu items */
        $cart = session()->get('cart');

        // the cart has already items , get all bookings for that dates
        if ($cart != null && count($cart) > 0) {

            $start_date = session()->get('start_date');
            $end_date = session()->get('end_date');

            $this->booked = Booking::getBookedProducts($start_date,$end_date);

            foreach ($cart as $item){
                $this->booked[] =$item['product_id'];
            }
        }

        // check booked products for searched period
        if ($this->date != null && $cart === null) {
            $start_date = Carbon::parse($this->date)->format('Y-m-d H:i:s');
            $end_date = Carbon::parse($start_date)->addDays(3)->format('Y-m-d H:i:s');
            $this->booked = Booking::getBookedProducts($start_date,$end_date);
        }


        $this->attributes = Attribute::with(['values', 'categories'])
            ->rightJoin('category_attribute', 'attributes.id', '=', 'category_attribute.attribute_id')
            ->when(
                $this->category->exists,
                fn ($query, $category) =>
                $query->where('category_id', $this->category->id)
            )
            ->get();

        if(count($this->filters)==0){
            $products = Product::latest()
                    ->with(['category', 'brand', 'images','attributes_values'])
                    ->active()
                    ->filter([
                        'category' => $this->category,
                        'brand' => $this->brand,
                        'attributes_values' => $this->filters
                    ])
                    ->paginate($this->productPerPage);
        }else{
            $products = Product::latest()
                    ->with(['category', 'brand', 'images','attributes_values'])
                    ->active()
                    ->filter([
                        'category' => $this->category,
                        'brand' => $this->brand,
                        'attributes_values' => $this->filters
                    ])->paginate($this->productPerPage);
        }

        //var_dump($this->brand);
        //dd([$this->filters,$products]);
        // Fire From end JS to re render libreries
        $this->emit('reRendering');

        return view('livewire.front.shop-component', [
            'products' => $products,
            'categories' => $this->categories,
            'brands' => $this->brands,
            'activeBrand' => $this->brand,
            'activeCategory' => $this->category->name ?? 'all',
            'attributes' => $this->attributes,
            'booked'    => $this->booked,
            'filters'    => $this->filters
        ])
            ->layout('front.layouts.app', ['title' => 'Emprunter des vêtements']);
    }
}
