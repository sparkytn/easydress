<?php

namespace App\Http\Livewire\Front;

use Livewire\Component;

class CartComponent extends Component
{
    public function render()
    {
        return view('livewire.front.cart-component')->layout('front.layouts.app', ['title'=> 'Mon Panier']);
    }
}
