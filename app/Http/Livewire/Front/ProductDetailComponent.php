<?php

namespace App\Http\Livewire\Front;

use App\Models\Product;
use App\Models\Booking;
use Livewire\Component;

class ProductDetailComponent extends Component
{
    public Product $product;
    public $booked;

    public function mount(Product $product)
    {
        $this->product = $product;
        $this->booked = array();
    }


    public function render()
    {/* Check if the cart is empty or have alreadu items */
        $cart = session()->get('cart');

        $products = Product::with(['category','brand','images'])
            ->inRandomOrder()
            ->limit(6)
            ->active()
            ->get();

        // the cart has already items , get all bookings for that dates
        if ($cart != null && count($cart) > 0) {

            $start_date = session()->get('start_date');
            $end_date = session()->get('end_date');

            $this->booked = Booking::getBookedProducts($start_date,$end_date,$this->product->id);
        }

        $available = (is_array($this->booked) && in_array($this->product->id,$this->booked))? false:true;

        $product = Product::with(['images','category','brand'])->findOrFail($this->product->id);

        return view('livewire.front.product-detail-component',[
                'product'=>$product,
                'products'=>$products,
                'available'=>$available
            ])
            ->layout('front.layouts.app', ['title'=> $product->name]);
    }
}
