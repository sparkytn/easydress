<?php

namespace App\Http\Requests\Administrator;

use App\Models\Category;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;


class StoreProductRequest extends FormRequest
{
    protected $category;

    protected $user;

    public $required_attributes;



    /**
     * Return required category attributes to be validated
     * Throw exception if user and category not found
     *
     * @return \App\Models\Attribute
     */

    public function verify(){

        $selected_category = $this->input('category');
//        $selected_user = $this->input('user');
//        $this->user = User::findOrFail($selected_user);
        $this->category = Category::findOrFail($selected_category);

        $this->required_attributes = $this->category->attributes;

        return $this->required_attributes;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    protected function prepareForValidation()
    {
        $this->merge([
            'slug' => Str::slug($this->slug),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        // Rules for the static fields
        $rules_array=[
                'name' => 'required|min:3|max:255',
                'brand' => 'required|required',
                'slug' => 'required|alpha_dash|unique:products|min:3|max:255',
                'rent_price' => 'required|integer',
                'value_price' => 'required|integer',
                'style_description' => 'required',
                'composition_description' => 'required',
                'size_description'  =>'required',
                'is_online'  =>'boolean'
        ];

        if( in_array($this->method(), ['PUT', 'PATCH']) ){
            $rules_array[ 'slug'] = 'required|min:3|max:255|unique:products,slug,'. $this->id;
        }

        // Category specific dynamic rules
       if($this->verify()){
            foreach($this->required_attributes as $r_attribute){
                $rules_array['attribute_'.$r_attribute->id] = 'required';
            }
       }

        return $rules_array;
    }
}
