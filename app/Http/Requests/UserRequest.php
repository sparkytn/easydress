<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        if ( ! in_array($this->method(), ['PUT', 'PATCH']) && $this->id !== auth()->user()->id ) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|email:rfc,filter|max:255|unique:users',
            'phone' => 'required|string|max:12|unique:users',
            'city' => 'required|string|max:100',
            'password' => 'required|string|confirmed|min:8',
        ];

        // for update method
        if( in_array($this->method(), ['PUT', 'PATCH']) ){
            $rules = [
                'name' => 'required|string|max:255',
                'email' => 'required|email:rfc,filter|max:255|unique:users,email,'.auth()->user()->id,
                'phone' => 'required|string|max:12|unique:users,phone,'. auth()->user()->id,
                'city' => 'required|string|max:100',
            ];
        }

        return $rules;
    }
}
