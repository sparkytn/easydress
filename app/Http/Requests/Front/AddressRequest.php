<?php

namespace App\Http\Requests\Front;


use Illuminate\Foundation\Http\FormRequest;


class AddressRequest extends FormRequest
{
   

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        // Rules for the static fields
        $rules_array=[
            'contact_person'    => 'required',
            'contact_phone'     => 'required',
            'address_label'     => 'required',
            'street'            => 'required',
            'zip_code'          => 'required',
            'city'              => 'required'
        ];

        return $rules_array;
    }
}
