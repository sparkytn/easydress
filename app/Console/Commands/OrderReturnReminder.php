<?php

namespace App\Console\Commands;

use App\Models\Booking;
use App\Models\StatusEvent;
use App\Notifications\BookingStatusNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;

class OrderReturnReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:return-order-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remind user on the returning date of the the articles ordered and change booking status';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $bookings = Booking::where('end_date', '<=' , Carbon::now()->startOfDay()->addDay()->toDateTimeString())
                    ->where('payment_recieved', '!=', 0)
                    ->where('status_event_id', StatusEvent::STATUSES['delivered'])
                    ->with(['products'])
                    ->get();
//        dd(Carbon::now()->startOfDay()->addDay()->toDateTimeString(),$bookings,StatusEvent::STATUSES['delivered']);

        foreach ($bookings as $booking){

            Log::info('Sending User Notification Order Return ', ['id' => $booking->user->id,'booking'=> $booking]);
            $booking->status_event_id = 4;
            $booking->save();
            $booking->user->notify(new BookingStatusNotification($booking));
        }

    }
}
