<?php

namespace App\Services;

use Illuminate\Support\Str;

class ProductService
{

    static function generateRef($product){

        return Str::upper(Str::substr($product->category->name, '0', '3')).$product->id;

    }

    public static function period_price($product, $period)
    {

        $min_period = 3;
        if($period == $min_period){
            return $product->rent_price;
        }
        return $product->rent_price + ( $product->rent_price * ($period/$min_period) * ($period/$min_period /10) );
    }

}
