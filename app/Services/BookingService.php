<?php

namespace App\Services;

use Illuminate\Support\Str;

class BookingService
{

    static function generateStatusBadge($booking)
    {

        $status = @$booking->currentStatus ?? '';

        $status_name = $status->name;



        return '<span class="badge bg-'. $booking->badge_class.'" data-toggle="todotooltip"
                                data-placement="top" title="">'.$status_name.'</span>';
    }
    static function generateStatusBadgeClass($status = 1)
    {
        return ' status-'.$status;
    }
}
