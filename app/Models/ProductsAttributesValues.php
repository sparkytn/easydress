<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductsAttributesValues extends Model
{
    use HasFactory;

    protected $table = 'products_attributes_values';

    protected $fillable = ['product_id','attribute_value_id'];
    
    public $timestamps = FALSE;

    public function products(){
        return $this->belongToMany(Product::class,'products_attributes_values','product_id','attribute_value_id');
    }
}
