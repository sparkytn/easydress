<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Boolean;
use function PHPUnit\Framework\isEmpty;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'slug', 'user_id', 'category_id', 'brand_id', 'rent_price', 'value_price', 'style_description',
        'composition_description', 'size_description', 'is_online'
    ];

    protected $casts = ['is_online' => 'boolean'];

    /**
     * @return \App\Model\Brand
     */

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * @return \App\Model\Category
     */

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \App\Model\User
     */

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \App\Model\AttributeValues
     */

    public function attributes_values()
    {
        return $this->belongsToMany(AttributeValues::class, 'products_attributes_values', 'product_id', 'attribute_value_id');
    }

    public function images()
    {
        return $this->hasMany(ProductImgs::class);
    }


    public function booked()
    {
        return $this->belongsTo(BookingItems::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getRefAttribute()
    {
        return Str::upper(Str::substr($this->category->name, '0', '3')). $this->id ;

    }

    /*==============================
    | Scopes
    */

    /**
     * Get Active (is_online) Products....
     */
    public function scopeActive($query)
    {
        return $query->where('is_online', 1);
    }

    /**
     * Filter Product by Brand, Category....
     */
    public function scopeFilter($query, $filters)
    {
        // Filter By Category
        $query->when($filters['category']->exists ? $filters['category'] : null , function ($query, $category ) {
            $query->whereHas(
                'category',
                fn ($query) =>
                $query->where('slug', $category->slug)
            );
        });


        // Filter By Attributes Value
        $query->when($filters['attributes_values'], function ($query, $filters) {
            foreach($filters as $value){
                if($value != 'null'){
                    $query->whereHas('attributes_values',function($q) use ($value){
                        $q->where('attribute_value_id','=',$value);
                    });
                }

            }
        });

        // Filter By Brand
        $query->when($filters['brand'] ?? false, function ($query, $brand) {
            $query->where('brand_id', $brand);
        });
    }


    /////////////////////////
    //  FRONTEND method
    ////////////////////////

    //* Get the 2 first images of a product
    /*
     /* */

    public function first2Images()
    {
        return $this->images()->select('file_name')->orderBy('order', 'ASC')->take(2);
    }
    public function getImageAttribute()
    {
        return $this->images()->select('file_name')->orderBy('order', 'ASC')->first();
    }
}
