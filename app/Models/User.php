<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'city',
        'password',
        'google_id',
        'facebook_id',
        'twitter_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function deliveryAddresses(){

        return $this->hasMany(DeliveryAddresses::class);

    }

    public function bookings(){

        return $this->hasMany(Booking::class);
    }

    public function products(){

        return $this->hasMany(Product::class);

    }

    public function adresses()
    {
        $this->hasMany(Adresses::class);
    }

    public function submited_products(){
        return $this->hasMany(SubmitedProduct::class);
    }

}
