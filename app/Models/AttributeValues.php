<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttributeValues extends Model
{
    use HasFactory;

    protected $table ='attribute_values';

    protected $fillable =['attribute_id','value'];

    /**
     * @return \App\Model\Category
     */

    public function attribute(){

        return $this->belongsTo(Attribute::class);

     }


     /**
     * @return \App\Model\AttributeValues
     */

    public function products(){

        return $this->belongToMany(Product::class,'products_attributes_values');

    }
}
