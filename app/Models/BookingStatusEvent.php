<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingStatusEvent extends Model
{
    use HasFactory;

    protected $fillable = ['booking_id','status_event_id','details'];

    public function update_booking_status(){

        $booking = Booking::findOrFail($this->booking_id);

        $booking->status_event_id = $this->status_event_id;

        $booking->update();

    }

    public function booking(){
        return $this->belongsTo(Booking::class);
    }

    public function status(){
        return $this->belongTo(StatusEvent::class);
    }
}
