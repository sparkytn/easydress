<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table ='categories';

    protected $fillable=['name','slug'];

    /**
     * Get all the attribute of this category
     *
     * @return \App\Model\Attribute
     */

    public function attributes()
    {
        return $this->belongsToMany(Attribute::class,'category_attribute');
    }

    /**
     * Get all products of this category
     *
     * @return \App\Model\Product
     */

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function submited_products()
    {
        return $this->hasMany(SubmitedProduct::class);
    }
    
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
