<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusEvent extends Model
{
    use HasFactory;

    protected $fillable = ['name'];


    const  STATUSES = [
            'pending' => 1,
            'confirmed' => 2,
            'delivered' => 3,
            'to_return' => 4,
            'ended' => 5,
            'canceled' => 6,
        ];
    const  STATUSES_LABELS = [
        'En attente',
        'Confirmée',
        'Livrée',
        'A retourner',
        'Terminée',
        'Annulée',
    ];

    const  STATUSES_COLORS = [
         '#ffc107',
         '#28a745',
         '#17a2b8',
         '#673ab7',
         '#92a8b3',
         '#9e9e9e',
    ];

    public $timestamps = false;

    public function bookings()
    {
        return $this->belongsToMany(Booking::class,'booking_status_events')->withPivot('details','created_at');
    }

    public function booking(){
        return $this->hasMany(Booking::class);
    }

    static public function status_label($val = 1) {
        return self::STATUSES_LABELS[$val];
    }
    static public function status_color($val = 1) {
        return self::STATUSES_COLORS[$val];
    }

}
