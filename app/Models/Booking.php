<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Booking extends Model
{
    use HasFactory;

    protected $table = 'bookings';


    protected $fillable = [
        'code', 'user_id', 'delivery_address_id',
        'status_event_id', 'start_date', 'end_date', 'price',
        'deposit', 'note', 'payment_note', 'payment_recieved', 'deposit_refunded'
    ];


    protected $dates = [
        'end_date',
        'start_date',
    ];

    /**
     * Prepare a date for array / JSON serialization.
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(\DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }



    public function status()
    {
        return $this->belongsToMany(StatusEvent::class, 'booking_status_events')->withPivot('details', 'created_at');
    }


    public function currentStatus()
    {
        return $this->belongsTo(StatusEvent::class, 'status_event_id');
    }


    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function products()
    {
        return $this->belongsToMany(Product::class, 'booking_items');
    }

    public function items()
    {
        return $this->hasMany(BookingItems::class);
    }

    public function deliveryAddress()
    {

        return $this->belongsTo(DeliveryAddresses::class);
    }

    public function getPeriodAttribute()
    {
        return $this->start_date->diffInDays($this->end_date->addHours(24));

    }

    public function getTypeAttribute()
    {
        return $this->start_date->diffInDays($this->end_date->addHours(24)) > 2 ? 'Location' : 'Essayage';

    }

//    public function getEndDateAttribute($value){
//        return Carbon::parse($value)->addHours(24);
//    }

    public function getBadgeClassAttribute($value)
    {
            return ' status-'.$this->status_event_id;

    }


    public function generateStatusNotification()
    {

        $subject = 'Votre commande #' . $this->code . ' ';

        $msg = 'Bonjour ' . $this->user->name . ',<br>';

        $products_list = '<ul>';
        foreach ($this->products as $product) {
            $products_list .= '<li><b>' . $product->name . '</b></li>';
        }
        $products_list .= '</ul>';


        switch ($this->status_event_id) {

            case '1': //Commande pending
                $subject .= ' a été enregistrée';
                $msg .= '<br> Vous serez notifier lorsque elle sera confirmée';
                break;
            case '2': //Commande confirmé
                $subject .= ' a été confirmée';
                $msg .= '<br> Vous serez notifier lorsque elle sera expédiée';
                break;

            case '3': //Commande livrée
                $subject .= ' a été livrée';
                $msg .= "<br> Profitez-en
                                <br>N'oublier pas que la date de retour est
                                le :" . Str::ucfirst($this->end_date->translatedFormat('l d/m/Y')) .
                    " avant 12h:00, nous comptons sur votre compréhonsion";
                break;

            case '4': //Commande à retournée
                $subject = 'Notification pour retour du produit que vous avez commandé';
                $msg .= 'Suite à la fin de la période de location, Merci de bien vouloir nous retourner :'
                    . $products_list
                    . 'Veuillez noter que la date de retourn est le :<b>' . Str::ucfirst($this->end_date->translatedFormat('l d/m/Y')) .
                    '</b> avant 12h:00 , nous comptons sur votre compréhonsion';
                break;
            case '5': //Commande retournée
                $subject = 'Notification du retour du produit';
                $msg .= 'Nous avons bien reçu le(s) article(s) :' . $products_list
                    . 'On vous remercie pour votre collaboration.<br>'
                    . "N'hésitez pas à consulter notre catalogue pour d'autre commande.";
                break;
            case '6': //Commande annulée
                $subject .= ' a été annulée';
                $msg .= "<br> N'hésitez pas à nous contacter si vous voulez plus d'information.";
                break;
        }

        $notification = array(
            'subject' => $subject,
            'msg' => $msg
        );

        return $notification;
    }

    public static function getDeliveryByPeriod($from, $to = false, $booking = false)
    {
        $collection = Booking::query();

        $collection->with('items', 'user', 'deliveryAddress');
        $collection->where('status_event_id','=',StatusEvent::STATUSES['confirmed']);

        if ($to) {
            $collection->whereBetween('start_date', [$from, $to]);
        } else {
            $collection->whereDate('start_date', '=', $from);
        }


        if ($booking) {
            $collection->where('id', '=', $booking);
        }

        return $collection->get();

    }


    public static function getBookingsInRange($start_date, $end_date, $filterBy = false, $comparision)
    {
//        $collection = Booking::query();
//
//        $collection->with('items', 'user', 'deliveryAddress');
//        $collection->whereDate('start_date', '>=', $from);
//        $collection->whereDate('start_date', [$from, $to]);
//        return $collection->get();

        return Booking::query()
            ->with('items', 'user', 'deliveryAddress')
            ->where('status_event_id','<>',StatusEvent::STATUSES['canceled'])
            ->orWhereBetween('start_date', [$start_date,$end_date])
            ->orWhereBetween('end_date', [$start_date,$end_date])
//            ->where('status_event_id', '>=', StatusEvent::STATUSES['ended'])
            ->get();


    }

    public static function getReturnsByPeriod($from, $to, $group = false)
    {
        $collection = Booking::query();

        $collection->with('items', 'user')
            ->where('status_event_id',  StatusEvent::STATUSES['delivered'])
            ->where('payment_recieved', 1);

        if ($to) {
            $collection->whereBetween('end_date', [$from, $to]);
        } else {
            $collection->whereDate('end_date', '=', $from);
        }


        return $collection->get();

    }

    public static function getBookedProducts($start_date, $end_date, $product_id = false)
    {
        $query = DB::table('bookings')
            ->join('booking_items', 'bookings.id', '=', 'booking_items.booking_id')
            ->where('bookings.status_event_id','<>', StatusEvent::STATUSES['ended'])
            ->where('bookings.status_event_id','<>', StatusEvent::STATUSES['canceled'])
            ->where(function ($query) use ($end_date, $start_date) {
                $query->whereBetween('start_date',[$start_date,$end_date])
                    ->orWhereBetween('end_date',[$start_date,$end_date])
                    ->orWhereBetween(DB::raw("'" . $start_date . "'"), [DB::raw("`start_date`"), DB::raw("`end_date`")])
                    ->orWhereBetween(DB::raw("'" . $end_date . "'"), [DB::raw("`start_date`"), DB::raw("`end_date`")]);
            })
            ->select(['booking_items.product_id']);

        if ($product_id) {
            $query->where('booking_items.product_id', '=', $product_id);
        }

        $result = $query->get();

        $booked_products = array();

        foreach ($result as $r) {
            $booked_products[] = $r->product_id;
        }
        return $booked_products;
    }


    public static function getBookedDetailProducts($start_date, $end_date, $product_id = false)
    {
        $query = Booking::with(['items', 'products'])
            ->whereBetween('start_date',[$start_date,$end_date])
            ->whereBetween('end_date',[$start_date,$end_date])
            ->whereBetween(DB::raw("'" . $start_date . "'"), [DB::raw("`start_date`"), DB::raw("`end_date`")])
            ->orWhereBetween(DB::raw("'" . $end_date . "'"), [DB::raw("`start_date`"), DB::raw("`end_date`")])
            ->where('status_event_id', '>=', StatusEvent::STATUSES['ended']);
        if ($product_id) {
            $query->where('booking_items.product_id', '=', $product_id);
        }

        $result = $query->get();

        $booked_products = array();

        foreach ($result as $r) {
            $booked_products[] = $r;
        }

        return $booked_products;
    }


}
