<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubmitedProduct extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'user_id', 'category_id', 'brand_id', 'marque', 'rent_price', 'value_price', 'size', 'color',
        'note', 'img1', 'img2', 'img3'
    ];

    /**
     * @return \App\Model\Brand
     */

    public function brand()
    {

        return $this->belongsTo(Brand::class);
    }

    /**
     * @return \App\Model\Category
     */

    public function category()
    {

        return $this->belongsTo(Category::class);
    }

    /**
     * @return \App\Model\User
     */

    public function user()
    {

        return $this->belongsTo(User::class);
    }

    /**
     * @return \App\Model\AttributeValues
     */

    public function attributes_values()
    {

        return $this->belongsToMany(AttributeValues::class, 'submited_products_attributes_values', 'product_id', 'attribute_value_id');
    }

    public function shop_product()
    {
        return $this->belongsTo(Product::class, 'migrated');
    }


}
