<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductImgs extends Model
{
    use HasFactory;

    protected $table = 'product_imgs';

    protected $fillable = ['file_name','product_id','order'];

    public function product(){
        return $this->belongsTo(Product::class);
    }

    public function reorder(Array $order){
        foreach($order as $key => $value){
            $this->where('id',$value)->update(['order'=>$key+1]);
        }
    }

    public function decrementOrderFrom(Int $order){
        DB::update('
            UPDATE '.$this->table.' 
            SET `order` = `order`-1 
            WHERE product_id = ? 
            AND `order` > ?',
            [$this->product->id,$order]
        );
    }
}
