<?php

namespace App\Notifications;

use App\Models\Booking;
use App\Services\ProductService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;

class BookingStatusNotification extends Notification
{
    use Queueable;

    private $booking;

    private $msg;


    /**
     * Create a new notification instance.
     *
     * @param $booking
     */
    public function __construct($booking)
    {
        $this->booking = $booking;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $message = new MailMessage;
        $message->greeting('Bonjour '.$this->booking->user->name . ',');

        $subject = 'EasyDress - Votre commande #' . $this->booking->code . ' ';

        $products_list = "<div class='table'><table style='width: 100%' width='100%'><tr><th>ID</th><th>Nom de l'article</th><th>Caution</th></tr>";
        foreach ($this->booking->products as $product) {
            $product_id = ProductService::generateRef($product);
            $products_list .= "<tr><td>".$product_id." </td><td>".$product->name."</td><td>".$product->value_price."DT</td></tr>";
        }
        $products_list .= '</table><div>';


        switch ($this->booking->status_event_id) {

            case '1': //Commande pending
                $message->subject($subject. ' a été enregistrée');
                $message->line('Votre commande a été enregistrée. Vous devez payé la somme de '. $this->booking->price .'DT pour la confirmer.');
                $message->action('Comment payer ?', route('front.account.payment.methods'));
                $message->line("N'oubliez pas que vous devez aussi fournir un chéque de caution au nom de 'Société location EASY DRESS' et d'un montant de ".$this->booking->deposit ."DT le jour de la réception de la commande.");
                $message->line(new HtmlString("N'hésitez pas à <a href='mailto:contact@easydress.tn' title='contact'>nous contacter</a> si vous avez besoin d'informations."));
                $message->line('On vous remercie pour votre collaboration.');
                break;
            case '2': //Commande confirmé
                $message->subject($subject. ' a été confirmée');
                $message->line("Votre commande a été confirmée. Vous serez notifié lorsqu'elle sera expédiée le ". $this->booking->start_date->format('l  d F Y'));
                $message->line('Votre Commande :');
                $message->line(new HtmlString($products_list));
                $message->line(new HtmlString("N'hésitez pas à <a href='mailto:contact@easydress.tn' title='contact'>nous contacter</a> si vous avez besoin d'informations."));
                $message->line('On vous remercie pour votre collaboration.');
                break;

            case '3': //Commande livrée
                $message->subject($subject. ' a été livrée');
                $message->line("Votre commande a été livrée, Profitez-en!");
                $message->line("N'oublier pas que la date de retour de commmande est le " . Str::ucfirst($this->booking->end_date->translatedFormat('l  d F Y')) .", nous comptons sur votre compréhonsion.");
                $message->line('On vous remercie pour votre collaboration.');

                break;

            case '4': //Commande à retournée
                $end_date = Str::ucfirst($this->booking->end_date->translatedFormat('l  d F Y'));
                $message->subject('EasyDress - Rappel de la date de retour de la commande #'. $this->booking->code);
                $message->line('On voulait vous rappeler que votre période de location se termine demain le '. new HtmlString($end_date).'. On vous contactera pour vous rendre la caution et récupérer la commande contenant :');
                $message->line(new HtmlString($products_list));
                $message->line(new HtmlString("N'hésitez pas à <a href='mailto:contact@easydress.tn' title='contact'>nous contacter</a> si vous avez besoin d'informations."));
                $message->line('On vous remercie pour votre collaboration.');
                break;
            case '5': //Commande retournée
                $message->subject('EasyDress - Merci! Commande #'. $this->booking->code .' est retournée');
                $message->line('Nous avons bien reçu le(s) article(s) :');
                $message->line(new HtmlString($products_list));
                $message->line("N'hésitez pas à consulter notre catalogue.");
                $message->action('Voir la boutique', route('front.shop'));
                $message->line("On vous remercie pour votre collaboration.");
                break;
            case '6': //Commande annulée
                $message->subject($subject. ' a été annulée');
                $message->line("Votre commande est annulée. N'hésitez pas à nous contacter si vous voulez plus d'information.");
                $message->action('Contactez-nous', 'mailto:contact@easydress.tn');
                break;
        }

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
