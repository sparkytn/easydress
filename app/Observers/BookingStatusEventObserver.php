<?php

namespace App\Observers;

use App\Models\BookingStatusEvent;

class BookingStatusEventObserver
{
    /**
     * Handle the BookingStatusEvent "created" event.
     *
     * @param  \App\Models\BookingStatusEvent  $bookingStatusEvent
     * @return void
     */
    public function created(BookingStatusEvent $bookingStatusEvent)
    {
        $bookingStatusEvent->update_booking_status();
    }

    /**
     * Handle the BookingStatusEvent "updated" event.
     *
     * @param  \App\Models\BookingStatusEvent  $bookingStatusEvent
     * @return void
     */
    public function updated(BookingStatusEvent $bookingStatusEvent)
    {
        //
    }

    /**
     * Handle the BookingStatusEvent "deleted" event.
     *
     * @param  \App\Models\BookingStatusEvent  $bookingStatusEvent
     * @return void
     */
    public function deleted(BookingStatusEvent $bookingStatusEvent)
    {
        //
    }

    /**
     * Handle the BookingStatusEvent "restored" event.
     *
     * @param  \App\Models\BookingStatusEvent  $bookingStatusEvent
     * @return void
     */
    public function restored(BookingStatusEvent $bookingStatusEvent)
    {
        //
    }

    /**
     * Handle the BookingStatusEvent "force deleted" event.
     *
     * @param  \App\Models\BookingStatusEvent  $bookingStatusEvent
     * @return void
     */
    public function forceDeleted(BookingStatusEvent $bookingStatusEvent)
    {
        //
    }
}
