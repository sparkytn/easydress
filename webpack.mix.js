const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.copy('node_modules/@coreui/utils/dist/coreui-utils.js', 'public/admin_assets/js');
// mix.copy('node_modules/pace-progress/pace.min.js', 'public/admin_assets/js');
mix.copy('node_modules/@coreui/coreui/dist/js/coreui.bundle.min.js', 'public/admin_assets/js');

//fonts
// mix.copy('node_modules/@coreui/icons/fonts', 'public/fonts');
//icons
// mix.copy('node_modules/@coreui/icons/css/free.min.css', 'public/admin_assets/css');
// mix.copy('node_modules/@coreui/icons/css/brand.min.css', 'public/admin_assets/css');
// mix.copy('node_modules/@coreui/icons/css/flag.min.css', 'public/admin_assets/css');
// mix.copy('node_modules/@coreui/icons/svg/flag', 'public/admin_assets/svg/flag');

// mix.copy('node_modules/@coreui/icons/sprites/', 'public/admin_assets/icons/sprites');
//images
// mix.copy('resources/assets', 'public/admin_assets/assets');

mix.browserSync(process.env.APP_URL)
	// front assets
	.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
	.options({
		processCssUrls: false
	})
	// Admin assets
	.js('resources/js/admin/app.js', 'public/admin_assets/js')
    .sass('resources/sass/admin/style.scss', 'public/admin_assets/css')
    .sourceMaps();
