<x-app-layout>
    <div class="container">
        <div class="row justify-content-center mt-5">
            <div class="col-lg-6 col-md-8">
                <x-auth-card>
                    <x-slot name="logo">
                        <h1>Mot de passe oublié</h1>
                    </x-slot>


                    <div class="mb-4 text-sm text-gray-600">
                        {{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
                    </div>

                    <!-- Session Status -->
                    <x-auth-session-status class="mb-4" :status="session('status')"/>

                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors"/>

                    <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <!-- Email Address -->
                        <div>
                            <x-label for="email" :value="__('Adresse Email')"/>

                            <x-input id="email" class="form-control" type="email" name="email" :value="old('email')" placeholder="Votre email" required autofocus/>
                        </div>

                        <div class="flex items-center justify-end mt-4">
                            <x-button class="btn btn-primary">
                                {{ __('Email Password Reset Link') }}
                            </x-button>
                        </div>
                    </form>
                </x-auth-card>
            </div>
        </div>
    </div>
</x-app-layout>
