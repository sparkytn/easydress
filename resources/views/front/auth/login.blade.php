<x-app-layout>
    <div style="background-color: rgba(0,0,0,1); position: relative;">
    <div  class="bg-secondary" style="background: url({{ asset('images/slide1.jpg') }}) center; background-size: cover; position: absolute; width: 100%;height: 100%;z-index: 0; left: 0; top: 0; opacity: 0.5">
    </div>
    <div class="container" style="z-index: 2; position: relative;">
        <div class="row justify-content-end">
            <div class="col-lg-6 col-md-8 mt-5 ">


                <nav>
                    <div class="nav nav-tabs bg-white d-flex text-center nav-pills" id="nav-tab" role="tablist">
                        <a class="nav-link active col border-0" id="nav-login-tab" data-toggle="tab" href="#nav-login" role="tab" aria-controls="nav-login" aria-selected="true">CONNEXION</a>
                        <a class="nav-link col border-0" id="nav-sign-up-tab" data-toggle="tab" href="#nav-sign-up" role="tab" aria-controls="nav-sign-up" aria-selected="false">INSCRIPTION</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-login" role="tabpanel" aria-labelledby="nav-login-tab">
                        <x-auth-card class="shadow-lg border-0">
                            <x-slot name="logo">
                                <h1 class="text-primary">Login</h1>
                            </x-slot>

                            <!-- Session Status -->
                            <x-auth-session-status class="mb-4" :status="session('status')" />

                            <!-- Validation Errors -->
                            <x-auth-validation-errors class="mb-4" :errors="$errors" />

                            <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <!-- Email Address -->
                                <div class="form-group">
                                    <x-label for="email" :value="__('Email')" />

                                    <x-input id="email"  type="email" name="email" :value="old('email')" required autofocus />
                                </div>

                                <!-- Password -->
                                <div class="form-group">
                                    <x-label for="password" :value="__('Mot de passe')" />

                                    <x-input id="password" type="password" name="password" required autocomplete="current-password" />
                                </div>

                                <!-- Remember Me -->
                                <div class="form-group">
                                    <div class="form-check">
                                        <input id="remember_me" type="checkbox" class="form-check-input" name="remember">
                                        <label for="remember_me" class="form-check-label">
                                            {{ __('Se souvenir de moi') }}
                                        </label>
                                    </div>
                                </div>

                                <div class="mb-4 text-center">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>
                                    <br>
                                    @if (Route::has('password.request'))
                                        <a class="d-inline-block mt-4" href="{{ route('password.request') }}">
                                            {{ __('Mot de passe oublié ?') }}
                                        </a>
                                    @endif

                                </div>
{{--                                <hr class="my-4">--}}
{{--                                <div class="text-center">--}}
{{--                                    <a href="{{ route('register') }}" class="btn btn-block btn-tertiary">Créer votre compte</a>--}}
{{--                                </div>--}}
                                <hr class="my-4">
                                <a href="{{ route('front.social.redirect','google') }}" class="btn btn-lg btn-google btn-block text-uppercase"><i class="fab fa-google mr-2"></i> Connexion avec Google</a>
                                <a href="{{ route('front.social.redirect','facebook') }}" class="btn btn-lg btn-facebook btn-block text-uppercase"><i class="fab fa-facebook-f mr-2"></i> Connexion avec Facebook</a>
                            </form>
                        </x-auth-card>
                    </div>
                    <div class="tab-pane fade" id="nav-sign-up" role="tabpanel" aria-labelledby="nav-sign-up-tab">
                        <x-auth-card>
                            <x-slot name="logo">
                                <h1 class="text-primary">Inscription</h1>
                            </x-slot>

                            <!-- Validation Errors -->
                            <x-auth-validation-errors class="mb-4" :errors="$errors" />

                            <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <!-- Name -->
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div>
                                            <x-label for="name" :value="__('Nom et prénom')" />

                                            <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-md-6">
                                        <!-- Email Address -->
                                        <div class="mt-4">
                                            <x-label for="email" :value="__('Email')" />

                                            <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <!-- Email Address -->
                                        <div class="mt-4">
                                            <x-label for="phone" :value="__('Téléphone')" />

                                            <x-input id="phone" class="block mt-1 w-full" type="tel" name="phone" :value="old('phone')" required />
                                        </div>
                                    </div>
                                </div>

                                <!-- Email Address -->
                                <div class="mt-4">
                                    <x-label for="city" :value="__('Votre Ville')" />
                                    <select id="city" class=" form-control" required name="city" value="{{old('city')}}">
                                        <option selected disabled>Gouvernorat</option>
                                        <option value="Ariana">Ariana</option>
                                        <option value="Béja">Béja</option>
                                        <option value="Ben Arous">Ben Arous</option>
                                        <option value="Bizerte">Bizerte</option>
                                        <option value="Gabès">Gabès</option>
                                        <option value="Gafsa">Gafsa</option>
                                        <option value="Jendouba">Jendouba</option>
                                        <option value="Kairouan">Kairouan</option>
                                        <option value="Kasserine">Kasserine</option>
                                        <option value="Kébili">Kébili</option>
                                        <option value="La Manouba">La Manouba</option>
                                        <option value="Le Kef">Le Kef</option>
                                        <option value="Mahdia">Mahdia</option>
                                        <option value="Médenine">Médenine</option>
                                        <option value="Monastir">Monastir</option>
                                        <option value="Nabeul">Nabeul</option>
                                        <option value="Sfax">Sfax</option>
                                        <option value="Sidi Bouzid">Sidi Bouzid</option>
                                        <option value="Siliana">Siliana</option>
                                        <option value="Sousse">Sousse</option>
                                        <option value="Tataouine">Tataouine</option>
                                        <option value="Tozeur">Tozeur</option>
                                        <option value="Tunis">Tunis</option>
                                        <option value="Zaghouan">Zaghouan</option>
                                    </select>
                                </div>

                                <!-- Password -->
                                <div class="mt-4">

                                    <x-label for="password" :value="__('Mot de passe')" />

                                    <x-input id="password" class="block mt-1 w-full"
                                             type="password"
                                             name="password"
                                             required autocomplete="new-password" />
                                    <span class="form-text">Au minimum 8 caractères</span>
                                </div>

                                <!-- Confirm Password -->
                                <div class="mt-4">
                                    <x-label for="password_confirmation" :value="__('Confirmation de mot de passe')" />

                                    <x-input id="password_confirmation" class="block mt-1 w-full"
                                             type="password"
                                             name="password_confirmation" required />
                                </div>

                                <div class="mt-4">
                                    <label for="privacy-input">
                                        <input type="checkbox" class="form-check-inline" id="privacy-input" value="privacy">

                                       Je confirme avoir lu et accepté les <a href="{{ route('front.home.politique') }}">Termes & Conditions et Politique de confidentialités</a>.
                                    </label>
                                </div>

                                <div class="mt-4 text-center">

                                    <button type="submit" class="btn btn-primary">
                                        {{ __('INSCRIPTION') }}
                                    </button>
                                    <div class="mt-4">
                                        <a class="small" href="{{ route('login') }}">
                                            {{ __('Déjà inscrit ?') }}
                                        </a>
                                    </div>

                                </div>
                            </form>
                        </x-auth-card>
                    </div>
                </div>



            </div>
        </div>

    </div>
    </div>
</x-app-layout>
