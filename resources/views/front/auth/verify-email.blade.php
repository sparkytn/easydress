<x-app-layout>
    <div style="background-color: rgba(0,0,0,1); position: relative;">
        <div  class="bg-secondary" style="background: url({{ asset('images/slide1.jpg') }}) center; background-size: cover; position: absolute; width: 100%;height: 100%;z-index: 0; left: 0; top: 0; opacity: 0.5">
        </div>
        <div class="container" style="z-index: 2; position: relative;">
            <div class="row justify-content-center">
                <div class="col-lg-8 mt-5">
                    <x-auth-card>
                        <x-slot name="logo">
                            <h1 class="text-primary">{{ __('Verification de l\'email') }}</h1>
                        </x-slot>

        <div class="mb-4">
            <p>Merci pour votre inscription! <br> Avant de commencer, pourriez-vous vérifier votre adresse e-mail en cliquant sur le lien que nous venons de vous envoyer ? </p>
            <p> Si vous n'avez pas reçu l'e-mail, nous vous en enverrons un autre avec plaisir.</p>
        </div>

        @if (session('status') == 'verification-link-sent')
            <div class="mb-4 text-success">
                <p>Un nouveau lien de vérification a été envoyé à l'adresse e-mail que vous avez fournie lors de l'inscription.</p>
            </div>
        @endif

        <div class="text-center">
            <form method="POST" action="{{ route('verification.send') }}">
                @csrf

                <div>
                    <button type="submit" class="btn btn-primary">
                        Renvoyer l'e-mail de vérification
                    </button>
                </div>
            </form>

            <form method="POST" action="{{ route('logout') }}">
                @csrf

                <button type="submit" class="btn btn-outline-primary mt-4">
                    Se déconnecter
                </button>
            </form>
        </div>
    </x-auth-card>
</x-guest-layout>
