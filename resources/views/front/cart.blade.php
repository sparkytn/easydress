<x-app-layout title="Mon Panier">
    @section('page-title', 'Mon Panier')
    @include('front.partials.page-header')

    <section class="section-spacing">
        <div class="container">

            <h3 class="text-primary">Détail du panier</h3>
            @if (session('card_full'))
                <x-front.max-order-alert show_cart_link="false"></x-front.max-order-alert>
            @endif

            @if (session('product_not_available'))
                <x-front.order-product-not-available :productNotAvailable="session('product_not_available')"></x-front.order-product-not-available>
            @endif
            <div class="card mb-5">
                <div class="card-body">
                    @forelse($cart as $key => $item)



                        <div class="row align-items-center mb-3">
                            <div class="col-md-2 text-center mb-2">

                                <img src="{{ asset('storage/imgs/card_thumb_' . $item['image']) }}" class="img-fluid" alt="">


                            </div>
                            <div class="col-md-7 mb-2">
                                <p class="small mb-2 text-muted">{{ $item['category'] }}</p>
                                <h4 class="mb-1"><span class="text-primary">
                                        {{ $item['product_name'] }}</span></h4>
                                <h6 class="text-muted">{{ $item['brand'] }}</h6>
                                <p>Location pour <b>{{ $item['selected_period'] }} jours</b>, du
                                    <b>{{ $item['start_date'] }}</b> au
                                    <b>{{ $item['end_date'] }}</b>
                                </p>

                            </div>
                            <div class="col-md-2 text-center mb-2">
                                <p class="small mb-2">Prix à payer</p>
                                <h2 class="text-primary">{{ $item['price'] }} <sup>DT</sup></h2>
                                <small>Prix du caution : <b>{{ $item['deposit'] }} DT</b></small>

                            </div>
                            <div class="col-md-1 text-center mb-2">
                                <form action="{{ route('front.deletefromcart') }}" method="POST">
                                    @csrf
                                    <input type="hidden" value="{{ $key }}" name="cart_item_id" />
                                    <button class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                </form>
                            </div>
                        </div>
                        <hr>
                    @empty
                        <div class="row align-items-center">
                            Votre panier est vide!
                        </div>
                    @endforelse
                    @if( count($cart)> 0)
                        <div class="bg-primary p-3 text-white font-weight-bold" style="font-size: 18px; ">
                            <div class="row align-items-center">
                                <div class="col-md-6">
                                    Prix Total :
                                </div>
                                <div class="col-md-6 text-md-right">
                                    <b>{{ session('total_amount') }} DT</b> <br>
                                   <span class="small">Caution <b>{{ session('total_deposit') }} DT</b></span>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>



            <div class="mt-5 text-center">
                @if ( count($cart) == 0)
                    <a class="btn btn-primary" href="{{ route('front.shop') }}">Voir la boutique</a>
                @else

                    <form action="{{ route('front.checkout') }}" method="post">
                        @csrf
                        <div class="alert alert-info">
                            <h3>Je confirme que :</h3>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="confirm" id="confirmCart" required>
                                <label class="form-check-label" for="confirmCart">
                                    Je souhaite recevoir ma commande le <b>{{ session('start_date') }}</b> et je la renvoie le <b>{{ session('end_date') }}</b>
                                </label>
                            </div>

                        </div>

                        <button class="btn btn-primary" type="submit" >Valider mon panier</button>
                    </form>

                @endif
            </div>

        </div>
    </section>
</x-app-layout>
