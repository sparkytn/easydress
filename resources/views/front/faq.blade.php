@section('body_attributes', 'data-spy=scroll data-target=#list-faq data-offset=80')
<x-app-layout title="FAQ">

    <header class="section-spacing mb-5 bg-secondary">
        <div class="container text-center">
            <h1 class="mb-0 text-white">FAQ</h1>
        </div>
    </header>

    <div class="container marketing">

        <div class="list-group flex-row flex-nowrap position-sticky w-100 border-bottom border-top mb-4" style="top: 0; background: white !important; z-index: 2; overflow-x: auto" id="list-faq">
            <a class="list-group-item list-group-item-action p-3 text-center border-right border-left-0  border-top-0 active" href="#faq-location">Location</a>
            <a class="list-group-item list-group-item-action p-3 text-center border-right" href="#faq-depot">Dépôts</a>
            <a class="list-group-item list-group-item-action p-3 text-center border-right" href="#faq-livraisons-retours">Livraisons et retours</a>
            <a class="list-group-item list-group-item-action p-3 text-center border-right" href="#faq-paiements-cautions">Paiements et cautions</a>
            <a class="list-group-item list-group-item-action p-3 text-center border-right-0" href="#faq-autres">Autres</a>
        </div>


        <div class="mb-5">
            <div style="scroll-margin-top: 70px;" id="faq-location">
                <h2 class="mb-5 h1">Location</h2>

                <div class="accordion" id="accordionLocation">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h2 class="clearfix mb-0">
                                <a class="trigger" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fa fa-chevron-circle-down"></i> COMMENT ÇA MARCHE ?</a>
                            </h2>
                        </div>
                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionLocation">
                            <div class="card-body">
                                <p>Vous avez prévu de sortir et vous n’avez toujours pas de tenue ? EasyDress est là pour vous aider à trouver votre tenue idéale. Il vous suffit tout simplement de sélectionner la date à laquelle vous souhaitez avoir votre tenue, puis vous aurez la sélection des pièces disponibles à cette date. <br>
                                    Votre commande est pour une seule et unique date. Pour différentes occasions, il faudra penser à faire des commandes distinctes.</p>
                                <p>Louez jusqu’à 3 tenues dans votre commande. Nous vous remboursons les articles non portés ! Si toutefois vous ne portez aucune des tenues, nous vous remboursons l'intégralité de la location excepté les frais de traitement liés aux frais de gestion de votre colis (20 Dt), et frais liés à une prolongation de location si vous avez choisi cette option (réduction de
                                    30%
                                    pour 6 jours, 40% pour 12 jours). Nous ne remboursons pas les accessoires loués.</p>
                                <p>Faîtes-vous plaisir et faîtes le choix qui vous plait !</p>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="fa fa-chevron-circle-down"></i> DOIS-JE NETTOYER LES VÊTEMENTS APRÈS LES AVOIR EMPRUNTÉS ?</a>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionLocation">
                            <div class="card-body"><p>Nettoyer les tenues après l’emprunt ? Vous pouvez easily dire non ! Ne vous inquiétez pas, tout est inclus dans notre service grâce à notre partenariat avec un pressing de qualité. Vous n’avez pas à vous soucier des tâches que vous auriez éventuellement, profitez de votre moment « dressy » !</p></div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class="fa fa-chevron-circle-down"></i>PUIS-JE EMPRUNTER PLUSIEURS PIÈCES À LA FOIS ?</a>
                            </h2>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionLocation">
                            <div class="card-body"><p>Vous pouvez commander jusqu’à trois modèles dans un même colis.</p></div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour"><i class="fa fa-chevron-circle-down"></i>COMBIEN DE TEMPS À L’AVANCE PUIS-JE RÉSERVER </a>
                            </h2>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionLocation">
                            <div class="card-body"><p>Pour les avant-gardistes, vous pouvez réserver jusqu'à quatre mois à l'avance. Et si vous préférez décider à la dernière minute, vous pouvez choisir la veille et vous recevrez votre tenue le lendemain avant 13h00 pour chaque commande passée avant 15h30 du lundi au vendredi.</p></div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour"><i class="fa fa-chevron-circle-down"></i>COMBIEN DE TEMPS JE PEUX GARDER LE COLIS ?</a>
                            </h2>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionLocation">
                            <div class="card-body"><p>Selon la date spécifiée, vous pouvez bénéficier de votre colis pendant 3, 6 ou 12 jours, à compter de la date de réception (sans week-ends et jours fériés). L'envoi doit nous être retourné le dernier jour de la location. Si vous avez envie de conserver la pièce plus longtemps, n'hésitez pas à nous contacter afin de trouver une solution.</p>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading5">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5"><i class="fa fa-chevron-circle-down"></i>
                                    COMMENT SAVOIR SI LE MODÈLE VA M’ALLER ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordionLocation">
                            <div class="card-body">
                                <p>Tous les articles affichés sur le site appartiennent à des particuliers. Ils peuvent avoir été retouchés et la taille indiquée sur l’étiquette n’est plus la taille réelle de la pièce. C'est la raison pour laquelle nous mesurons chaque article avant de le partager sur notre plateforme, et nous avons pensé à établir un guide de taille propre à "EasyDress", que vous
                                    trouverez sur chaque fiche produit au niveau de l'onglet "Tailles et coupes".</p>
                                <p>Nous vous offrons l’avantage d’essayage au prix de 20 Dt. Cette offre vous permet d’essayer jusqu'à 3 tenues devant votre miroir pendant 24h puis reprogrammer une location.</p>
                                <p>Vous pouvez commander jusqu'à 3 tenues distinctes et nous vous rembourserons les pièces non utilisées, à l'exception des frais liés à la prolongation de location, si vous avez sélectionné cette option. Si vous n'avez porté aucun des articles loués, nous garderons 20 dinars, ce qui correspond aux frais de traitement liés à l'expédition de votre colis.</p>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading6">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6"><i class="fa fa-chevron-circle-down"></i>
                                    QUE FAIRE SI LA TAILLE NE ME CONVIENT PAS ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordionLocation">
                            <div class="card-body">
                                <p>Vous trouverez une étiquette de sécurité sur le vêtement. Si la taille ne vous convient pas, ne coupez pas cette étiquette, elle servira de preuve que vous n'avez pas utilisé le vêtement et nous vous rembourserons. Nous vous recommandons de prévoir la réception de votre colis un ou deux jours avant la date à laquelle vous souhaitez les utiliser, afin que nous
                                    puissions vous envoyer rapidement un autre modèle si la taille n'est pas adéquate.</p>
                                <p>Cependant, si vous ne portez pas le vêtement, nous vous rembourserons l'intégralité de la location, à l'exception des frais de traitement d'expédition de votre colis (20 Dt) et des frais d'une prolongation de location si vous avez choisi cette option (30% de remise pour 6 jours, 40% pour 12 jours).</p>
                                <p>Nous ne remboursons pas les accessoires loués.</p>

                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading7">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7"><i class="fa fa-chevron-circle-down"></i>
                                    LA TAILLE INDIQUÉE SUR LE VÊTEMENT NE CORRESPOND PAS À CELLE AFFICHÉE SUR LE SITE ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordionLocation">
                            <div class="card-body">
                                <p>Tous les modèles du site appartiennent à des particuliers. Par conséquent, ils pourraient être retouchés et ne correspondent plus toujours à la taille indiquée. Ne soyez donc pas surpris si la taille sur l'étiquette ne correspond pas à votre taille lorsque vous recevez le vêtement, certains modèles sont grands ou petits et nous avons ajusté les tailles selon
                                    notre
                                    tableau de tailles que vous pouvez retrouver sur chaque feuille, dans l'onglet « Tailles et coupe ». Par conséquent, vous devez vous fier à la taille indiquée sur le site web et non à la taille indiquée sur le vêtement.</p>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading8">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8"><i class="fa fa-chevron-circle-down"></i>
                                    PUIS-JE ESSAYER LES MODÈLES ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordionLocation">
                            <div class="card-body">
                                <p>Nous vous proposons une formule d’essayage à 20 Dt qui vous permet d'avoir jusqu'à 3 modèles chez vous pendant 24h pour les tester puis reprogrammer une date de location. Vous avez la possibilité de sélectionner la formule d’essayage sur la page du produit, cliquez sur « essayer » et votre commande passera automatiquement à une formule d’essayage. Vous pouvez
                                    sélectionner jusqu'à trois tenues différentes.</p>
                                <p>Lorsque vous validez votre panier, vous ne payez que 20 Dt pour commander les 3 modèles. Vous les recevrez à votre domicile à la date que vous aurez choisie et vous devrez les retourner le lendemain. </p>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading9">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9"><i class="fa fa-chevron-circle-down"></i>
                                    QUE FAIRE SI JE CONSTATE UN DÉFAUT SUR UN MODÈLE QUE JE REÇOIS ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordionLocation">
                            <div class="card-body">
                                <p>Lors de la réception du colis, vous trouverez à l’intérieur une sorte d’inventaire du vêtement. Si vous n'êtes pas d'accord avec cet inventaire ou découvrez des défauts non répertoriés sur la fiche, ne coupez pas l'étiquette de sécurité qui servira de preuve pour le remboursement et prévenez-nous immédiatement par e-mail ou par téléphone.</p>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading10">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10"><i class="fa fa-chevron-circle-down"></i>
                                    QUE SE PASSE-T’IL SI J’ABÎME UNE PIÈCE ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordionLocation">
                            <div class="card-body">
                                <p>Ne vous inquiétez pas, nous nous occupons des petites réparations et nous avons un très bon partenaire de nettoyage qui s'occupe de la plupart des tâches. Si des frais de réparation importants sont à prévoir après des dommages importants, nous sommes tenus de prendre en charge votre caution conformément à la réparation du matériel. Cependant, si l'article est
                                    trop
                                    abîmé pour être loué, nous devrons facturer la valeur du vêtement (caution) et la tenue abîmée sera restituée à son propriétaire.</p>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading11">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11"><i class="fa fa-chevron-circle-down"></i>
                                    COMMENT LES PIÈCES SONT-ELLES NETTOYÉES ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordionLocation">
                            <div class="card-body">
                                <p>Chaque modèle récupéré sera envoyé au pressing et nettoyé suite au retour de la location. Nous nous occupons de le rendre impeccable avant de vous l'expédier.</p>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading13">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13"><i class="fa fa-chevron-circle-down"></i>
                                    PUIS-JE PROLONGER MA LOCATION ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#accordionLocation">
                            <div class="card-body">
                                <p>Vous pouvez choisir l'option prolongation lorsque vous passez une commande sur notre site :</p>
                                <ul>
                                    <li>30% de réduction sur le total pour l'option 6 jours</li>
                                    <li>40% de réduction sur le total pour l'option 12 jours</li>
                                </ul>
                                <p>Veuillez noter que ces frais supplémentaires s'appliquent à l‘ensemble de la commande. Si vous n'utilisez aucun des 3 articles loués, nous retiendrons 20 Dt, soit les frais de livraison, ainsi que les frais liés à la prolongation.</p>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading14">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapse14"><i class="fa fa-chevron-circle-down"></i>
                                    QUE SE PASSE-T’IL SI J’AI DU RETARD ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#accordionLocation">
                            <div class="card-body">
                                <p>Chaque jour de retard la somme de 20 Dt est facturée. Après une semaine de retard, vous serez facturé de la valeur des vêtements (caution). Quelqu'un a peut-être réservé la même pièce que vous peu de temps après, et sera très déçu s'il ne peut pas l'obtenir à temps pour son l'événement !</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="scroll-margin-top: 70px;" id="faq-depot">
                <h2 class="my-5 h1">DÉPÔTS</h2>
                <div class="accordion" id="accordionDepot">
                    <div class="card">
                        <div class="card-header" id="headingDOne">
                            <h2 class="clearfix mb-0">
                                <a class="trigger" data-toggle="collapse" data-target="#collapseDOne" aria-expanded="true" aria-controls="collapseDOne"><i class="fa fa-chevron-circle-down"></i> QUELS TYPES DE VÊTEMENTS PUIS-JE DÉPOSER ?</a>
                            </h2>
                        </div>
                        <div id="collapseDOne" class="collapse show" aria-labelledby="headingDOne" data-parent="#accordionDepot">
                            <div class="card-body">
                                <p>Nous acceptons toutes sortes de vêtements chics et élégants. Nous avons commencé avec des robes, mais maintenant vous pouvez laisser toutes sortes de vêtements et d’accessoires (bijoux, diadèmes, etc.) dans toutes les tailles. Nous acceptons également les tenues pour femmes enceintes. Nous faisons des choix très précis en termes de qualité, de style et de marque
                                    du
                                    vêtement, et nous n'acceptons pas les modèles achetés pour moins de 200 Dt (sauf pour certains articles très demandés). Assurez-vous que vos pièces sont en parfait état, n'ont pas de tâches ou de défauts avant de nous les envoyer.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingDTwo">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseDTwo" aria-expanded="false" aria-controls="collapseDTwo"><i class="fa fa-chevron-circle-down"></i> A QUEL MOMENT PUIS-JE TOUCHER MA COMMISSION ?</a>
                            </h2>
                        </div>
                        <div id="collapseDTwo" class="collapse" aria-labelledby="headingDTwo" data-parent="#accordionDepot">
                            <div class="card-body"><p>Les commissions seront versées à la fin de chaque mois dans votre fonds de collecte, dans votre espace personnel. Vous pouvez choisir de l'utiliser sur notre site ou de le déposer sur votre compte bancaire.</p></div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingDThree">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseDThree" aria-expanded="false" aria-controls="collapseDThree"><i class="fa fa-chevron-circle-down"></i> COMMENT PROPOSER UN MODÈLE EN LOCATION ?</a>
                            </h2>
                        </div>
                        <div id="collapseDThree" class="collapse" aria-labelledby="headingDThree" data-parent="#accordionDepot">
                            <div class="card-body"><p>On vous donne rendez-vous dans l'onglet « déposer » du site, et laissez-vous guider. On s’occupera de la sélection préliminaire des photos ainsi que des descriptions.</p></div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingDFour">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseDFour" aria-expanded="false" aria-controls="collapseDFour"><i class="fa fa-chevron-circle-down"></i> SOUS COMBIEN DE TEMPS PUIS-JE SAVOIR SI MES VÊTEMENTS SONT ACCEPTÉS ?</a>
                            </h2>
                        </div>
                        <div id="collapseDFour" class="collapse" aria-labelledby="headingDFour" data-parent="#accordionDepot">
                            <div class="card-body"><p>C’est très rapide ! Vous serez prévenu par email dans les jours suivants votre envoi en ligne.</p></div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingDFour">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseDFour" aria-expanded="false" aria-controls="collapseDFour"><i class="fa fa-chevron-circle-down"></i>COMBIEN EST-CE QUE JE GAGNE LORSQUE L’UN DE MES VÊTEMENTS EST EMPRUNTÉ ?</a>
                            </h2>
                        </div>
                        <div id="collapseDFour" class="collapse" aria-labelledby="headingDFour" data-parent="#accordionDepot">
                            <div class="card-body">
                                <p>Nous vous reversons 40% du prix de location hors taxes. Ce qui correspond à la grille ci-dessous :</p>
                                <table class="table table-striped table">
                                    <tr>
                                        <th>Prix de location</th>
                                        <th>Commission reversée</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            35,00 Dt
                                        </td>
                                        <td>11,35 Dt</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            40,00 Dt
                                        </td>
                                        <td>
                                            13,00 Dt
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>45,00 Dt</td>
                                        <td>14,60 Dt</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            50,00 Dt
                                        </td>
                                        <td>16,20 Dt</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            60,00 Dt
                                        </td>
                                        <td>
                                            19,44 Dt
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            65,00 Dt
                                        </td>
                                        <td>
                                            21,00 Dt
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            70,00 Dt
                                        </td>
                                        <td>
                                            22,68 Dt
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            75,00 Dt
                                        </td>
                                        <td>
                                            24,30 Dt
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            80,00 Dt
                                        </td>
                                        <td>
                                            25,92 Dt
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            85,00 Dt
                                        </td>
                                        <td>
                                            27,54 Dt
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            90,00 Dt
                                        </td>
                                        <td>
                                            29,16 Dt
                                        </td>
                                    </tr>
                                </table>
                                <p>En fonction de la période d'extension choisie entre 6 et 12 jours, votre commission est donc recalculée sur la base des journées ajoutées, si l'option est sélectionnée.</p>
                            </div>

                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingD5">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseD5" aria-expanded="false" aria-controls="collapseD5"><i class="fa fa-chevron-circle-down"></i>
                                    POURQUOI LES MODÈLES SONT-ILS CENTRALISÉS CHEZ EASY DRESS ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapseD5" class="collapse" aria-labelledby="headingD5" data-parent="#accordionDepot">
                            <div class="card-body">
                                <p>Pour vous faciliter la vie : une fois que nous recevrons vos tenues dans nos locaux, détendez-vous votre mission est accomplie ! On sera au petit soin, elles seront gâtées chez nous, et vous les récupérez au moment où vous en aurez besoin ! De cette façon, nous ferons de notre mieux de s’assurer que les articles que vous recevez soient toujours aussi impeccables
                                    et
                                    de faire en sorte que la location de vêtements soit disponible même pour ceux qui n'habitent pas dans les grandes villes. Ceci augmentera vos chances de louer vos tenues, car elles peuvent être expédiées partout en Tunisie.</p>
                            </div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header" id="headingD6">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseD6" aria-expanded="false" aria-controls="collapseD6"><i class="fa fa-chevron-circle-down"></i>
                                    AI-JE LE DROIT DE RÉCUPÉRER MES MODÈLES ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapseD6" class="collapse" aria-labelledby="headingD6" data-parent="#accordionDepot">
                            <div class="card-body">
                                <p>Nous concluons des contrats de trois mois renouvelables. Pendant les trois premiers mois, les vêtements doivent rester disponibles à la location. Après trois mois vous avez droit à un aller-retour par mois ou vous pouvez récupérer vos modèles dans nos bureaux si besoin. Si vous souhaitez récupérer vos modèles plus d'une fois par mois, nous vous facturerons les
                                    frais
                                    de port. Après l'expiration des trois premiers mois, vous pouvez retirer vos modèles de façon permanente à tout moment.</p>

                            </div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header" id="headingD7">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseD7" aria-expanded="false" aria-controls="collapseD7"><i class="fa fa-chevron-circle-down"></i>
                                    QUE SE PASSE-T'IL S’ILS SONT ABÎMÉS ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapseD7" class="collapse" aria-labelledby="headingD7" data-parent="#accordionDepot">
                            <div class="card-body">
                                <p>Lorsque vous déposez votre modèle, nous concluons un contrat de location dans lequel nous lui accordons une valeur de dépréciation. Si nous découvrons des dégâts importants ou un vol lors du retour d'une location, ce montant sera remboursé et le modèle pourra vous être restitué sur simple demande. Mais rassurez-vous, cela n'arrive presque jamais.</p>
                            </div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header" id="headingD8">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseD8" aria-expanded="false" aria-controls="collapseD8"><i class="fa fa-chevron-circle-down"></i>
                                    QUE SE PASSE-T-IL SI JE CONSTATE QUE MON ARTICLE EST ABÎMÉ LORS D'UN RETOUR DÉFINITIF ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapseD8" class="collapse" aria-labelledby="headingD8" data-parent="#accordionDepot">
                            <div class="card-body">
                                <p>Lorsque votre article vous sera finalement rendu, il sera accompagné de votre fiche d’inventaire. Si vous constatez une différence, vous disposez de 2 jours après réception de votre colis pour nous l’informer.</p>
                            </div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header" id="headingD9">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseD9" aria-expanded="false" aria-controls="collapseD9"><i class="fa fa-chevron-circle-down"></i>
                                    COMMENT SAVOIR QUE MON MODÈLE EST LOUÉ ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapseD9" class="collapse" aria-labelledby="headingD9" data-parent="#accordionDepot">
                            <div class="card-body">
                                <p>Dès qu'un de vos modèles est réservé, vous serez averti par email. Vous trouverez toutes les informations concernant vos locations dans votre espace personnel. </p>
                            </div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header" id="headingD10">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseD10" aria-expanded="false" aria-controls="collapseD10"><i class="fa fa-chevron-circle-down"></i>
                                    COMMENT EST-CE QUE JE VOUS ENVOIE MA TENUE ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapseD10" class="collapse" aria-labelledby="headingD10" data-parent="#accordionDepot">
                            <div class="card-body">
                                <p>Il y a deux solutions possibles : soit vous pouvez prendre rendez-vous pour la déposer à notre bureau (à Tunis) soit nous vennons la récupérer.</p>
                            </div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header" id="headingD11">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseD11" aria-expanded="false" aria-controls="collapseD11"><i class="fa fa-chevron-circle-down"></i>
                                    COMMENT ENTRETENEZ-VOUS NOS MODÈLES ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapseD11" class="collapse" aria-labelledby="headingD11" data-parent="#accordionDepot">
                            <div class="card-body">
                                <p>Soyez assurées que nous gâterons vos vêtements. Bouton à coudre, fermeture éclair à réparer... Nous veillons chaque jour à ce que vos tenues soient impeccables et nous nous efforçons de vous les rendre en parfait état que vous nous les avez conférées.</p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div style="scroll-margin-top: 70px;" id="faq-livraisons-retours">

                <h2 class="my-5 h1">LIVRAISONS ET RETOURS</h2>

                <div class="accordion" id="accordionLivraison">
                    <div class="card">
                        <div class="card-header" id="headingLOne">
                            <h2 class="clearfix mb-0">
                                <a class="trigger" data-toggle="collapse" data-target="#collapseLOne" aria-expanded="true" aria-controls="collapseLOne"><i class="fa fa-chevron-circle-down"></i> QUELLES SONT LES OPTIONS DE LIVRAISONS ?</a>
                            </h2>
                        </div>
                        <div id="collapseLOne" class="collapse show" aria-labelledby="headingLOne" data-parent="#accordionLivraison">
                            <div class="card-body">
                                <p>Toutes les livraisons sont effectuées par notre livreur professionnel, vous recevrez le colis à domicile en moins de 24h, dans tous les cas un transport aller-retour est offert à partir d’une valeur de 50 Dt de commande. En dessous de 50 Dt, les frais de port sont fixés à 8 Dt. Vous trouverez des informations sur le suivi et la traçabilité de votre colis dans
                                    votre
                                    espace personnel. Vous pouvez également retirer votre tenue au <b>3 rue Bagdad Cite Amal DenDen 2011</b>.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingLTwo">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseLTwo" aria-expanded="false" aria-controls="collapseLTwo"><i class="fa fa-chevron-circle-down"></i> QUEL EST LE TARIF DE LA LIVRAISON ?</a>
                            </h2>
                        </div>
                        <div id="collapseLTwo" class="collapse" aria-labelledby="headingLTwo" data-parent="#accordionLivraison">
                            <div class="card-body"><p>Les frais de livraison sont offerts pour toutes les commandes de 50 Dt ou plus. Pour les commandes inférieures à 50 Dt, les frais de port sont fixés à 8 Dt.</p></div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingLThree">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseLThree" aria-expanded="false" aria-controls="collapseLThree"><i class="fa fa-chevron-circle-down"></i> QUELS SONT LES DÉLAIS DE LIVRAISONS ?</a>
                            </h2>
                        </div>
                        <div id="collapseLThree" class="collapse" aria-labelledby="headingLThree" data-parent="#accordionLivraison">
                            <div class="card-body"><p>Si vous passez votre commande avant 15h30, vous recevrez le colis le lendemain avant 13h00. Si vous avez réservé une commande bien à l'avance, nous vous informerons dès que le colis aura été expédié et vous le recevrez le lendemain avant 13h00.</p></div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingLFour">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseLFour" aria-expanded="false" aria-controls="collapseLFour"><i class="fa fa-chevron-circle-down"></i> PUIS-JE ME FAIRE LIVRER LE SAMEDI ?</a>
                            </h2>
                        </div>
                        <div id="collapseLFour" class="collapse" aria-labelledby="headingLFour" data-parent="#accordionLivraison">
                            <div class="card-body"><p>Bien évidemment, mais il sera impératif d'ajouter un supplément de 7 Dt pour que vous puissiez recevoir le colis le samedi. Les livraisons standards s’effectueront généralement du lundi au vendredi.</p></div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingLFour">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseLFour" aria-expanded="false" aria-controls="collapseLFour"><i class="fa fa-chevron-circle-down"></i> COMMENT SE PASSE LE RETOUR ?</a>
                            </h2>
                        </div>
                        <div id="collapseLFour" class="collapse" aria-labelledby="headingLFour" data-parent="#accordionLivraison">
                            <div class="card-body">
                                <p>Ne vous inquiétez pas ! EasyDress s’en chargera de venir récupérer les pièces à la fin du délai de la location.</p>
                            </div>

                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingL5">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseL5" aria-expanded="false" aria-controls="collapseL5"><i class="fa fa-chevron-circle-down"></i>
                                    COMMENT SUIS-JE REMBOURSÉE DES TENUES NON PORTÉES ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapseL5" class="collapse" aria-labelledby="headingL5" data-parent="#accordionLivraison">
                            <div class="card-body">
                                <p>Toutes les tenues non utilisées seront entièrement remboursées au retour du colis, sauf si vous n'utilisez aucun des articles que vous avez commandés. Dans ce cas, nous retiendrons 20 Dt, ce qui correspond aux frais de traitement liés à l'expédition de votre colis et aux frais de blocage de la ou des tenues. Nous ne remboursons pas non plus les frais associés à
                                    une
                                    prolongation de location si vous avez sélectionné cette option lors de la commande. Le remboursement sera effectué dans les 48 heures après réception du colis, si le remboursement n'arrive pas à temps, vous pouvez nous contacter directement par téléphone ou email. Les accessoires loués ne sont pas remboursés.</p>
                            </div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header" id="headingL6">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseL6" aria-expanded="false" aria-controls="collapseL6"><i class="fa fa-chevron-circle-down"></i>
                                    QUE FAIRE SI MON EMPRUNT SE TERMINE UN DIMANCHE OU UN JOUR FÉRIÉ ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapseL6" class="collapse" aria-labelledby="headingL6" data-parent="#accordionLivraison">
                            <div class="card-body">
                                <p>Si le troisième jour de votre location est un jour férié ou un dimanche, vous pouvez nous le renvoyer le premier jour suivant cette date.</p>

                            </div>
                        </div>
                    </div>


                    <div class="card">
                        <div class="card-header" id="headingL7">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseL7" aria-expanded="false" aria-controls="collapseL7"><i class="fa fa-chevron-circle-down"></i>
                                    OÙ LIVREZ-VOUS ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapseL7" class="collapse" aria-labelledby="headingL7" data-parent="#accordionLivraison">
                            <div class="card-body">
                                <p>Nous livrons actuellement que sur la région du grand Tunis.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div style="scroll-margin-top: 70px;" id="faq-paiements-cautions">

                <h2 class="my-5 h1">PAIEMENTS ET CAUTIONS</h2>
                <div class="accordion" id="accordionPaiement">
                    <div class="card">
                        <div class="card-header" id="headingPOne">
                            <h2 class="clearfix mb-0">
                                <a class="trigger" data-toggle="collapse" data-target="#collapsePOne" aria-expanded="true" aria-controls="collapsePOne"><i class="fa fa-chevron-circle-down"></i> COMMENT PUIS-JE RÉGLER MES ACHATS ?</a>
                            </h2>
                        </div>
                        <div id="collapsePOne" class="collapse show" aria-labelledby="headingPOne" data-parent="#accordionPaiement">
                            <div class="card-body">
                                <p>Tous les achats et réservations sur notre site internet peuvent être effectués : en espèces (lors de la réception ou au showroom), par virement bancaire ou auprès de votre collecte de fonds.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingPTwo">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapsePTwo" aria-expanded="false" aria-controls="collapsePTwo"><i class="fa fa-chevron-circle-down"></i> PUIS-JE ANNULER UNE COMMANDE ?</a>
                            </h2>
                        </div>
                        <div id="collapsePTwo" class="collapse" aria-labelledby="headingPTwo" data-parent="#accordionPaiement">
                            <div class="card-body">
                                <p>Chaque réservation empêche une autre personne de réserver pour les mêmes dates. "EasyDress" offre au locataire la possibilité d'annuler la commande sans frais jusqu'à 14 jours avant l'expédition du vêtement / des accessoires.</p>
                                <p>Si la commande est annulée après l'expiration du délai légal de rétractation dans les 14 jours précédant la date d'expédition prévue du vêtement/accessoire, des frais d'annulation de 15Dt s'appliqueront. Exceptionnellement, ces tarifs ne s'appliquent pas si la commande est annulée dans les 48 heures suivant la passation de la commande.</p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div style="scroll-margin-top: 70px;" id="faq-autres">
                <h2 class="my-5 h1">Autres</h2>
                <div class="accordion" id="accordionAutre">
                    <div class="card">
                        <div class="card-header" id="headingAOne">
                            <h2 class="clearfix mb-0">
                                <a class="trigger" data-toggle="collapse" data-target="#collapseAOne" aria-expanded="true" aria-controls="collapseAOne"><i class="fa fa-chevron-circle-down"></i> Y’A T’IL DES MODÈLES DISPONIBLES À L’ACHAT ?</a>
                            </h2>
                        </div>
                        <div id="collapseAOne" class="collapse show" aria-labelledby="headingAOne" data-parent="#accordionAutre">
                            <div class="card-body">
                                <p>Pour le moment, nous ne proposons pas l'achat des modèles. Cependant, si vous êtes vraiment amoureux d'un vêtement emprunté, veuillez-nous en informer afin que nous puissions demander au propriétaire.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingATwo">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseATwo" aria-expanded="false" aria-controls="collapseATwo"><i class="fa fa-chevron-circle-down"></i> JE N’ARRIVE PAS À TÉLÉCHARGER LES PHOTOS DE MES VÊTEMENTS ?</a>
                            </h2>
                        </div>
                        <div id="collapseATwo" class="collapse" aria-labelledby="headingATwo" data-parent="#accordionAutre">
                            <div class="card-body"><p>Pas de crainte, nous avons toujours une solution pour vous ! Remplissez le formulaire sans ajouter les photos et envoyez les photos par mail à contact@easydress.tn avec votre nom/prénom et numéro de téléphone.</p></div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingAThree">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseAThree" aria-expanded="false" aria-controls="collapseAThree"><i class="fa fa-chevron-circle-down"></i> PUIS-JE VOUS ENVOYER DES PHOTOS DE MOI PORTANT LES MODÈLES ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapseAThree" class="collapse" aria-labelledby="headingAThree" data-parent="#accordionAutre">
                            <div class="card-body"><p>Mais quelle idée géniale ! Nous les partagerons sur nos réseaux sociaux pour aider les autres clients à choisir. Rien ne vaut une tenue portée sur quelqu'un d'autre juste pour l'imaginer. N'hésitez pas à compléter votre &nbsp;&nbsp; « showroom personnel » où vous pourrez télécharger des photos de vous portant les modèles, ainsi que vos précieux
                                    conseils sur la façon de les porter et/ou les accessoiriser.</p></div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingAFour">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseAFour" aria-expanded="false" aria-controls="collapseAFour"><i class="fa fa-chevron-circle-down"></i> COMMENT NOUS CONTACTER ?</a>
                            </h2>
                        </div>
                        <div id="collapseAFour" class="collapse" aria-labelledby="headingAFour" data-parent="#accordionAutre">
                            <div class="card-body">
                                <p>Vous pouvez nous contacter : </p>
                                <p>Notre équipe est à votre disposition pour vous aider et pour vous satisfaire.</p>
                                <ul>
                                    <li>Par téléphone au <a href="tel:+21626058025">00216 26 05 80 25</a> : du lundi au vendredi de 9h à 18h.</li>
                                    <li>Par mail à l'adresse suivante : <a href="mailto:contact@easydress.tn">contact@easydress.tn</a></li>
                                    <li>Via le chat : du lundi au vendredi de 9h à 18h. Notre équipe est à votre entière disposition pour vous aider et pour vous satisfaire.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingAFour">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseAFour" aria-expanded="false" aria-controls="collapseAFour"><i class="fa fa-chevron-circle-down"></i> J'AIMERAIS PARRAINER UNE AMIE !</a>
                            </h2>
                        </div>
                        <div id="collapseAFour" class="collapse" aria-labelledby="headingAFour" data-parent="#accordionAutre">
                            <div class="card-body">
                                <p>Rien de plus simple que de devenir parrain : RDV ici ! Vous pouvez facilement saisir les adresses e-mail de votre future filleule ! 5 Dt pour votre filleule à l'inscription = 5 Dt pour vous dès la première commande ! Offre illimitée... Alors n'hésitons pas et partageons !</p>
                            </div>

                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="headingA5">
                            <h2 class="mb-0">
                                <a class="trigger collapsed" data-toggle="collapse" data-target="#collapseA5" aria-expanded="false" aria-controls="collapseA5"><i class="fa fa-chevron-circle-down"></i>
                                    PUIS-JE POSTULER CHEZ EASY DRESS ?
                                </a>
                            </h2>
                        </div>
                        <div id="collapseA5" class="collapse" aria-labelledby="headingA5" data-parent="#accordionAutre">
                            <div class="card-body">
                                <p>Écrivez-nous à Team@easydress.tn. Nous nous ferons un plaisir d'examiner attentivement votre candidature.</p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>

</x-app-layout>
