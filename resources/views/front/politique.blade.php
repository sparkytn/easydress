<x-app-layout title="Politique de confidentialité">

    <header class="section-spacing mb-5 bg-secondary">
        <div class="container">
            <h1 class="mb-0 text-white">Politique de confidentialité pour Société location EASY DRESS</h1>
        </div>
    </header>

    <div class="container marketing">

        <div class="container">

            <p>Chez EasyDress, accessible depuis easydress.tn, l'une de nos principales priorités est la confidentialité de nos visiteurs. Ce document de politique de confidentialité contient des types d'informations qui sont collectées et enregistrées par EasyDress et comment nous les utilisons.</p>

            <p>Si vous avez des questions supplémentaires ou souhaitez plus d'informations sur notre politique de confidentialité, n'hésitez pas à nous contacter.</p>

            <p>Cette politique de confidentialité s'applique uniquement à nos activités en ligne et est valable pour les visiteurs de notre site Web en ce qui concerne les informations qu'ils partagent et/ou collectent dans EasyDress. Cette politique ne s'applique pas aux informations collectées hors ligne ou via des canaux autres que ce site Web. Notre politique de confidentialité a été créée avec l'aide du <a href="https://www.privacypolicygenerator.info">générateur gratuit de politique de confidentialité</a>.</p>

            <h2>Consentement</h2>

            <p>En utilisant notre site Web, vous consentez par la présente à notre politique de confidentialité et acceptez ses conditions.</p>

            <h2>Informations que nous collectons</h2>

            <p>Les informations personnelles que vous êtes invité à fournir, et les raisons pour lesquelles vous êtes invité à les fournir, vous seront précisées au moment où nous vous demanderons de fournir vos informations personnelles.</p>
            <p>Si vous nous contactez directement, nous pouvons recevoir des informations supplémentaires vous concernant telles que votre nom, votre adresse e-mail, votre numéro de téléphone, le contenu du message et/ou des pièces jointes que vous pouvez nous envoyer, et toute autre information que vous pouvez choisir de fournir .</p>
            <p>Lorsque vous créez un compte, nous pouvons vous demander vos coordonnées, y compris des éléments tels que le nom, le nom de l'entreprise, l'adresse, l'adresse e-mail et le numéro de téléphone.</p>

            <h2>Comment nous utilisons vos informations</h2>

            <p>Nous utilisons les informations que nous collectons de différentes manières, notamment pour :</p>

            <ul>
                <li>Fournir, exploiter et maintenir notre site Web</li>
                <li>Améliorer, personnaliser et développer notre site Web</li>
                <li>Comprendre et analyser la façon dont vous utilisez notre site Web</li>
                <li>Développer de nouveaux produits, services, fonctionnalités et fonctionnalités</li>
                <li>Communiquer avec vous, directement ou par l'intermédiaire de l'un de nos partenaires, y compris pour le service client, pour vous fournir des mises à jour et d'autres informations relatives au site Web, et à des fins de marketing et de promotion</li>
                <li>Vous envoyer des e-mails</li>
                <li>Détecter et prévenir les fraudes</li>
            </ul>

            <h2>Fichiers journaux</h2>

            <p>EasyDress suit une procédure standard d'utilisation des fichiers journaux. Ces fichiers enregistrent les visiteurs lorsqu'ils visitent des sites Web. Toutes les sociétés d'hébergement le font et font partie de l'analyse des services d'hébergement. Les informations collectées par les fichiers journaux incluent les adresses IP (protocole Internet), le type de navigateur, le fournisseur d'accès Internet (FAI), l'horodatage, les pages de renvoi/sortie et éventuellement le nombre de clics. Ceux-ci ne sont liés à aucune information personnellement identifiable. Le but de l'information est d'analyser les tendances, d'administrer le site, de suivre les mouvements des utilisateurs sur le site Web et de recueillir des informations démographiques.</p>

            <h2>Cookies et balises Web</h2>

            <p>Comme tout autre site Web, EasyDress utilise des "cookies". Ces cookies sont utilisés pour stocker des informations, y compris les préférences des visiteurs et les pages du site Web auxquelles le visiteur a accédé ou visité. Les informations sont utilisées pour optimiser l'expérience des utilisateurs en personnalisant le contenu de notre page Web en fonction du type de navigateur des visiteurs et/ou d'autres informations.</p>

            <p>Pour plus d'informations générales sur les cookies, veuillez lire <a href="https://www.generateprivacypolicy.com/#cookies">l'article sur les cookies sur le site Web Generate Privacy Policy</a>.</p>



            <h2>Politiques de confidentialité des partenaires publicitaires</h2>

            <P>Vous pouvez consulter cette liste pour trouver la politique de confidentialité de chacun des partenaires publicitaires d'EasyDress.</p>

            <p>Les serveurs publicitaires ou réseaux publicitaires tiers utilisent des technologies telles que les cookies, JavaScript ou les balises Web qui sont utilisées dans leurs publicités et liens respectifs qui apparaissent sur EasyDress, qui sont envoyés directement au navigateur des utilisateurs. Ils reçoivent automatiquement votre adresse IP lorsque cela se produit. Ces technologies sont utilisées pour mesurer l'efficacité de leurs campagnes publicitaires et/ou pour personnaliser le contenu publicitaire que vous voyez sur les sites Web que vous visitez.</p>

            <p>Notez qu'EasyDress n'a aucun accès ni aucun contrôle sur ces cookies qui sont utilisés par des annonceurs tiers.</p>

            <h2>Politiques de confidentialité des tiers</h2>

            <p>La politique de confidentialité d'EasyDress ne s'applique pas aux autres annonceurs ou sites Web. Ainsi, nous vous conseillons de consulter les politiques de confidentialité respectives de ces serveurs publicitaires tiers pour des informations plus détaillées. Il peut inclure leurs pratiques et instructions sur la façon de se retirer de certaines options. </p>

            <p>Vous pouvez choisir de désactiver les cookies via les options de votre navigateur. Pour connaître des informations plus détaillées sur la gestion des cookies avec des navigateurs Web spécifiques, vous pouvez les trouver sur les sites Web respectifs des navigateurs.</p>

            <h2>Droits de confidentialité CCPA (Ne vendez pas mes informations personnelles)</h2>

            <p>En vertu du CCPA, entre autres droits, les consommateurs californiens ont le droit de :</p>
            <p>Demander qu'une entreprise qui collecte les données personnelles d'un consommateur

        </div>


        <!-- /END THE FEATURETTES -->

    </div><!-- /.container -->

</x-app-layout>
