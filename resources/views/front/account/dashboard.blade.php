
<x-account-layout title="Mon Compte">
    @section('page-title', 'Moyens de paiement')
    @include('front.partials.page-header')

    <section class="section-spacing">
        <div class="container">


                <div class="row">
                    <!-- Sidebar-->
                    <x-front.account.sidebar-dashboard />
                    <!-- Content-->
                    <section class="col-lg-8 pt-lg-4 pb-4 mb-3">
                        <div class="pt-2 px-4 ps-lg-0 pe-xl-5">
                            <h2 class="h3 py-2">Dashboard</h2>


                        </div>
                    </section>
                </div>

        </div>
    </section>
</x-account-layout>
