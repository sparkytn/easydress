<x-account-layout title="Mes Commandes">
    @section('page-title', 'Historique des commandes')
    @include('front.partials.page-header')

    <section class="section-spacing">
        <div class="container">

            @if (Session::has('success_order'))
                <div class="my-4">
                    <div class="alert alert-warning">
                        <h3>Commande : <b class="text-secondary">#{{ session()->get('success_commande') }}</b></h3>
                        Pour valider votre commande, vous avez 2 jours pour procéder au paiement, <a href="{{ route('front.account.payment.methods') }}">consulter les moyens de paiement</a>, sinon votre commande sera automatiquement annulée.
                    </div>
                </div>
            @endif


            <div class="row">
                <!-- Sidebar-->
                <x-front.account.sidebar-dashboard/>
                <!-- Content-->
                <section class="col-lg-8 pt-lg-4 pb-4 mb-3">
                    <div class="pt-2 px-4 ps-lg-0 pe-xl-5">
                        <h2 class="h3 py-2">Historique</h2>

                        <ul class="nav nav-tabs mb-1 flex-grow-1" id="myTab" role="tablist">
                        @foreach($orders as $key => $order_group)
                                <li class="nav-item flex-grow-1" role="presentation">
                                    <button class="nav-link {{ $loop->first ? ' active': '' }} w-100 font-weight-bold small text-uppercase" id="status-tab-{{$key}}" data-toggle="tab" data-target="#status-{{$key}}" type="button" role="tab">
                                        <i class="fa fa-certificate" style="color:{{ \App\Models\StatusEvent::status_color($key -1)}}"></i>
                                        {{ \App\Models\StatusEvent::status_label($key -1)}} ({{ count($order_group) }})
                                    </button>
                                </li>

                        @endforeach
                            </ul>
                        <div class="tab-content" id="myTabContent">

                            @forelse($orders as $key =>$order_group)
                                <div class="tab-pane fade {{ $loop->first ? 'show active': '' }}" id="status-{{$key}}" role="tabpanel">
                                    @foreach($order_group as $order)

                                        <div class="card border mb-4">
                                            <!-- Card header -->
                                            <div class="card-header border-bottom bg-secondary d-md-flex justify-content-md-between align-items-center">
                                                <!-- Icon and Title -->
                                                <div class="d-flex align-items-center">
                                                    <div class="icon-lg bg-white text-primary rounded-circle flex-shrink-0">
                                                        <i class="fas fa-shopping-basket"></i>
                                                    </div>
                                                    <!-- Title -->
                                                    <div class="ml-3">
                                                        <h4 class="card-title mb-0 text-white">Commande : <span class="font-sans">#<b>{{ $order->code }}</b></span></h4>
                                                        <ul class="nav nav-divider small text-primary">
                                                            <li class="nav-item mr-3">Commande passée le {{ $order->created_at->format('Y-m-d H:i') }}</li>
                                                            <li class="nav-item">Période {{$order->period}} Jours</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="mt-2 mt-md-0 text-white">
                                                    <h3 class="font-sans text-white mb-0"><b>{{ $order->price }}DT</b></h3>
                                                    <p class="m-0">Caution : <b>{{ $order->deposit }} DT</b></p>
                                                </div>
                                            </div>

                                            <!-- Card body -->
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-sm-6 col-md-4 mb-4">
                                                        <span class="booking-card-label">Jour de livraison: </span>
                                                        <div class="mb-0">{{ $order->start_date->format('d/m/Y') }}</div>
                                                    </div>

                                                    <div class="col-sm-6 col-md-4 mb-3">
                                                        <span class="booking-card-label">Jour du retour</span>
                                                        <div class="mb-0">{{ $order->end_date->format('d/m/Y') }}</div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <span class="booking-card-label">Status</span>
                                                        <div class="mb-0"><span class="badge {{ $order->badge_class }} m-0 " style="font-size: 14px">{{ $order->currentStatus->name }}</span></div>
                                                    </div>
                                                    <div class="col-12">
                                                        <hr>
                                                    </div>
                                                    <div class="col-12">
                                                        <h3 class="text-primary">Adresse de Livraison :</h3>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <p>
                                                                    Contact: <strong>{{ @$order->deliveryAddress->contact_person }}</strong> <br>Téléphone: <strong> {{ @$order->deliveryAddress->contact_phone }}</strong>
                                                                </p>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <p>
                                                                    <b>{{ @$order->deliveryAddress->label }}</b>
                                                                    {{ @$order->deliveryAddress->street }} <br>
                                                                    {{ @$order->deliveryAddress->zip_code }} - {{ @$order->deliveryAddress->city }}
                                                                </p>
                                                            </div>
                                                        </div>


                                                    </div>

                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <h3 class="text-primary">Les Articles</h3>
                                                <div class="d-flex flex-row flex-wrap">
                                                    @foreach($order->items as $item)
                                                        <div class=" col-md-6 mb-3">
                                                            <div class="d-flex flex-row p-3 border bg-white">

                                                                @php
                                                                    $image = $item->product->image;

                                                                    if ($image) {
                                                                        $image_path = asset('storage/imgs/'. $image->file_name) ;
                                                                    } else {
                                                                        $image_path = asset('images/no-image.jpg');
                                                                    }
                                                                @endphp

                                                                <div class="mr-3"><img src="{{ $image_path }}" width="50px" alt=""></div>

                                                                <div>
                                                                    <h3 class="mb-1"><a class="small" href="{{ route('front.product.detail', $item->product) }}">{!!  $item->product->name  !!} Lorem ipsum dolor sit.</a></h3>
                                                                    <div class="">Péférence : <b>{{ $item->product->ref }}</b></div>
                                                                    <div class="">Brand : <b>{{$item->product->brand->name}}</b></div>
                                                                    <div class="">Prix : <b>{{ $item->price }}DT </b></div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach
                                </div>


                            @empty

                                <h3 class="text-primary mt-5 text-center">Pas de commande pour le moment </h3>

                            @endforelse
                        </div>


                    </div>
                </section>
            </div>

        </div>
    </section>
</x-account-layout>
