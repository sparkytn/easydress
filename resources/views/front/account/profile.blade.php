
<x-account-layout title="Mon Profil">
    @section('page-title', 'Mon Profil')
    @include('front.partials.page-header')

    <section class="section-spacing">
        <div class="container">


                <div class="row">
                    <!-- Sidebar-->
                    <x-front.account.sidebar-dashboard />
                    <!-- Content-->
                    <section class="col-lg-8 pt-lg-4 pb-4 mb-3">
                        <div class="pt-2 px-4 ps-lg-0 pe-xl-5">
                            <h2 class="h3 py-2">Profil</h2>
                            <form action="{{ route('front.account.profile.update') }}" method="post">
                                @csrf
                                @method('PATCH')
                                <input type="hidden" name="id" value="{{ $user->id }}">
                                <x-auth-validation-errors class="mb-4 alert alert-danger" :errors="$errors" />
                                <div class="row">
                                    <div class="col-sm-12 mb-4">
                                        <label class="form-label" for="dashboard-fn">Nom et prénom</label>
                                        <input class="form-control" type="text" name="name" value="{{ $user->name }}" required>
                                    </div>
                                    <div class="col-sm-12 mb-4">
                                        <label class="form-label" for="dashboard-ln">Adresse email</label>
                                        <input class="form-control" type="email" name="email" value="{{ $user->email }}" required>
                                    </div>
                                    <div class="col-sm-6 mb-4">
                                        <label class="form-label" for="dashboard-email">Téléphone</label>
                                        <input class="form-control" type="text" name="phone" value="{{ $user->phone }}" required>
                                    </div>
                                    <div class="col-sm-6 mb-4">
                                        <label class="form-label" for="dashboard-address">Ville</label>
                                        <input class="form-control" type="text" name="city" value="{{ $user->city }}">
                                    </div>
                                    <div class="col-12">
                                        <div class="d-sm-flex justify-content-end align-items-center">
                                            <button class="btn btn-primary mt-3 mt-sm-0" type="submit">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </section>
                </div>

        </div>
    </section>
</x-account-layout>
