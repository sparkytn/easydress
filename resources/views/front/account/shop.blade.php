
<x-account-layout title="Demande d'Ajout d'Arcticle">
    @section('page-title', 'Demande De Dépôts')
    @include('front.partials.page-header')

    <section class="section-spacing">
        <div class="container">


                <div class="row">
                    <!-- Sidebar-->
                    <x-front.account.sidebar-dashboard />
                    <!-- Content-->
                    <livewire:submitproduct-form />
                </div>

        </div>
    </section>
</x-account-layout>
