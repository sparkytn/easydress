<x-account-layout  title="Moyens De Paiement">
    @section('page-title', 'Moyens de paiement')
    @include('front.partials.page-header')
    <section class="section-spacing">
        <div class="container">

            <div class="row">
                <!-- Sidebar-->
                <x-front.account.sidebar-dashboard/>
                <section class="col-lg-8 pt-lg-5 pb-4 mb-3">
                    <div class="container">
                        <div class="card mb-3">
                            <div class="card-header">
                                <h3>Espèces ou par chèque</h3>
                            </div>
                            <div class="card-body">
                                <p>Vous avez la possibilité de payer en espèces ou par chèque à une de nos agences en appelant 70 168 620 pour prendre un rendez-vous :</p>
                                <p>12 Rue Sleim Ammar Ennasr II, 2037, Ariana - Tunisie <br>
                                    De Lundi au Vendredi, de 9H à 14H.</p>
                            </div>
                        </div>

                        <div class="card mb-3">
                            <div class="card-header">
                                <h3>Virement bancaire</h3>
                            </div>
                            <div class="card-body">
                                <p>Virement bancaire depuis votre banque / Versement bancaire depuis n'importe quelle agence ATTIJARI BANK ou bien STB.</p>
                                <p><b>ATTIJARI BANK </b><br>
                                    (Domiciliation : Agence Ennasr II) <br>
                                    RIB : 0402 1107 0079 6501 2355 <br>
                                    Titulaire du compte : foulen
                                </p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>
</x-account-layout>
