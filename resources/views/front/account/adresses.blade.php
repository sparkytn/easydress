<x-account-layout title="Mes Adresses">
    @section('page-title', 'Mes Adresses de livraison')
    @include('front.partials.page-header')

    <section class="section-spacing">
        <div class="container">

            <div class="row">
                <!-- Sidebar-->
                <x-front.account.sidebar-dashboard/>
                <!-- Content-->
                <section class="col-lg-8 pt-lg-4 pb-4 mb-3">
                    <div class="">
                        <h2 class="h3 py-2">Adresses de livraison</h2>
                        @if (session('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>Youpi !</strong> {{ session('success') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <script>

                            </script>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead>
                                <tr>
                                    <th>Nom du lieu</th>
                                    <th>Personne à contacter</th>
                                    <th>Adresse</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($adresses as $adr)
                                    <tr>
                                        <td class="py-3"><h4>{{ $adr->address_label }}</h4></td>
                                        <td class="py-3">
                                            <b>{{ $adr->contact_person }}</b> <br>
                                            {{ $adr->contact_phone }}
                                        </td>
                                        <td class="py-3">{{ $adr->street }}
                                            <br/> {{ $adr->zip_code }} - {{ $adr->city }}
                                        </td>
                                        <td class="py-3">

                                            <a class="btn-sm btn btn-secondary my-1" href="{{ route('front.account.address.edit', $adr->id) }}">
                                                <i class="fas fa-pen"></i>
                                            </a>
                                            <form action="{{ route('front.account.address.destroy', $adr->id) }}" method="post" class="d-inline-block">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn-sm btn btn-danger  my-1"
                                                        onclick="return confirm('Voulez vous vraiment supprimer cet enregistrement?')"
                                                >
                                                    <i class="fas fa-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr class=" py-2">
                                        <td colspan="5" class="py-3 text-center">
                                            <p class="text-primary">Vous n'avez pas ajouté une adresse de livraison
                                                encore</p>


                                        </td>
                                    </tr>
                                @endforelse


                                </tbody>
                            </table>
                        </div>
                        <div class="mt-4 text-center">
                            <a class="btn btn-primary" href="{{ route('front.account.address.add') }}">Ajouter
                                une nouvelle adresse</a>

                            <div class="mt-2">
                                <small>Les adresses de livraison seront utils lorsque vous passez une
                                    commande.</small>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </div>
    </section>
</x-account-layout>
