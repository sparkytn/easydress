<x-account-layout title="Mes Adresses">
    @section('page-title', 'Mes Adresses')
    @include('front.partials.page-header')

    <section class="section-spacing">
        <div class="container">

            <div class="row">
                <!-- Sidebar-->
                <x-front.account.sidebar-dashboard/>
                <!-- Content-->
                <section class="col-lg-8 pt-lg-4 pb-4 mb-3">
                    <div class="pt-2 px-4 ps-lg-0 pe-xl-5">
                        <h2 class="h3 py-2">Adresse de livraison</h2>
                        @if ($action == 'update')
                            <form action="{{ route('front.account.address.update', $address) }}" method="post">
                                <input type="hidden" name="id" value="{{ $address->id }}"/>
                                @else
                                    <form action="{{ route('front.account.address.store') }}" method="post">
                                        @endif

                                        @csrf
                                        <x-auth-validation-errors class="mb-4 alert alert-danger" :errors="$errors"/>
                                        <div class="col-md-12">
                                            <div class="form-row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="contact_person">Nom de la personne à contacter</label>
                                                        <input type="text" class="form-control form-control-small"
                                                               value="{{ old('contact_person', $address->contact_person ?? '') }}"
                                                               name="contact_person" id="contact_person"
                                                               placeholder="Personne a contacter" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="contact_phone">Téléphone</label>
                                                        <input type="tel" class="form-control"
                                                               value="{{ old('contact_phone', $address->contact_phone ?? '') }}"
                                                               name="contact_phone" id="contact_phone" placeholder="Téléphone" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="address_label">Nom du lieu</label>
                                                <input type="text" class="form-control form-control-large" name="address_label"
                                                       id="address_label" placeholder="Maison, Travail..."
                                                       value="{{ old('address_label', $address->address_label ?? '') }}" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="address">Rue</label>
                                                <input type="text" class="form-control form-control-large" name="street" id="street"
                                                       placeholder="Rue" value="{{ old('street', $address->street ?? '') }}" required>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="zip_code">Code Postal</label>
                                                        <input type="text" class="form-control form-control-small" name="zip_code"
                                                               id="zip_code" placeholder="Code postale"
                                                               value="{{ old('zip_code', $address->zip_code ?? '') }}" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="form-group">
                                                        <label for="city">Gouvernorat</label>
                                                        <select id="city" class=" form-control" required name="city">


                                                            <option selected disabled>Gouvernorat</option>
                                                            @if(isset($address))
                                                                <option value="Ariana" {{ $address->city == 'Ariana' ? 'selected' : '' }}>Ariana</option>
                                                                <option value="Béja" {{ $address->city == 'Béja' ? 'selected' : '' }}>Béja</option>
                                                                <option value="Ben Arous" {{ $address->city == 'Ben Arous' ? 'selected' : '' }}>Ben Arous</option>
                                                                <option value="Bizerte" {{ $address->city == 'Bizerte' ? 'selected' : '' }}>Bizerte</option>
                                                                <option value="Gabès" {{ $address->city == 'Gabès' ? 'selected' : '' }}>Gabès</option>
                                                                <option value="Gafsa" {{ $address->city == 'Gafsa' ? 'selected' : '' }}>Gafsa</option>
                                                                <option value="Jendouba" {{ $address->city == 'Jendouba' ? 'selected' : '' }}>Jendouba</option>
                                                                <option value="Kairouan" {{ $address->city == 'Kairouan' ? 'selected' : '' }}>Kairouan</option>
                                                                <option value="Kasserine" {{ $address->city == 'Kasserine' ? 'selected' : '' }}>Kasserine</option>
                                                                <option value="Kébili" {{ $address->city == 'Kébili' ? 'selected' : '' }}>Kébili</option>
                                                                <option value="La Manouba" {{ $address->city == 'La Manouba' ? 'selected' : '' }}>La Manouba</option>
                                                                <option value="Le Kef" {{ $address->city == 'Le Kef' ? 'selected' : '' }}>Le Kef</option>
                                                                <option value="Mahdia" {{ $address->city == 'Mahdia' ? 'selected' : '' }}>Mahdia</option>
                                                                <option value="Médenine" {{ $address->city == 'Médenine' ? 'selected' : '' }}>Médenine</option>
                                                                <option value="Monastir" {{ $address->city == 'Monastir' ? 'selected' : '' }}>Monastir</option>
                                                                <option value="Nabeul" {{ $address->city == 'Nabeul' ? 'selected' : '' }}>Nabeul</option>
                                                                <option value="Sfax" {{ $address->city == 'Sfax' ? 'selected' : '' }}>Sfax</option>
                                                                <option value="Sidi Bouzid" {{ $address->city == 'Sidi Bouzid' ? 'selected' : '' }}>Sidi Bouzid</option>
                                                                <option value="Siliana" {{ $address->city == 'Siliana' ? 'selected' : '' }}>Siliana</option>
                                                                <option value="Sousse" {{ $address->city == 'Sousse' ? 'selected' : '' }}>Sousse</option>
                                                                <option value="Tataouine" {{ $address->city == 'Tataouine' ? 'selected' : '' }}>Tataouine</option>
                                                                <option value="Tozeur" {{ $address->city == 'Tozeur' ? 'selected' : '' }}>Tozeur</option>
                                                                <option value="Tunis" {{ $address->city == 'Tunis' ? 'selected' : '' }}>Tunis</option>
                                                                <option value="Zaghouan" {{ $address->city == 'Zaghouan' ? 'selected' : '' }}>Zaghouan</option>
                                                            @else
                                                                <option value="Ariana">Ariana</option>
                                                                <option value="Béja">Béja</option>
                                                                <option value="Ben Arous">Ben Arous</option>
                                                                <option value="Bizerte">Bizerte</option>
                                                                <option value="Gabès">Gabès</option>
                                                                <option value="Gafsa">Gafsa</option>
                                                                <option value="Jendouba">Jendouba</option>
                                                                <option value="Kairouan">Kairouan</option>
                                                                <option value="Kasserine">Kasserine</option>
                                                                <option value="Kébili">Kébili</option>
                                                                <option value="La Manouba">La Manouba</option>
                                                                <option value="Le Kef">Le Kef</option>
                                                                <option value="Mahdia">Mahdia</option>
                                                                <option value="Médenine">Médenine</option>
                                                                <option value="Monastir">Monastir</option>
                                                                <option value="Nabeul">Nabeul</option>
                                                                <option value="Sfax">Sfax</option>
                                                                <option value="Sidi Bouzid">Sidi Bouzid</option>
                                                                <option value="Siliana">Siliana</option>
                                                                <option value="Sousse">Sousse</option>
                                                                <option value="Tataouine">Tataouine</option>
                                                                <option value="Tozeur">Tozeur</option>
                                                                <option value="Tunis">Tunis</option>
                                                                <option value="Zaghouan">Zaghouan</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <button class="btn btn-primary btn-sm">Enregistrer</button>
                                        </div>
                                    </form>
                    </div>
                </section>
            </div>

        </div>
    </section>
</x-account-layout>
