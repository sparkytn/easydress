<x-account-layout title="Mes Dépôts">
    @section('page-title', 'Mes Dépôts')
    @include('front.partials.page-header')

        <section class="section-spacing">
            <div class="container">



                <div class="row">
                    <!-- Sidebar-->
                    <x-front.account.sidebar-dashboard />
                    <!-- Content-->
                    <section class="col-lg-8 pt-lg-4 pb-4 mb-3">
                        <div class="pt-2 px-4 ps-lg-0 pe-xl-5">
                            <h2 class="h3 py-2 d-inline-block mr-4">Mes Articles </h2> <a class="btn btn-primary btn-sm small" href="{{ route('front.account.my-shop.add_product') }}">+ Ajouter un produit</a>
                            <div class="table-responsive ">
                                <table class="table table-hover mb-0 small w-100">
                                    <tr>
                                        <th></th>
                                        <th>Nom</th>
                                        <th>Categorie / Marque</th>
                                        <th>Prix</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                    @forelse($my_products as $product)

                                        <tr>
                                            <td class="py-3">
                                                @if($product->img1)
                                                <img src="{{ asset('storage/images/submited_products/'.$product->img1) }}" width="100px" class="img-fluid img-thumbnail float-left" />
                                                @elseif ($product->image)
                                                    <img src="{{ asset('storage/imgs/gallery_thumb_'.$product->image->file_name) }}" width="100px" class="img-fluid" alt="">
                                                @else
                                                    <img src="{{ asset('images/no-image.jpg') }}" width="100px" class="img-fluid" alt="">
                                                @endif
                                            </td>
                                            <td class="py-3">
                                                <h5>{{ $product->name }}</h5>
                                                <i>Ajouter le:  {{ $product->created_at->format('d/m/Y H:i') }}</i>
                                            </td>
                                            <td class="py-3">
                                                    Catégorie : {{ $product->category->name }}
                                                <br>
                                                @if(get_class($product) === 'App\Models\SubmitedProduct')
                                                    Marque : {{ $product->marque }}
                                                @else
                                                    Marque : {{ $product->brand->name }}
                                                @endif
                                            </td>
                                            <td class="py-3"> Prix de location :  {{ $product->rent_price }} DT
                                                <br>
                                                Prix achat: <b>{{ $product->value_price }} DT</b>
                                            </td>
                                            <td>
                                                @if(isset($product->migrated))
                                                    @if ($product->is_accepted)
                                                        <span class="badge badge-success">Validé</span>
                                                    @else
                                                        <span class="badge badge-secondary">Pas encore validé</span>
                                                    @endif
                                                @else
                                                    @if($product->is_online)
                                                        <a href="{{ route('front.product.detail',$product) }}">Voir catalogue</a>
                                                    @else
                                                        <span class="badge badge-success">Validé</span> <br>
                                                        <span class="badge badge-info">N'est pas en ligne</span>
                                                    @endif
                                                @endif
                                            </td>
                                            <td>
                                                @if(get_class($product) === 'App\Models\SubmitedProduct')
                                                    @if (!$product->migrated && !$product->is_accepted)
                                                        <a href="{{ route('front.account.my-shop.delete_product',$product->id) }}"  onclick="return confirm('voulez vous vraiment supprimer cet article?')"><i class="fa-trash fa" ></i></a>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @empty
                                        <tr class="text-center">
                                            <td colspan="6">
                                                <h3 class="text-primary mt-5">Pas d'articles </h3>
                                                <p>Vous pouvez ajouter un nouveau article en cliquant <a href="{{ route('front.account.my-shop.add_product') }}">ici</a></p>
                                            </td>
                                        </tr>
                                    @endforelse


                            </table>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </section>
    </x-account-layout>
