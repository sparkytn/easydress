<x-account-layout  title="Checkout">
    @section('page-title', 'Checkout')
    @include('front.partials.page-header')

    <section class="section-spacing">
        <div class="container">
            <form action="{{ route('front.save_order') }}" method="post">
                @csrf
                <h3 class="text-primary">2 Lieu de livraison</h3>
                <div class="list-group">
                    {{-- <div class="list-group-item"> --}}
                    {{-- <div class="list-group-item-heading"> --}}
                    {{-- <div class="row radio"> --}}
                    {{-- <div class="col-md-2"> --}}
                    {{-- <label> --}}
                    {{-- <input type="radio" name="optionShipp" id="optionShipp1" value="option2"> --}}
                    {{-- 1509 Latona St --}}
                    {{-- </label> --}}
                    {{-- </div> --}}
                    {{-- <div class="col-md-9"> --}}
                    {{-- <dl class="dl-small"> --}}
                    {{-- <dt>Miguel Perez</dt> --}}
                    {{-- <dd>1509 Latona St, Philadelphia, PA 19146 </dd> --}}
                    {{-- </dl> --}}
                    {{-- </div> --}}
                    {{-- </div> --}}
                    {{-- </div> --}}
                    {{-- </div> --}}
                    <div class="list-group-item">
                        <div class="list-group-item-heading">
                            <div class="row">
                                {{-- <div class="col-md-2"> --}}
                                {{-- <div class="radio"> --}}
                                {{-- <label> --}}
                                {{-- <input type="radio" name="optionShipp" id="optionShipp2" value="option2" checked> --}}
                                {{-- A new address --}}
                                {{-- </label> --}}
                                {{-- </div> --}}
                                {{-- </div> --}}
                                <div class="col-md-12">
                                    {{-- <form role="form" class=""> --}}

                                    @if ($delivery_addresses != null && $delivery_addresses->count() > 0)
                                        <div class="form-group">
                                            <h6>Choisir une adresse de livraison</h6>
                                            <select name="delivery_address" class="form-control">
                                                <option value="">
                                                    Selectionnez...
                                                </option>
                                                @foreach ($delivery_addresses as $adr)
                                                    <option value="{{ $adr->id }}">
                                                        {{ $adr->address_label }}
                                                    </option>
                                                @endforeach
                                            </select>
                                            <hr class="w-100">
                                        </div>
                                        <h6 class="{{ $delivery_addresses->count() > 0 ? 'collapsed' : '' }} bg-tertiary text-white p-2 btn-sm"
                                            data-toggle="collapse" data-target="#collapseAddrForm" aria-expanded="true"
                                            aria-controls="collapseAddrForm"> Entrez une nouvelle adresse de livraison
                                        </h6>
                                    @endif
                                    <div class="collapse {{ ($delivery_addresses != null && $delivery_addresses->count() > 0) ? '' : 'show' }}"
                                        id="collapseAddrForm">
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="contact_person">Nom de la personne a contacter</label>
                                                    <input type="text" class="form-control form-control-small"
                                                        value="{{ auth()->user()->name }}" name="contact_person"
                                                        id="contact_person" placeholder="Personne a contacter">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="contact_phone">Téléphone</label>
                                                    <input type="text" class="form-control"
                                                        value="{{ auth()->user()->phone }}" name="contact_phone"
                                                        id="contact_phone" placeholder="Téléphone">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="address_label">Nom du lieu</label>
                                            <input type="text" class="form-control form-control-large"
                                                name="address_label" id="address_label"
                                                placeholder="Maison, Travail...">
                                        </div>
                                        <div class="form-group">
                                            <label for="address">Rue</label>
                                            <input type="text" class="form-control form-control-large" name="address"
                                                id="address" placeholder="Adresse">
                                        </div>
                                        {{-- <div class="form-group"> --}}
                                        {{-- <label for="inputAddress2">Street address 2</label> --}}
                                        {{-- <input type="text" class="form-control form-control-large" id="inputAddress2" placeholder="Enter address"> --}}
                                        {{-- </div> --}}
                                        <div class="form-row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="zip_code">Code Postale</label>
                                                    <input type="text" class="form-control form-control-small"
                                                        name="zip_code" id="zip_code" placeholder="Code postale">
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label for="city">Gouvernorat</label>
                                                    <input type="text" class="form-control" name="city" id="city"
                                                        placeholder="Gouvernorat">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="form-group"> --}}
                                    {{-- <label for="inputState" class="control-label">State</label> --}}
                                    {{-- <select class="form-control form-control-large"> --}}
                                    {{-- <option>Select state</option> --}}
                                    {{-- </select> --}}
                                    {{-- </div> --}}
                                    {{-- </form> --}}
                                    {{-- <button class="btn btn-primary btn-sm">Save Address</button> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="mt-5 text-center">
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. <br>Culpa
                        delectus
                        explicabo incidunt laboriosam modi nobis quasi quia, sit suscipit voluptas!</p>
                    <button class="btn btn-primary">Confirmer ma commande</button>
                </div>
            </form>
        </div>
    </section>
</x-account-layout>
