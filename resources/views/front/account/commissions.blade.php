<x-account-layout title="Mes Commissions">
    @section('page-title', 'Mes commissions')
        @include('front.partials.page-header')

        <section class="section-spacing">
            <div class="container">

                <div class="row">
                    <!-- Sidebar-->
                    <x-front.account.sidebar-dashboard />
                    <!-- Content-->
                    <section class="col-lg-8 pt-lg-4 pb-4 mb-3">
                        <div class="">
                            <h2 class="h3 py-2">Mes commissions</h2>
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                <thead>
                                    <tr>
                                        <th>No Commande</th>
                                        <th>Montant</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($commissions as $commission)
                                        <tr>
                                            <td class="py-3"><h4>{{ $commission->booking->code }}</h4></td>
                                            <td class="py-3">{{ $commission->amount }} DT</td>
                                            <td class="py-3">
                                                @if($commission->status =="1") Pas encore payée @endif
                                                @if($commission->status =="2") Payée le: {{ $commission->payment_date->translatedFormat('d/m/Y') }}@endif
                                            </td>

                                        </tr>
                                    @empty
                                        <tr class=" py-2">
                                            <td colspan="5" class="py-3 text-center">
                                                <p class="text-primary">Vous n'avez pas encore de commission</p>

                                            </td>
                                        </tr>
                                    @endforelse


                                </tbody>
                            </table>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </section>
    </x-account-layout>
