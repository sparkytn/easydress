<section class="hero-home">
    <div class="swiper-container hero-slider dark-overlay">
        <div class="swiper-wrapper">
            <div class="swiper-slide" style="background-image:url({{ asset('images/slide1.jpg') }})"></div>
            <div class="swiper-slide" style="background-image:url({{ asset('images/slide2.jpg') }})"></div>
            <div class="swiper-slide" style="background-image:url({{ asset('images/slide4.jpg') }})"></div>
        </div>
    </div>
    <div class="container py-6 py-md-7 h-100">
        <div class="row justify-content-center align-content-end h-100 pb-100">
            <div class="col-xl-10 mb-5">
                <div class="text-center text-white">
                    <p class="subtitle subtitle-center text-white">JE CHOISIS MA TENUE ET RÉSERVE MA DATE</p>
{{--                    <h1 class="display-3 text-shadow">Location de vêtement</h1>--}}
                </div>
                <div class="search-bar mt-2 bg-white text-left">
                    <form action="{{ route('front.home.filter') }}" method="post">
                        @csrf
                        <div class="container form-container p-lg-0">
                            <div class="row">
                                <div class="col-lg-3 col-md-12 mb-2 mb-lg-0 box box-first px-0 pb-2 pt-2 p-lg-0 ">
                                    <div class="d-flex h-100">
                                        <div class="px-2 pt-1 bg-primary text-white"><i class="fas fa-tshirt"></i></div>
                                        <select class="selectpicker  w-100" id="" title="Type de vêtement" name="category">
                                            @foreach($allCategories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6  mb-2 mb-lg-0 box  px-0 pb-2 pb-lg-0">
                                    <div class="d-flex h-100">
                                        <div class="px-2 pt-1 bg-primary text-white"><i class="fas fa-ruler"></i></div>
                                        <select class="selectpicker w-100" name="size" title="La taille">
                                            @foreach($all_sizes as $size)
                                                <option value="{{ $size->id }}">{{ $size->value }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                                <div class="col-lg-3 col-md-6  box box-last  px-0 pb-2 pb-lg-0">
                                    <div class="d-flex h-100">
                                        <div class="px-2 pt-1 bg-primary text-white"><i class="fas fa-calendar"></i></div>
                                        <input type='text' name="start_date"  class="form-control border-0 date-input" data-language='fr' placeholder="Date de début" autocomplete="off" />
                                    </div>
                                </div>
                                <div class="col-lg-3 px-0">
                                    <button class="btn btn-primary btn-block btn-sm h-100 rounded-0" type="submit"> Chercher</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
