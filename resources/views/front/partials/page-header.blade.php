<header class="page-header section-spacing bg-secondary text-white">
    <div class="container text-center">
            <h1 class="mb-0 text-uppercase"> @yield('page-title')</h1>
    </div>
</header>
