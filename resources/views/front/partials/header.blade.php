<header class="bg-light navbar-sticky">
    <div class="navbar navbar-expand-lg navbar-light">
        <div class="container">

            {{-- <a class="navbar-brand d-none d-sm-block flex-shrink-0 me-4 order-lg-1 h5 m-0" href="/"> --}}
            {{-- <span class="text-primary">Easy</span><span class="text-secondary">Dress</span> --}}
            {{-- </a> --}}

            <a class="navbar-brand me-2 order-lg-1" href="/">
                <img src="{{ asset('images/EasyDress-logo.svg') }}" alt="Easy Dress">
            </a>

            <!-- Toolbar-->
            <div class="navbar-toolbar d-flex align-items-center order-lg-3">

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse">
                    <span class="navbar-toggler-icon"></span>
                </button>

                {{-- <a class="navbar-tool d-none d-lg-flex" href="javascript:void(0)" data-toggle="collapse" data-target="#searchBox" role="button" aria-expanded="false" aria-controls="searchBox"> --}}
                {{-- <span class="navbar-tool-tooltip">Search</span> --}}
                {{-- <div class="navbar-tool-icon-box"><i class="fas fa-search"></i></div> --}}
                {{-- </a> --}}
                {{-- <a class="navbar-tool d-none d-lg-flex" href="dashboard-favorites.html"> --}}
                {{-- <span class="navbar-tool-tooltip">Favorites</span> --}}
                {{-- <div class="navbar-tool-icon-box"><i class="fas fa-heart"></i></div> --}}
                {{-- </a> --}}
                <div class="navbar-tool mr-3">
                    <a class="navbar-tool-icon-box bg-secondary dropdown-toggle" href="{{ route('front.showcart') }}">
                        <span class="navbar-tool-label">
                            {{ session()->get('total_items') ?? 0 }}
                        </span>
                        <i class="fas fa-shopping-basket"></i>
                    </a>
                    <a class="navbar-tool-text" href="{{ route('front.showcart') }}"><small>Mon Panier</small> {{ session()->get('total_amount') ?? 0 }} DT</a>
                </div>
                @guest
                    <div class="navbar-tool ml-3">
                        <a class="navbar-tool-icon-box bg-secondary" href="{{ route('login') }}">
                            <i class="fas fa-user"></i>
                        </a>
                        <a class="navbar-tool-text" href="{{ route('front.account.dashboard') }}">
                            <small>Bienvenue,</small>
                            <span class="font-weight-bold uppercase text-primary">Connectez vous</span>
                        </a>
                    </div>
                @endguest
                @auth

                    <div class="navbar-tool dropdown ml-2">

                        <a href="{{ route('front.account.dashboard') }}" class="navbar-tool-icon-box bg-secondary"><i class="fas fa-user"></i></a>
                        <a class="navbar-tool-text" href="{{ route('front.account.dashboard') }}">
                            <small>Bienvenue</small>
                            <span class="font-weight-bold uppercase text-primary">{{ Auth::user()->name }}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end">
                            <div style="min-width: 14rem;">
                                <p class="dropdown-header font-weight-bolder text-uppercase">Mon Compte</p>
                                <a class="dropdown-item d-flex align-items-center"
                                    href="{{ route('front.account.profile') }}"><i class="fas fa-id-card mr-2"></i>Mon
                                    Profil</a>
                                <a class="dropdown-item d-flex align-items-center"
                                    href="{{ route('front.account.addresses') }}"><i
                                        class="fas fa-map-marker-alt mr-2"></i>Mes Adresses</a>
                                <a class="dropdown-item d-flex align-items-center"
                                    href="{{ route('front.account.order.history') }}"><i
                                        class="fas fa-history mr-2"></i>Historique</a>
                                <div class="dropdown-divider"></div>
                                <p class="dropdown-header font-weight-bolder text-uppercase">Ma Boutique</p>

                                <a class="dropdown-item d-flex align-items-center"
                                    href="{{ route('front.account.my-shop') }}"><i class="fas fa-store mr-2"></i> Mes
                                    Dépôt</a>
                                <a class="dropdown-item d-flex align-items-center"
                                    href="{{ route('front.account.my-shop.my-commissions') }}"><i
                                        class="fas fa-money-bill mr-2"></i>Mes Commissions</a>

                                <div class="dropdown-divider"></div>
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <button class="dropdown-item d-flex align-items-center text-danger" type="submit"><i
                                            class="fas fa-sign-out-alt mr-2"></i>Sign Out</button>
                                </form>
                            </div>
                        </div>
                    </div>
                @endauth

            </div>


            <div class="collapse navbar-collapse me-auto order-lg-2" id="navbarCollapse">
                <!-- Search-->
                {{-- <div class="input-group d-lg-none my-3"><i class="ci-search position-absolute top-50 start-0 translate-middle-y text-muted fs-base ms-3"></i> --}}
                {{-- <input class="form-control rounded-start" type="text" placeholder="Search marketplace"> --}}
                {{-- </div> --}}
                <!-- Categories dropdown-->
                <ul class="navbar-nav navbar-mega-nav pr-lg-2 mr-lg-2 ml-lg-3 border-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle font-weight-bolder text-uppercase" href="#"
                            data-toggle="dropdown">Emprunter</a>
                        <div class="dropdown-menu py-1">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="dropdown-header text-uppercase font-weight-bold text-dark">Type de vêtements</div>
                                    <a class="dropdown-item text-uppercase" href="{{ route('front.shop') }}">Voir Tout</a>
                                    @foreach ($allCategories as $category)
                                        <a class="dropdown-item text-uppercase"
                                            href="{{ route('front.shop', $category) }}">{{ $category->name }}</a>
                                    @endforeach
                                </div>
                                <div class="col-md-6 d-none">
                                    <div class="dropdown-header text-uppercase font-weight-bold">Les marques</div>
                                    <a class="dropdown-item text-uppercase" href="{{ route('front.shop') }}">Voir
                                        Tout</a>
                                    @foreach ($allbrands as $brand)
                                        <a class="dropdown-item text-uppercase"
                                            href="{{ route('front.shop', $brand) }}">{{ $brand->name }}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="nav-item"><a href="{{ route('front.account.my-shop.add_product') }}"
                            class="nav-link text-uppercase font-weight-bold">Déposer</a></li>
                    <li class="nav-item"><a href="{{ route('front.le-concept') }}"
                            class="nav-link text-uppercase font-weight-bold">Le Concept</a></li>
                    <li class="nav-item"><a href="{{ route('front.faq') }}"
                            class="nav-link text-uppercase font-weight-bold">faq</a></li>
                </ul>
                <!-- Primary menu-->

            </div>
        </div>
    </div>


    <div class="search-box collapse" id="searchBox">
        <div class="card pt-2 pb-4 border-0 rounded-0">
            <div class="container">
                <div class="input-group"><i
                        class="ci-search position-absolute top-50 start-0 translate-middle-y text-muted fs-base ms-3"></i>
                    <input class="form-control rounded-start" type="text" placeholder="Search marketplace">
                </div>
            </div>
        </div>
    </div>
</header>
