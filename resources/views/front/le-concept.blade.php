<x-app-layout title="Le concept">

    <header class="section-spacing bg-accent">
        <div class="container text-center">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <h1 class="mb-3 text-secondary text-uppercase">COMMENT ÇA MARCHE ?</h1>
                    <p class="text-black">La mode c’est s’habiller conformément au goût d’une époque. Et d’une époque à une autre nos goûts changent ! Soyez libres, libres de tout oser, libres de changer d’avis tous les soirs ! Avec EasyDress  c’est easy de choisir une tenue exceptionnelle juste pour la soirée.</p>
                </div>
            </div>

        </div>
    </header>

    <div class=" marketing">
        <div id="location" class=" section-spacing">

            <div class="container">
                <div class="section__header text-center mb-5">
                    <p class="subtitle subtitle-center">LIBRE DE CHANGER D’AVIS TOUS LES SOIRS !</p>
                    <h2 class="title h1">Les locations</h2>
                </div>

                <div class="liste-etapes">

                    <div class="row">

                        <div class="etape une equilibre col-md-6 text-center" style="min-height: 401px;">
                            <h4 class="text-primary">1.	Choisir le Look qui vous convient </h4>
                            <p>Je profite de ma liberté avec EasyDress pour choisir de nouveaux looks. Peu importe la date de votre évènement, vous pouvez réserver votre tenue à la dernière minute (la veille) ou anticiper 3 mois avant.</p>

                            <div class="image">
                                <img src="{{ asset('images/concept') }}/concept-1.jpg?v30" alt="">
                            </div>

                        </div>

                        <div class="etape une equilibre col-md-6 text-center" style="min-height: 401px;">
                            <h4 class="text-primary">2.	Livraison assurée </h4>
                            <p>Vous aurez votre tenue jusqu’à chez vous en moins de 24h. Vous pouvez choisir de vous faire livrer à l’endroit que vous souhaiteriez. Transport aller-retour garanti par EasyDress.</p>

                            <div class="image">
                                <img src="{{ asset('images/concept') }}/concept-2.jpg?v30" alt="">
                            </div>
                        </div>

                        <div class="etape une equilibre col-md-6 text-center" style="min-height: 401px;">
                            <h4 class="text-primary">3.	J’enfile ma jolie tenue</h4>
                            <p>Être la plus belle pendant 3, 6 ou 12 jours au choix, selon vos dates de réservation. </p>

                            <div class="image">
                                <img src="{{ asset('images/concept') }}/concept-3.jpg?v30" alt="">
                            </div>
                        </div>

                        <div class="etape une equilibre col-md-6 text-center" style="min-height: 401px;">
                            <h4 class="text-primary">4. Je renvoie l’article</h4>
                            <p>Ne vous inquiétez pas ! EasyDress s’en chargera de venir récupérer les pièces à la fin du délai de la location.
                            </p>
                            <div class="image">
                                <img src="{{ asset('images/concept') }}/concept-4.jpg?v30" alt="">
                            </div>

                        </div>

                        <div class="col-md-3"></div>

                        <div class="etape une equilibre col-md-6 text-center" style="min-height: 401px;">
                            <h4 class="text-primary">5.	Le pressing est offert </h4>
                            <p>Nous veillerons à ce que vos pièces resteront impeccables ! Un service de nettoyage professionnel et écologique est assuré avant et après la location. </p>

                            <div class="image">
                                <img src="{{ asset('images/concept') }}/concept-5.jpg?v30" alt="">
                            </div>

                        </div>
                    </div>

                </div>
                <div class="text-center mt-5">
                    <p class="strong">POUR DÉCOUVRIR NOTRE SERVICE AVEC PLUS DE PRÉCISION, RENDEZ-VOUS SUR NOTRE <a class="link-click_conceptFaq" href="{{ route('front.faq') }}">FAQ</a> !</p>

                    <div class="button-large">
                        <a class="btn btn-primary link-click_conceptShowTenues" href="/emprunter/">Voir les tenues</a>

                    </div>
                </div>



            </div>

        </div>
        <div id="depots" class="bg-secondary text-white section-spacing">

                <div class="container">
                    <div class="section__header text-center mb-5 ">
                        <p class="subtitle subtitle-center text-white">UN SERVICE ALL INCLUSIVE</p>
                        <h2 class="title h1">Les Dépôts</h2>
                    </div>
                    <div class="depot-content">
                        <div class="row justify-content-center">


                            <div class="col-md-5">

                                <div class="texte equilibre mt-5">

                                    <h4 class="text-primary mt-3">1.	VOS TENUES SONT CHOUCHOUTÉES  CHEZ NOUS : </h4>
                                    <p>L’envoie de vos tenues est à nos charges. </p>

                                    <h4 class="text-primary mt-3">2.	ON GERE VOS LOCATIONS :</h4>
                                    <p>On pense à tous : les envois, les échanges et l’entretien de vos articles. Pas besoin de vous fatiguer !</p>

                                    <h4 class="text-primary mt-3">3.	VOUS BENEFICIEZ DE 40% DE VOS LOCATIONS </h4>
                                    <p>Vous êtes libre de gérer votre cagnotte pour louer sur notre site, recevoir un virement à la fin du mois sur votre compte bancaire ou bien en espèce sur place.</p>

                                    <h4 class="text-primary mt-3">4.	VOUS SEREZ TOUJOURS LE PROPRIETAIRE DE VOS PIÈCES</h4>
                                    <p>Vous pouvez récupérer vos articles ou même les porter quand vous voulez (prévenez-nous à l’avance).</p>

                                    <h4 class="text-primary mt-3">5.	MAINTENANCE DE VOS ARTICLES</h4>
                                    <p>Nous veillons à garder vos vêtements en parfait état : entretiens réguliers, retouches et pressing à nos charges.</p>

                                    <h4 class="text-primary mt-3">6.	GESTION DES ENDOMMAGEMENTS</h4>
                                    <p>On vous rembourse de la valeur dépréciée de votre article.</p>

                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="image equilibre" style="text-align: center; min-height: 566px; line-height: 566px;">
                                    <img src="{{ asset('images/concept') }}/concept-6.jpg?v30" alt="">

                                </div>

                            </div>

                        </div>
                    </div>


                    <div class="text-center mt-5">
                        <p class="strong">POUR DÉCOUVRIR NOTRE SERVICE AVEC PLUS DE PRÉCISIONS, RENDEZ-VOUS SUR NOTRE <a class="link-click_conceptFaq" href="{{ route('front.faq') }}">FAQ</a> !</p>

                        <div class="button-large">
                            <a class="btn btn-primary link-click_conceptDeposer" href="{{ route('front.account.my-shop.add_product') }}">Déposer mes tenues</a>
                        </div>
                    </div>


                </div>

            </div>

        <div class="essayages bg-accent section-spacing" id="essayages">

                <div class="container">
                    <div class="section__header text-center mb-5 ">
                        <p class="subtitle subtitle-center">PROFITEZ DE VOS AVANTAGES</p>
                        <h2 class="title h1">Les Essayages</h2>
                    </div>

                    <div class="row justify-content-center align-items-stretch text-center">


                        <div class="col-md-3 col-xs-12">
                            <div class="etape-essayage" style="min-height: 201px;">

                                <div class="icone mb-4">
                                    <img src="{{ asset('images/concept') }}/picto-essayage.png?v31" atl="">
                                </div>

                                <h4>AVANTAGE ESSAYAGE</h4>

                                <p>Vous hésitez quels articles choisir ? Vous avez toute une journée pour essayer jusqu’à trois modèles devant votre miroir pour 20DT seulement.</p>

                            </div>
                        </div>

                        <div class="col-md-3 col-xs-12">
                            <div class="etape-essayage" style="min-height: 201px;">

                                <div class="icone mb-4">
                                    <img src="{{ asset('images/concept') }}/picto-mesures.png?v30" atl="">
                                </div>


                                <h4>LES TAILLES</h4>

                                <p>Vous disposez d’une grille de mesures de tous nos modèles <a href="#" data-toggle="modal" data-target="#sizeModal">(voir le tableau de mesures)</a>.</p>


                            </div>
                        </div>

                        <div class="col-md-3 col-xs-12">
                            <div class="etape-essayage" style="min-height: 201px;">

                                <div class="icone mb-4">
                                    <img src="{{ asset('images/concept') }}/picto-robes.png?v30" atl="">
                                </div>

                                <h4>SERVICE FLEXIBLE, CONFIANCE MUTUELLE</h4>

                                <p>Si vous rendez un article avec l’étiquette EasyDress, vous serez remboursée de celui que vous n’avez pas porté.</p>

                            </div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                            <div class="etape-essayage" style="min-height: 201px;">

                                <div class="icone mb-4">
                                    <img src="{{ asset('images/concept') }}/picto-robes.png?v30" atl="">
                                </div>

                                <h4>ESSAYAGE GRATUIT DANS NOS LOCAUX</h4>

                                <p>Vous pouvez aussi prendre un rendez-vous pour essayer des articles dans nos locaux avant de confirmer la réservation.</p>

                            </div>

                        </div>


                    </div>

                    <div class="text-center mt-5">
                        <p class="strong">POUR DÉCOUVRIR NOTRE SERVICE AVEC PLUS DE PRÉCISION, RENDEZ-VOUS SUR NOTRE <a class="link-click_conceptFaq" href="{{ route('front.faq') }}">FAQ</a> !</p>

                        <div class="button-large">
                            <a class="btn btn-primary link-click_conceptShowTenues" href="/emprunter/">Voir les tenues</a>

                        </div>
                    </div>

                </div>

            </div>


    </div>
    <!-- Modal -->
    <div class="modal fade" id="sizeModal" tabindex="-1" aria-labelledby="sizeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title text-primary" id="sizeModalLabel">LES MESURES</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fas fa-times"></span></button>
                </div>
                <div class="modal-body">
                    <h4 class="text-primary"></h4>
                    <p>Tous les modèles sont mesurés en arrivant chez nous.</p>

                    <p>
                        Afin de vous permettre de choisir au mieux votre taille, nous reprenons les mesures de chaque modèle et leur attribuons une taille standard correspondant à la grille de taille ci-dessous (basée sur une stature de 1,68m). Il vous suffit de comparer les résultats du tableau avec votre tour de poitrine, de taille et de hanches pour connaître votre taille « Easy Dress ».
                    </p>
                    <div class=" table-responsive">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>&nbsp;</th>
                                <th>34</th>
                                <th>36</th>
                                <th>38</th>
                                <th>40</th>
                                <th>42</th>
                                <th>44</th>
                            </tr>
                            <tr>
                                <th>TOUR DE POITRINE(CM)</th>
                                <td>80 à 84</td>
                                <td>84 à 88</td>
                                <td>88 à 91</td>
                                <td>91 à 94</td>
                                <td>94 à 98</td>
                                <td>98 à 102</td>
                            </tr>
                            <tr>
                                <th>TOUR DE TAILLE(CM)</th>
                                <td>60 à 64</td>
                                <td>64 à 68</td>
                                <td>68 à 73</td>
                                <td>73 à 79</td>
                                <td>79 à 85</td>
                                <td>85 à 91</td>
                            </tr>
                            <tr>
                                <th>TOUR DES HANCHES(CM)</th>
                                <td>88 à 92</td>
                                <td>92 à 96</td>
                                <td>96 à 101</td>
                                <td>101 à 107</td>
                                <td>107 à 113</td>
                                <td>113 à 119</td>
                            </tr>
                            <tr>
                                <th>LONGEUR TAILLE / SOL (CM)</th>
                                <td>105</td>
                                <td>106</td>
                                <td>106</td>
                                <td>107</td>
                                <td>107</td>
                                <td>108</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
