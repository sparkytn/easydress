<x-app-layout title="Checkout">
    @section('page-title', 'Checkout')
    @include('front.partials.page-header')

    <section class="section-spacing">
        <div class="container">
            <form action="{{ route('front.save_order') }}" method="post">
                @csrf
                <h3 class="text-primary">Lieu de livraison</h3>
                <div class="list-group">

                    <div class="list-group-item">
                        <div class="list-group-item-heading">
                            <x-auth-validation-errors class="mb-4 alert alert-danger alert-address" :errors="$errors"/>
                            @php($has_address = ($delivery_addresses != null && $delivery_addresses->count() > 0))
                            @if ($has_address)
                                <div class="form-group">
                                    <h6>Choisir une adresse de livraison</h6>
                                    <select name="delivery_address" class="form-control" id="checkout_delivery_address" showSubtext="true" required>
                                        <option value="" {{ old('delivery_address') === '' ? 'selected' : '' }}>
                                            Selectionnez une adresse...
                                        </option>
                                        <option value="add_new_adr" {{ old('delivery_address') === 'add_new_adr' ? 'selected' : '' }}>+ Ajouter une nouvelle adresse</option>
                                        <optgroup label="Enregistrée(s)">
                                            @foreach ($delivery_addresses as $adr)
                                                <option value="{{ $adr->id }}" data-subtext=" - {{ $adr->street }}, {{ $adr->city }}" {{ old('delivery_address') === $adr->id ? 'selected' : '' }}>
                                                    {{ $adr->address_label }}
                                                </option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                    <hr class="w-100">
                                </div>
                            @endif
                            <div id="new_checkout_delivery_address">
                                <div id="new_checkout_delivery_address_holder">
                                    <h4 class="bg-tertiary text-white py-2 px-3"> Ajouter une nouvelle adresse de livraison</h4>

                                    <div class="new_address_form">
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="contact_person">Nom de la personne a contacter</label>
                                                    <input type="text" class="form-control form-control-small"
                                                           value="{{ auth()->user()->name }}" name="contact_person"
                                                           id="contact_person" placeholder="Personne a contacter" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="contact_phone">Téléphone</label>
                                                    <input type="text" class="form-control"
                                                           value="{{ auth()->user()->phone }}" name="contact_phone"
                                                           id="contact_phone" placeholder="Téléphone" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="address_label">Nom du lieu</label>
                                            <input type="text" class="form-control form-control-large"
                                                   name="address_label" id="address_label"
                                                   placeholder="Maison, Travail..." required value="{{old('address_label')}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="address">Rue</label>
                                            <input type="text" class="form-control form-control-large" name="street"
                                                   id="address" placeholder="Adresse" required value="{{old('street')}}">
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="zip_code">Code Postal</label>
                                                    <input type="text" class="form-control form-control-small"
                                                           name="zip_code" id="zip_code" placeholder="Code postale"  value="{{old('zip_code')}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label for="city">Gouvernorat</label>
                                                    <select id="city" class="form-control checkout-adr-city" required name="city"  value="{{old('city')}}">
                                                        <option selected disabled>Gouvernorat</option>
                                                        <option value="Ariana">Ariana</option>
                                                        <option value="Béja">Béja</option>
                                                        <option value="Ben Arous">Ben Arous</option>
                                                        <option value="Bizerte">Bizerte</option>
                                                        <option value="Gabès">Gabès</option>
                                                        <option value="Gafsa">Gafsa</option>
                                                        <option value="Jendouba">Jendouba</option>
                                                        <option value="Kairouan">Kairouan</option>
                                                        <option value="Kasserine">Kasserine</option>
                                                        <option value="Kébili">Kébili</option>
                                                        <option value="La Manouba">La Manouba</option>
                                                        <option value="Le Kef">Le Kef</option>
                                                        <option value="Mahdia">Mahdia</option>
                                                        <option value="Médenine">Médenine</option>
                                                        <option value="Monastir">Monastir</option>
                                                        <option value="Nabeul">Nabeul</option>
                                                        <option value="Sfax">Sfax</option>
                                                        <option value="Sidi Bouzid">Sidi Bouzid</option>
                                                        <option value="Siliana">Siliana</option>
                                                        <option value="Sousse">Sousse</option>
                                                        <option value="Tataouine">Tataouine</option>
                                                        <option value="Tozeur">Tozeur</option>
                                                        <option value="Tunis">Tunis</option>
                                                        <option value="Zaghouan">Zaghouan</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="mt-5 text-center">
                    <button class="btn btn-primary">Confirmer et enregister ma commande</button>
                </div>
            </form>
        </div>
    </section>
</x-app-layout>
