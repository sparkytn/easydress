<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico?v2') }}">
    <link rel="icon" type="image/png" href="{{ asset('favicon.png?v2') }}">

    <title>
        @isset($title)
            {{ $title }} |
        @endisset
        {{ config('app.name') }}
    </title>
    <meta name="description" content="EasyDress est un concept de dressing collaboratif, qui vous facilite la vie en mettant à votre disposition des hautes gammes  en location">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <livewire:styles />
    <livewire:scripts />
</head>
<body @yield('body_attributes')>
    <div id="app">

        @include('front.partials.header')

        @if(auth()->user() && (auth()->user()->password === 'not_set_yet' || auth()->user()->password === null || auth()->user()->password === ''))
            <div class="container">
                <div class="section section-spacing">
                    <div class="row justify-content-center">
                        <div class="col-md-6 col-lg-5">
                            <h1 class="mt-5 text-center">Enregistrez votre mot de passe pour continuer</h1>


                            <!-- Validation Errors -->
                            <x-auth-validation-errors class="mb-4 alert alert-danger" :errors="$errors"/>

                            <form method="POST" action="{{ route('front.social.set-password') }}">
                            @csrf
                            <!-- Password -->
                                <div class="mt-4">
                                    <x-label for="password" :value="__('Password')"/>
                                    <x-input id="password" class="" type="password" name="password" required
                                             autocomplete="new-password" />
                                </div>

                                <!-- Confirm Password -->
                                <div class="mt-4">
                                    <x-label for="password_confirmation" :value="__('Confirm Password')"/>
                                    <x-input id="password_confirmation" class="" type="password" name="password_confirmation"
                                             required/>
                                </div>
                                <div class="mt-4 text-center">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Enregistrer') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        @else
            <main>
                {{ $slot }}
                @yield('content')
            </main>
        @endif
    </div>

    @include('front.layouts.footer')

    <script src="{{ asset('js/app.js') }}"></script>

    @stack('js')

</body>
</html>
