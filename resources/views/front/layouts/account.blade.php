<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex">

    <title>
        @isset($title)
            {{ $title }} |
        @endisset
        {{ config('app.name') }}
    </title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Libre+Baskerville&family=Roboto:ital,wght@0,300;0,400;0,700;1,400;1,700&display=swap" type="text/css" rel="stylesheet" property="stylesheet" media="all">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <livewire:styles />
    <livewire:scripts />
</head>
<body>
    <div id="app">


        @include('front.partials.header')

        <main>
            {{ $slot }}
            @yield('content')
        </main>
    </div>

    @include('front.layouts.footer')

    <script src="{{ asset('js/app.js?i') }}"></script>

    @stack('js')

</body>
</html>
