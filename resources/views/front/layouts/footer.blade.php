<section class="section section-spacing bg-primary text-center">
    <div class="container">
        <div class="section__header text-center">
            <p class="subtitle subtitle-center text-white opacity-50">Gagner de l'argent</p>
            <h2 class="title">La mode on ne la possède pas, on la suit </h2>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8  col-lg-7 mt-3">
                <p class="lead">Il est désormais possible dès aujourd’hui de louer ses robes les plus chics qui ont passé des mois dans le placard. Rentabilisez votre garde-robe et renouvelez-le avec Easy Dress !</p>
{{--                <a href="{{ route('front.account.my-shop.add_product') }}" class="btn btn-secondary mt-3">En savoir plus</a>--}}
            </div>
        </div>

    </div>
</section>


<footer id="footer-main" class="footer">
    <div class="footer__top section-spacing pb-3">
        <div class="container">
            <div class="row pt-md">
                <div class="col-lg-6 mb-5 mb-lg-0">
                    <a href="/">
                        <img src="{{ asset('images/EasyDress-logo-white.svg') }}" width="120px" class="mb-4" alt="">
                    </a>
                    <p class="text-justify">EasyDress est un concept de dressing collaboratif de haute gamme, qui vous facilite la vie en mettant à votre disposition des tenues exceptionnelles disponibles en location. L’offre est en «all inclusive», livraison jusqu’à chez vous, pressing compris et possibilité de réserver de nombreuses tenues à la fois. </p>
                </div>
                <div class="col-lg-2 col-6 col-sm-4 ml-lg-auto mb-5 mb-lg-0">
                    <h6 class="heading mb-3">Menu</h6>
                    <ul class="list-unstyled">
                        <li class="mb-2"><a href="{{ route('front.shop') }}">Emprunter</a></li>
                        <li class="mb-2"><a href="{{ route('front.account.my-shop.add_product') }}">Déposer</a></li>
                        <li class="mb-2"><a href="{{ route('front.le-concept') }}">Le concept</a></li>
                        <li class="mb-2"><a href="{{ route('front.faq') }}">FAQ</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-6 col-sm-4 mb-5 mb-lg-0">
                    <h6 class="heading mb-3">Rejoignez nous</h6>
                    <ul class="list-unstyled text-small">
                        <li class="mb-2"><a href="fb.com/easydress">
                                <i class="fab fa-facebook mr-1"></i> Facebook
                            </a></li>
                        <li class="mb-2"><a href="#">
                                <i class="fab fa-instagram mr-1"></i> Instagram
                            </a></li>

                    </ul>
                </div>

            </div>
        </div>
    </div>
    <div class="footer__copyright">
        <div class="container">
            <div class="row delimiter-top py-4">
                <div class="col-md-12">
                    <div class="copyright text-sm font-weight-bold text-center text-md-left">
                        © 2021 EasyDress, all rights reserved. Made by <a href="https://webpixels.io" target="_blank">Sparky</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
