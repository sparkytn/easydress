<nav x-data="{ open: false }" class="bg-white">
    <div class="max-w-7xl mx-auto px-4 flex items-center justify-between flex-wrap p-6">

        <div class="flex items-center flex-shrink-0 mr-12 text-primary font-serif">
            <span class="font-semibold text-xl tracking-tight">Easy <span class="text-tertiary">Dress</span></span>
        </div>

        <div :class="{'block': open, 'hidden': ! open}"  class="flex-grow w-full lg:flex lg:items-center lg:w-auto order-last lg:order-none">
            <div class="text-sm lg:flex-grow text-text uppercase font-bold">
                <x-nav-link :href="route('front.home')" :active="request()->routeIs('front.home')">
                    Accueil
                </x-nav-link>
                <x-nav-link :href="route('front.account.dashboard')" :active="request()->routeIs('front.account.dashboard')">
                    Emprunter
                </x-nav-link>

                <x-nav-link :href="route('front.account.dashboard')" :active="request()->routeIs('front.account.dashboard')">
                    Blog
                </x-nav-link>

            </div>
        </div>

        <div class="text-primary order-2">
            <div class="inline-block">
                <button class="btn-ed">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-7 w-7 p-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                    </svg>
                </button>
                <button class="inline-block text-sm px-1 py-1 mr-3 hover:text-tertiary">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-7 w-7 p-1" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z" />
                    </svg>
                </button>

                <!-- Settings Dropdown -->
                @guest
{{--                <div class="inline-block">--}}
{{--                    <a href="{{ route('login') }}" class="flex items-center text-sm hover:text-tertiary transition duration-150 ease-in-out translate-y-6">--}}

{{--                        <svg xmlns="http://www.w3.org/2000/svg" class="h-7 w-7 border rounded-full border-gray-300 p-1 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">--}}
{{--                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />--}}
{{--                        </svg>--}}
{{--                        --}}{{--                                <svg class="fill-current h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">--}}
{{--                        --}}{{--                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />--}}
{{--                        --}}{{--                                </svg>--}}
{{--                    </a>--}}
{{--                </div>--}}
                <x-dropdown align="right" width="48" >
                    <x-slot name="trigger">
                        <button class="flex items-center text-sm hover:text-tertiary transition duration-150 ease-in-out translate-y-6">

                            <svg xmlns="http://www.w3.org/2000/svg" class="h-7 w-7 border rounded-full border-gray-300 p-1 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                            </svg>
                            <svg class="fill-current h-5 w-5 inline-block" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                            </svg>
                        </button>
                    </x-slot>

                    <x-slot name="content">
                        <div class="px-4 py-2 text-sm leading-5 bg-secondary text-primary">
                            <div class="uppercase font-bold">Espace client</div>
                        </div>
                        <x-dropdown-link :href="route('login')">
                            Connectez-vous
                        </x-dropdown-link>
                        <x-dropdown-link :href="route('register')">
                            Inscrivez vous
                        </x-dropdown-link>

                    </x-slot>
                </x-dropdown>
                @endguest
                @auth
                <x-dropdown align="right" width="48" >
                    <x-slot name="trigger">
                        <button class="flex items-center text-sm hover:text-tertiary transition duration-150 ease-in-out translate-y-6">

                            <svg xmlns="http://www.w3.org/2000/svg" class="h-7 w-7 border rounded-full border-gray-300 p-1 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                            </svg>
                            <span class="font-bold uppercase font-sm"> {{ Auth::user()->name }}</span>
                                                            <svg class="fill-current h-5 w-5 inline-block" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                                                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                                            </svg>
                        </button>
                    </x-slot>

                    <x-slot name="content">
                        <div class="px-4 py-2 text-sm leading-5 bg-secondary text-primary">
                            <div class="uppercase font-bold">{{ Auth::user()->name }}</div>
                            <div class="text-xs text-white">{{ Auth::user()->email }}</div>
                        </div>
                        <x-dropdown-link :href="route('front.account.dashboard')" :active="request()->routeIs('front.account.dashboard')">
                            Dashboard
                        </x-dropdown-link>
                        <!-- Authentication -->
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <x-dropdown-link :href="route('logout')"
                                             onclick="event.preventDefault();
                                                this.closest('form').submit();">
                                {{ __('Log out') }}
                            </x-dropdown-link>
                        </form>
                    </x-slot>
                </x-dropdown>
                @endauth
            </div>
            <button  @click="open = ! open" class="inline-block lg:hidden items-center px-1 py-1 hover:text-tertiary">
                <svg class="h-7 w-7 p-1" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                    <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                    <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                </svg>
            </button>
        </div>
    </div>
</nav>


{{--<nav x-data="{ open: false }" class="bg-white border-b border-gray-100">--}}
{{--    <!-- Primary Navigation Menu -->--}}
{{--    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">--}}
{{--        <div class="flex justify-between h-16">--}}
{{--            <div class="flex">--}}
{{--                <!-- Logo -->--}}
{{--                <div class="flex-shrink-0 flex items-center">--}}
{{--                    <a href="{{ route('front.account.dashboard') }}">--}}
{{--                        <x-application-logo class="block h-10 w-auto fill-current text-gray-600" />--}}
{{--                    </a>--}}
{{--                </div>--}}

{{--                <!-- Navigation Links -->--}}
{{--                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">--}}
{{--                    <x-nav-link :href="route('front.home')" :active="request()->routeIs('front.home')">--}}
{{--                        Accueil--}}
{{--                    </x-nav-link>--}}
{{--                    <x-nav-link :href="route('front.account.dashboard')" :active="request()->routeIs('front.account.dashboard')">--}}
{{--                        Dashboard--}}
{{--                    </x-nav-link>--}}
{{--                    @guest--}}
{{--                        <x-nav-link :href="route('login')" :active="request()->routeIs('login')">--}}
{{--                            Mon Compte--}}
{{--                        </x-nav-link>--}}
{{--                    @endguest--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            @auth--}}
{{--                <!-- Settings Dropdown -->--}}
{{--                <div class="hidden sm:flex sm:items-center sm:ml-6">--}}
{{--                    <x-dropdown align="right" width="48">--}}
{{--                        <x-slot name="trigger">--}}
{{--                            <button class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">--}}
{{--                                <div>{{ Auth::user()->name }}</div>--}}

{{--                                <div class="ml-1">--}}
{{--                                    <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">--}}
{{--                                        <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />--}}
{{--                                    </svg>--}}
{{--                                </div>--}}
{{--                            </button>--}}
{{--                        </x-slot>--}}

{{--                        <x-slot name="content">--}}
{{--                            <x-dropdown-link :href="route('front.account.dashboard')" :active="request()->routeIs('front.account.dashboard')">--}}
{{--                                Dashboard--}}
{{--                            </x-dropdown-link>--}}
{{--                            <!-- Authentication -->--}}
{{--                            <form method="POST" action="{{ route('logout') }}">--}}
{{--                                @csrf--}}

{{--                                <x-dropdown-link :href="route('logout')"--}}
{{--                                        onclick="event.preventDefault();--}}
{{--                                                    this.closest('form').submit();">--}}
{{--                                    {{ __('Log out') }}--}}
{{--                                </x-dropdown-link>--}}
{{--                            </form>--}}
{{--                        </x-slot>--}}
{{--                    </x-dropdown>--}}
{{--                </div>--}}
{{--            @endauth--}}

{{--            <!-- Hamburger -->--}}
{{--            <div class="-mr-2 flex items-center sm:hidden">--}}
{{--                <button @click="open = ! open" class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">--}}
{{--                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">--}}
{{--                        <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />--}}
{{--                        <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />--}}
{{--                    </svg>--}}
{{--                </button>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    <!-- Responsive Navigation Menu -->--}}
{{--    <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">--}}
{{--        <div class="pt-2 pb-3 space-y-1">--}}
{{--            <x-responsive-nav-link :href="route('front.account.dashboard')" :active="request()->routeIs('front.account.dashboard')">--}}
{{--                {{ __('Dashboard') }}--}}
{{--            </x-responsive-nav-link>--}}
{{--        </div>--}}
{{--    @auth--}}
{{--        <!-- Responsive Settings Options -->--}}
{{--        <div class="pt-4 pb-1 border-t border-gray-200">--}}
{{--            <div class="flex items-center px-4">--}}
{{--                <div class="flex-shrink-0">--}}
{{--                    <svg class="h-10 w-10 fill-current text-gray-400" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">--}}
{{--                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />--}}
{{--                    </svg>--}}
{{--                </div>--}}

{{--                <div class="ml-3">--}}
{{--                    <div class="font-medium text-base text-gray-800">{{ Auth::user()->name }}</div>--}}
{{--                    <div class="font-medium text-sm text-gray-500">{{ Auth::user()->email }}</div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="mt-3 space-y-1">--}}
{{--                <!-- Authentication -->--}}
{{--                <form method="POST" action="{{ route('logout') }}">--}}
{{--                    @csrf--}}

{{--                    <x-responsive-nav-link :href="route('logout')"--}}
{{--                            onclick="event.preventDefault();--}}
{{--                                        this.closest('form').submit();">--}}
{{--                        {{ __('Log out') }}--}}
{{--                    </x-responsive-nav-link>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    @endauth--}}
{{--    </div>--}}
{{--</nav>--}}
