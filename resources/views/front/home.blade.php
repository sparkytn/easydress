<x-app-layout>


    @include('front.partials.hero-home')


    <section class="section section-spacing section__product">
        <div class="container">
            <div class="section__header">
                <p class="subtitle">Découvrez les tenues</p>
                <h2 class="title">Les tenues <br> de la semaine</h2>
            </div>

            <!-- Slider main container -->
            <div class="swiper-container home-products-carousel mt-5">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    @foreach($products as $product)
                        <div class="swiper-slide">
                            <x-front.product-card :product="$product" :booked="$booked"/>
                        </div>
                    @endforeach

                </div>
                <!-- If we need pagination -->
                <div class="swiper-pagination swiper-pagination-primary"></div>
            </div>

            <div class="mt-4 text-center">
                <a href="{{ route('front.shop') }}" title="Voir tous les tenus diponibles" class="btn btn-primary btn-lg">Voir tous</a>
            </div>
        </div>
    </section>

    <section class="section section-spacing section__features bg-secondary">
        <div class="container">
            <div class="row row-grid align-items-stretch justify-content-center ali">
                <div class="col-lg-8">
                    <div class="section__header text-center mb-5">
                        <p class="subtitle subtitle-center">Les avantages</p>
                        <h2 class="title text-white">Choisir EasyDress</h2>
                    </div>
                </div>
                <div class="w-100"></div>
                <div class="col-md-3">
                    <div class=" hover-shadow-lg text-center h-100">
                        <div class=" icon-wrapper mx-auto">
                            <svg class="features__icon" xmlns="http://www.w3.org/2000/svg" xmlns:x="http://www.w3.org/1999/x" x="0px" y="0px" viewBox="0 0 448 512" style="enable-background:new 0 0 448 512;" xml:space="preserve">
                                            <style type="text/css">
                                                .st0{fill:#787484;}
                                                .st1{fill:#413D49;}
                                            </style>
                                <g>
                                    <path class="st0" d="M0,192v272c0,26.51,21.49,48,48,48l0,0h352c26.51,0,48-21.49,48-48l0,0V192H0z M345.26,305l-143,141.8
                                                    c-4.68,4.69-12.28,4.71-16.97,0.03c-0.01-0.01-0.02-0.02-0.03-0.03l-82.6-83.26c-4.68-4.69-4.66-12.29,0.03-16.97
                                                    c0.01-0.01,0.02-0.02,0.03-0.03l0.08-0.08l28.4-28.17c4.68-4.69,12.28-4.71,16.97-0.03c0.01,0.01,0.02,0.02,0.03,0.03l46,46.36
                                                    l106-105.19c4.68-4.69,12.28-4.71,16.97-0.03c0.01,0.01,0.02,0.02,0.03,0.03L345.3,288c4.68,4.69,4.68,12.29-0.01,16.97
                                                    C345.28,304.98,345.27,304.99,345.26,305L345.26,305z M304,128h32c8.84,0,16-7.16,16-16V16c0-8.84-7.16-16-16-16h-32
                                                    c-8.84,0-16,7.16-16,16v96C288,120.84,295.16,128,304,128z M112,128h32c8.84,0,16-7.16,16-16V16c0-8.84-7.16-16-16-16h-32
                                                    c-8.84,0-16,7.16-16,16v96C96,120.84,103.16,128,112,128z"/>
                                    <path class="st1" d="M345.33,288l-28.2-28.4c-4.65-4.72-12.25-4.78-16.97-0.13c-0.01,0.01-0.02,0.02-0.03,0.03l-106,105.19
                                                    l-46-46.36c-4.65-4.72-12.25-4.77-16.97-0.12c-0.01,0.01-0.02,0.02-0.03,0.03l-28.4,28.17c-4.72,4.65-4.78,12.25-0.13,16.97
                                                    c0.01,0.01,0.02,0.02,0.03,0.03l82.6,83.26c4.65,4.72,12.25,4.78,16.97,0.13c0.01-0.01,0.02-0.02,0.03-0.03l143-141.8
                                                    c4.72-4.65,4.78-12.25,0.13-16.97c-0.01-0.01-0.02-0.02-0.03-0.03V288z M400,64h-48v48c0,8.84-7.16,16-16,16h-32
                                                    c-8.84,0-16-7.16-16-16V64H160v48c0,8.84-7.16,16-16,16h-32c-8.84,0-16-7.16-16-16V64H48C21.49,64,0,85.49,0,112v80h448v-80
                                                    C448,85.49,426.51,64,400,64z"/>
                                </g>
                                            </svg>


                        </div>
                        <div class="px-4 pb-3">
                            <h3 class="text-primary text-uppercase">Choisir le Look qui vous convient</h3>
                            <p class="text-white">Je profite de ma liberté avec EasyDress pour choisir de nouveaux looks. Peu importe la date de votre évènement, vous pouvez réserver votre tenue à la dernière minute (la veille) ou anticiper 3 mois avant. </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class=" hover-shadow-lg text-center h-100">
                        <div class=" icon-wrapper mx-auto">

                            <svg class="features__icon" xmlns="http://www.w3.org/2000/svg" xmlns:x="http://www.w3.org/1999/x" x="0px" y="0px" viewBox="0 0 640 512" style="enable-background:new 0 0 640 512;" xml:space="preserve">
                                            <style type="text/css">
                                                .st0{fill:#413D49;}
                                                .st1{fill:#787484;}
                                            </style>
                                <g>
                                    <path class="st0" d="M8.86,96.5c-7.89,3.98-11.1,13.57-7.2,21.5l57.23,114.5c3.98,7.89,13.57,11.1,21.5,7.2l17.32-8.48L26.09,88
                                                    L8.86,96.5z M467.86,15.55c-4.22,4.64-8.66,9.07-13.32,13.26C418.45,61.3,370.67,79.2,320,79.2s-98.41-17.9-134.51-50.39
                                                    c-4.66-4.19-9.1-8.62-13.32-13.26L54.76,73.75l71.69,143.4L137,212c7.95-3.87,17.52-0.56,21.39,7.39c1.06,2.18,1.61,4.58,1.61,7.01
                                                    V480c0,17.67,14.33,32,32,32h256c17.67,0,32-14.33,32-32V226.3c-0.01-8.84,7.15-16.01,15.99-16.01c2.43,0,4.83,0.55,7.01,1.61
                                                    l10.57,5.18L585.19,73.7L467.86,15.55z M631.17,96.55l-17.32-8.59l-71.6,143.19l17.24,8.45h0.06c7.91,3.93,17.51,0.71,21.45-7.2
                                                    L638.28,118c3.98-7.89,0.81-17.51-7.08-21.49c-0.01,0-0.01-0.01-0.02-0.01L631.17,96.55z"/>
                                    <path class="st1" d="M320,47.2c-51.89,0-96.39-19.4-116.49-47.2l-31.34,15.55c4.22,4.64,8.66,9.07,13.32,13.26
                                                    C221.59,61.3,269.37,79.2,320,79.2s98.43-17.9,134.53-50.39c4.66-4.19,9.1-8.62,13.32-13.26L436.51,0
                                                    C416.41,27.8,371.91,47.2,320,47.2z M26.09,88l71.62,143.22l28.74-14.07L54.76,73.75L26.09,88z M585.19,73.7l-71.68,143.38
                                                    l28.74,14.07l71.61-143.24L585.19,73.7z"/>
                                </g>
                                            </svg>

                        </div>
                        <div class="px-4 pb-3">
                            <h3 class="text-primary text-uppercase">Livraison assurée</h3>
                            <p class="text-white">Vous aurez votre tenue jusqu’à chez vous en moins de 24h. Vous pouvez choisir de vous faire livrer à l’endroit que vous souhaiteriez. Transport aller-retour garanti par EasyDress.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class=" hover-shadow-lg text-center h-100">
                        <div class=" icon-wrapper mx-auto">
                            <svg class="features__icon" xmlns="http://www.w3.org/2000/svg" xmlns:x="http://www.w3.org/1999/x" x="0px" y="0px" viewBox="0 0 640 512" style="enable-background:new 0 0 640 512;" xml:space="preserve">
                                            <style type="text/css">
                                                .st0{fill:#787484;}
                                                .st1{fill:#413D49;}
                                            </style>
                                <g>
                                    <path class="st0" d="M248,160H40c-4.42,0-8,3.58-8,8v16c0,4.42,3.58,8,8,8h208c4.42,0,8-3.58,8-8v-16
                                                    C256,163.58,252.42,160,248,160z M224,248v-16c0-4.42-3.58-8-8-8H8c-4.42,0-8,3.58-8,8v16c0,4.42,3.58,8,8,8h208
                                                    C220.42,256,224,252.42,224,248z M176,352c-44.18,0-80,35.82-80,80s35.82,80,80,80s80-35.82,80-80S220.18,352,176,352z M464,352
                                                    c-44.18,0-80,35.82-80,80s35.82,80,80,80s80-35.82,80-80S508.18,352,464,352z M280,96H8c-4.42,0-8,3.58-8,8v16c0,4.42,3.58,8,8,8
                                                    h272c4.42,0,8-3.58,8-8v-16C288,99.58,284.42,96,280,96z"/>
                                    <path class="st1" d="M624,352h-16V243.9c-0.02-12.72-5.09-24.92-14.1-33.9L494,110.1c-8.98-9.01-21.18-14.08-33.9-14.1H416V48
                                                    c0-26.51-21.49-48-48-48H112C85.49,0,64,21.49,64,48v48h216c4.42,0,8,3.58,8,8v16c0,4.42-3.58,8-8,8H64v32h184c4.42,0,8,3.58,8,8
                                                    v16c0,4.42-3.58,8-8,8H64v32h152c4.42,0,8,3.58,8,8v16c0,4.42-3.58,8-8,8H64v112c-0.02,8.82,2.4,17.47,7,25
                                                    c21.52-57.99,85.98-87.56,143.97-66.04c38.46,14.27,66.04,48.43,71.89,89.04h66.28c8.8-61.23,65.57-103.72,126.8-94.92
                                                    c49.2,7.07,87.85,45.72,94.92,94.92H624c8.84,0,16-7.16,16-16v-32C640,359.16,632.84,352,624,352z M560,256H416V144h44.1l99.9,99.9
                                                    V256z"/>
                                </g>
                                            </svg>
                        </div>
                        <div class="px-4 pb-3">
                            <h3 class="text-primary text-uppercase">J’enfile ma jolie tenue</h3>
                            <p class="text-white">Être la plus belle pendant 3, 6 ou 12 jours au choix, selon vos dates de réservation. </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class=" hover-shadow-lg text-center h-100">
                        <div class=" icon-wrapper mx-auto">
                            <svg class="features__icon" xmlns="http://www.w3.org/2000/svg" xmlns:x="http://www.w3.org/1999/x" x="0px" y="0px"
                                 viewBox="0 0 448 512" style="enable-background:new 0 0 448 512;" xml:space="preserve">
                                            <style type="text/css">
                                                .st0{fill:#787484;}
                                                .st1{fill:#413D49;}
                                            </style>
                                <g>
                                    <path class="st0" d="M298,300c13.49,0,26.45-5.27,36.11-14.69c1.16,6.17,1.79,12.42,1.89,18.69c0,61.86-50.14,112-112,112
                                                    s-112-50.14-112-112c0.1-6.27,0.73-12.52,1.89-18.69c20.5,19.95,53.29,19.5,73.24-1c0,0,0,0,0,0c19.64,20.36,52.08,20.95,72.44,1.3
                                                    c0.44-0.43,0.88-0.86,1.3-1.3C270.62,294.34,284.01,300,298,300z"/>
                                    <path class="st1" d="M384,0H64C28.65,0,0,28.65,0,64v416c0,17.67,14.33,32,32,32h384c17.67,0,32-14.33,32-32V64
                                                    C448,28.65,419.35,0,384,0z M184,64c13.25,0,24,10.75,24,24s-10.75,24-24,24s-24-10.75-24-24S170.75,64,184,64z M64,88
                                                    c0-13.25,10.75-24,24-24s24,10.75,24,24s-10.75,24-24,24S64,101.25,64,88z M224,448c-79.53,0-144-64.47-144-144s64.47-144,144-144
                                                    s144,64.47,144,144S303.53,448,224,448z"/>
                                </g>
                                            </svg>

                        </div>
                        <div class="px-4 pb-3">
                            <h3 class="text-primary text-uppercase">Le pressing est offert</h3>
                            <p class="text-white">Nous veillerons à ce que vos pièces resteront impeccables ! Un service de nettoyage professionnel et écologique est assuré avant et après la location. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-spacing section__article">
        <div class="container">
            <div class="row no-gutters align-items-md-center">
                <div class="card bg-white border-0 hover-scale-110 order-2 col-lg-7 mb-0">
                    <div class="card-body p-5">
                        <h2 class="mb-4 title">Éco Stylée</h2>
                        <p class="lead text-muted">
                            La soirée responsable et stylée, avec des robes, des couleurs, des paillettes et des papillons au ventre en plus. Les éco-stylée, à la fois amoureuse de la mode et de la planète kiff kiff, plus de concessions, plus de préférence. <br>  Qui dit éco-stylée dit éco-smart, c’est échanger ses tenues entre collectionneuses  de mode au lieu d’acheter.  Vivre en harmonie avec la planète et les unes avec les autres.
                        </p>
                        <div class="text-right">
                        </div>
                    </div>
                </div>
                <div class="order-1 col-lg-5 mb-4 mb-lg-0">
                    <img alt="Image placeholder" src="{{ asset('images/article-1.jpg') }}" class="img-fluid shadow">
                </div>
            </div>
        </div>
    </section>
</x-app-layout>
