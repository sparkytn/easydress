@component('mail::message')

{!! $msg !!}

@component('mail::button', ['url' => route('front.account.order.history')])
Consultez vos commandes
@endcomponent

Merci,<br>
{{ config('app.name') }}
@endcomponent
