@component('mail::message')
# Votre commission à été payée !

Bonjour {{ $commission->user->name }},

Nous avons le plaisir de vous informer que votre commission relative à la commande #{{  $commission->booking->code }}
du montant de : <b> {{ $commission->amount  }} DT</b> a été payée.

@component('mail::button', ['url' => url('/mon-compte/ma-boutique/mes-commissions')])
Consulter vos commissions
@endcomponent

Merci,<br>
{{ config('app.name') }}
@endcomponent
