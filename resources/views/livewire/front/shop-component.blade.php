<div>
    <header class="page-header section-spacing bg-secondary text-white">
        <div class="container text-center">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    @if($activeCategory == 'all')
                        <h1 class="mb-2">Location De Vêtement </h1>
                    @elseif(strtolower($activeCategory) == 'accessoires')
                        <h1 class="mb-2">Location d'accessoires </h1>
                    @else
                        <h1 class="mb-2">Location de {{ strtolower($activeCategory) }} </h1>
                    @endif
                    <p class="lead text-primary">Louez désormais vos {{ strtolower($activeCategory) == 'all' ? 'vêtements' : strtolower($activeCategory) }} d’exceptions <br> et soyez la plus belle pour chacune de vos occasions</p>
                </div>
            </div>

        </div>
    </header>

    <section class="section section-spacing section__shop">
        <div class="container">

            @php
                $cart = session('cart') ?? array();
            @endphp
            @if ( count($cart) >= config('app.max_order') || session('card_full'))
                <x-front.max-order-alert></x-front.max-order-alert>


            @else
            <div class="loading" wire:loading>
                <div id="spinner"></div>
            </div>
                <div class="row">
                    <aside class="col-lg-3 col-md-4">
                        <button class="btn btn-primary d-md-none d-block w-100 mb-3" type="button" data-toggle="collapse" data-target="#prodcutFilter" aria-expanded="false" aria-controls="prodcutFilter">
                            <i class="fas fa-filter"></i> Filtres des articles
                        </button>
                        <div class="collapse d-md-block mt-3" id="prodcutFilter">
                            <div class="card filter-card border-0">
                                <div class="">
                                    <div id="shop-filter">
                                        <h3 class="font-sans filter-card__title">Catégories :</h3>
                                        <div class="list-group">
                                            <a href="{{ route('front.shop') }}" class="list-group-item list-group-item-action @if($activeCategory === 'all' ) active @endif " @if($activeCategory === 'all' ) aria-current="true" @endif>
                                                Toutes des catégories
                                            </a>
                                            @foreach($allCategories as $category)
                                                <a href="{{ route('front.shop',$category) }}" class="list-group-item list-group-item-action @if($activeCategory === $category->name ) active @endif " @if($activeCategory === $category->name ) aria-current="true" @endif>
                                                    {{ $category->name }}
                                                </a>
                                            @endforeach
                                        </div>

                                        <h3 class="font-sans filter-card__title mt-4">Marques : </h3>
                                        <div class="list-group list-group-product-filter">
                                            <button wire:click="$set( 'brand', null )" class="list-group-item @if($activeBrand === null ) active @endif " @if($activeBrand === null ) aria-current="true" @endif>
                                                Toutes les Marques
                                            </button>
                                            <div class="list-group-item has-search p-2 ">
                                                <span class="fa fa-search form-control-feedback"></span>
                                                <input type="search" class="form-control filterSearchInput " placeholder="Recherche">
                                            </div>

                                            @foreach($brands as $brandItem)
                                                <button wire:click="$set( 'brand', {{$brandItem->id}} )" class="list-group-item list-group-item-action py-2 @if($activeBrand === $brandItem->id ) active @endif " @if($activeBrand === $brandItem->id ) aria-current="true" @endif >
                                                    {{ $brandItem->name }}
                                                </button>
                                            @endforeach
                                        </div>


                                        @foreach($attributes as $attribute)
                                            <div wire:key="{{ $attribute->id }}">
                                                <div class="form-group">
                                                    <h3 class="font-sans filter-card__title mt-4"> {{ $attribute->name }} : </h3>

                                                    <div class="list-group list-group-product-filter">
                                                        <label class="list-group-item mb-0 @if( isset($filters[$attribute->id]) && $filters[$attribute->id] == 'null' ) active @endif @if(!isset($filters[$attribute->id])) active @endif">
                                                            <input class="fake-input" type="radio" name="{{ $attribute->id }}" value="null" wire:model="filters.{{ $attribute->id }}" checked>
                                                            Touts
                                                        </label>
                                                        <div class="list-group-item has-search p-2 ">
                                                            <span class="fa fa-search form-control-feedback"></span>
                                                            <input type="search" class="form-control filterSearchInput " placeholder="Recherche">
                                                        </div>

                                                        @foreach($attribute->values as $value)
                                                            <label class="list-group-item list-group-item-action py-2 mb-0 @if( isset($filters[$attribute->id]) && $filters[$attribute->id] == $value->id ) active @endif ">
                                                                <input class="fake-input" type="radio" name="{{ $attribute->id }}" value="{{ $value->id }}" wire:model="filters.{{ $attribute->id }}">
                                                                {{ $value->value }}
                                                            </label>
                                                        @endforeach
                                                    </div>

                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>

                    </aside>

                    <section class="col-lg-9 col-md-8">
                        <header class="list-header mb-3">
                            <div class="row align-items-center">
                                <div class="col-6">
                                    <b class="font-weight-bolder text-uppercase text-secondary">Les Articles :</b>
                                </div>
                                <div class="col-6 text-right">
                                    <select class="form-control w-auto" name="items-per-page" wire:model="productPerPage">
                                        <option value="12">12 par page</option>
                                        <option value="24">24 par page</option>
                                        <option value="48">48 par page</option>
                                    </select>
                                </div>
                            </div>
                        </header>


                        <div class="row justify-content-center">

                            @forelse($products as $product)
                                <div class="col-md-6 col-lg-4 col-6 px-2 mb-4">
                                    <x-front.product-card :product="$product" :booked="$booked"/>
                                </div>
                            @empty
                                <div class="col-md-8 text-center">
                                    <h3 class="text-primary mt-5">Pas de resultat pour le monent </h3>
                                </div>
                            @endforelse
                        </div>


                        <nav aria-label="Shop navigation" class="justify-content-center d-flex">
                            {{ $products->links() }}
                        </nav>

                    </section>
                </div>
            @endif
        </div>

    </section>

    <script>
        Livewire.on('reRendering', () => {
            $('select').selectpicker();
        })
    </script>
</div>
