<div>

    <div class="mt-3">

        @if ( !$inCart)
            @if (session()->has('selected_period'))
                @if (session('selected_period') != 2)
                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modalRent"
                            data-type=".calendar-rent-info" wire:click="$set('selectedPeriod',3)"><i
                            class="fas fa-shopping-cart"></i> Louer
                    </button>
                @else
                    <button class="btn btn-outline-primary" type="button" data-toggle="modal" data-target="#modalRent"
                            data-type=".calendar-rent-dates" wire:click="$set('selectedPeriod',2)">Essayer
                    </button>
                @endif
            @else
                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modalRent"
                        data-type=".calendar-rent-info" wire:click="$set('selectedPeriod',3)"><i
                        class="fas fa-shopping-cart"></i> Louer
                </button>
                <span class="h4 mx-3">ou</span>
                <button class="btn btn-outline-primary" type="button" data-toggle="modal" data-target="#modalRent"
                        data-type=".calendar-rent-dates" wire:click="$set('selectedPeriod',2)">Essayer
                </button>
            @endif
        @else
            <div class="card card-body bg-secondary text-white">
                <h4 class="font-sans">Cet article est dans votre panier.</h4>
                <a class="btn btn-primary" href="{{ route('front.showcart') }}">
                    Mon panier
                </a>
            </div>
            {{--            <b>Non disponible pour les dates que vous ayez choisis !</b>--}}
        @endif
    </div>

    <div class="modal fade" wire:ignore id="modalRent" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content modal-lg">
                <form action="{{ route('front.addtocart') }}" method="post">
                    @csrf
                    <div class="modal-body pb-0">
                        <div class="row">
                            <div class="col-lg-6 mb-lg-0 mb-4">
                                <h3>Je veux louer cet article :</h3>

                                <div class="card border">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-3 col-3">
                                                @foreach ($product->images as $image)
                                                    <img src="{{ asset('storage/imgs/gallery_thumb_' . $image->file_name) }}"
                                                         class="img-fluid" alt="">
                                                    @break;
                                                @endforeach
                                            </div>
                                            <div class="col-md-9 col-9">
                                                <p class="small mb-2 text-muted">{{ $product->category->name }}</p>
                                                <h3 class="mb-1"><span class="text-primary">
                                                        {{ $product->name }}</span></h3>
                                                <h6 class="text-muted">{{ $product->brand->name }}</h6>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="row align-items-stretch justify-content-center">
                                    <div class="col-6">
                                        <div class=" hover-shadow-lg text-center h-100">
                                            <div class=" icon-wrapper mx-auto">
                                                <svg class="features__icon" xmlns="http://www.w3.org/2000/svg" xmlns:x="http://www.w3.org/1999/x" x="0px" y="0px" viewBox="0 0 448 512" style="enable-background:new 0 0 448 512;" xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #787484;
                                                    }

                                                    .st1 {
                                                        fill: #413D49;
                                                    }
                                                </style>
                                                    <g>
                                                        <path class="st0" d="M0,192v272c0,26.51,21.49,48,48,48l0,0h352c26.51,0,48-21.49,48-48l0,0V192H0z M345.26,305l-143,141.8
                                                        c-4.68,4.69-12.28,4.71-16.97,0.03c-0.01-0.01-0.02-0.02-0.03-0.03l-82.6-83.26c-4.68-4.69-4.66-12.29,0.03-16.97
                                                        c0.01-0.01,0.02-0.02,0.03-0.03l0.08-0.08l28.4-28.17c4.68-4.69,12.28-4.71,16.97-0.03c0.01,0.01,0.02,0.02,0.03,0.03l46,46.36
                                                        l106-105.19c4.68-4.69,12.28-4.71,16.97-0.03c0.01,0.01,0.02,0.02,0.03,0.03L345.3,288c4.68,4.69,4.68,12.29-0.01,16.97
                                                        C345.28,304.98,345.27,304.99,345.26,305L345.26,305z M304,128h32c8.84,0,16-7.16,16-16V16c0-8.84-7.16-16-16-16h-32
                                                        c-8.84,0-16,7.16-16,16v96C288,120.84,295.16,128,304,128z M112,128h32c8.84,0,16-7.16,16-16V16c0-8.84-7.16-16-16-16h-32
                                                        c-8.84,0-16,7.16-16,16v96C96,120.84,103.16,128,112,128z"/>
                                                        <path class="st1" d="M345.33,288l-28.2-28.4c-4.65-4.72-12.25-4.78-16.97-0.13c-0.01,0.01-0.02,0.02-0.03,0.03l-106,105.19
                                                        l-46-46.36c-4.65-4.72-12.25-4.77-16.97-0.12c-0.01,0.01-0.02,0.02-0.03,0.03l-28.4,28.17c-4.72,4.65-4.78,12.25-0.13,16.97
                                                        c0.01,0.01,0.02,0.02,0.03,0.03l82.6,83.26c4.65,4.72,12.25,4.78,16.97,0.13c0.01-0.01,0.02-0.02,0.03-0.03l143-141.8
                                                        c4.72-4.65,4.78-12.25,0.13-16.97c-0.01-0.01-0.02-0.02-0.03-0.03V288z M400,64h-48v48c0,8.84-7.16,16-16,16h-32
                                                        c-8.84,0-16-7.16-16-16V64H160v48c0,8.84-7.16,16-16,16h-32c-8.84,0-16-7.16-16-16V64H48C21.49,64,0,85.49,0,112v80h448v-80
                                                        C448,85.49,426.51,64,400,64z"/>
                                                    </g>
                                            </svg>
                                            </div>
                                            <div class="pb-3">
                                                <h4 class="text-primary text-uppercase">Je choisis ma tenue et réserve ma date</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class=" hover-shadow-lg text-center h-100">
                                            <div class=" icon-wrapper mx-auto">
                                                <svg class="features__icon" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:x="http://www.w3.org/1999/x" x="0px" y="0px"
                                                     viewBox="0 0 640 512" style="enable-background:new 0 0 640 512;"
                                                     xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #413D49;
                                                    }

                                                    .st1 {
                                                        fill: #787484;
                                                    }

                                                </style>
                                                    <g>
                                                        <path class="st0"
                                                              d="M8.86,96.5c-7.89,3.98-11.1,13.57-7.2,21.5l57.23,114.5c3.98,7.89,13.57,11.1,21.5,7.2l17.32-8.48L26.09,88
                                                L8.86,96.5z M467.86,15.55c-4.22,4.64-8.66,9.07-13.32,13.26C418.45,61.3,370.67,79.2,320,79.2s-98.41-17.9-134.51-50.39
                                                c-4.66-4.19-9.1-8.62-13.32-13.26L54.76,73.75l71.69,143.4L137,212c7.95-3.87,17.52-0.56,21.39,7.39c1.06,2.18,1.61,4.58,1.61,7.01
                                                V480c0,17.67,14.33,32,32,32h256c17.67,0,32-14.33,32-32V226.3c-0.01-8.84,7.15-16.01,15.99-16.01c2.43,0,4.83,0.55,7.01,1.61
                                                l10.57,5.18L585.19,73.7L467.86,15.55z M631.17,96.55l-17.32-8.59l-71.6,143.19l17.24,8.45h0.06c7.91,3.93,17.51,0.71,21.45-7.2
                                                L638.28,118c3.98-7.89,0.81-17.51-7.08-21.49c-0.01,0-0.01-0.01-0.02-0.01L631.17,96.55z"/>
                                                        <path class="st1" d="M320,47.2c-51.89,0-96.39-19.4-116.49-47.2l-31.34,15.55c4.22,4.64,8.66,9.07,13.32,13.26
                                                C221.59,61.3,269.37,79.2,320,79.2s98.43-17.9,134.53-50.39c4.66-4.19,9.1-8.62,13.32-13.26L436.51,0
                                                C416.41,27.8,371.91,47.2,320,47.2z M26.09,88l71.62,143.22l28.74-14.07L54.76,73.75L26.09,88z M585.19,73.7l-71.68,143.38
                                                l28.74,14.07l71.61-143.24L585.19,73.7z"/>
                                                    </g>
                                            </svg>
                                            </div>
                                            <div class="pb-3">
                                                <h4 class="text-primary text-uppercase">Transport aller/retour offert
                                                </h4>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class=" hover-shadow-lg text-center h-100">
                                            <div class=" icon-wrapper mx-auto">
                                                <svg class="features__icon" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:x="http://www.w3.org/1999/x" x="0px" y="0px"
                                                     viewBox="0 0 640 512" style="enable-background:new 0 0 640 512;"
                                                     xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #787484;
                                                    }

                                                    .st1 {
                                                        fill: #413D49;
                                                    }

                                                </style>
                                                    <g>
                                                        <path class="st0" d="M248,160H40c-4.42,0-8,3.58-8,8v16c0,4.42,3.58,8,8,8h208c4.42,0,8-3.58,8-8v-16
                                                C256,163.58,252.42,160,248,160z M224,248v-16c0-4.42-3.58-8-8-8H8c-4.42,0-8,3.58-8,8v16c0,4.42,3.58,8,8,8h208
                                                C220.42,256,224,252.42,224,248z M176,352c-44.18,0-80,35.82-80,80s35.82,80,80,80s80-35.82,80-80S220.18,352,176,352z M464,352
                                                c-44.18,0-80,35.82-80,80s35.82,80,80,80s80-35.82,80-80S508.18,352,464,352z M280,96H8c-4.42,0-8,3.58-8,8v16c0,4.42,3.58,8,8,8
                                                h272c4.42,0,8-3.58,8-8v-16C288,99.58,284.42,96,280,96z"/>
                                                        <path class="st1" d="M624,352h-16V243.9c-0.02-12.72-5.09-24.92-14.1-33.9L494,110.1c-8.98-9.01-21.18-14.08-33.9-14.1H416V48
                                                c0-26.51-21.49-48-48-48H112C85.49,0,64,21.49,64,48v48h216c4.42,0,8,3.58,8,8v16c0,4.42-3.58,8-8,8H64v32h184c4.42,0,8,3.58,8,8
                                                v16c0,4.42-3.58,8-8,8H64v32h152c4.42,0,8,3.58,8,8v16c0,4.42-3.58,8-8,8H64v112c-0.02,8.82,2.4,17.47,7,25
                                                c21.52-57.99,85.98-87.56,143.97-66.04c38.46,14.27,66.04,48.43,71.89,89.04h66.28c8.8-61.23,65.57-103.72,126.8-94.92
                                                c49.2,7.07,87.85,45.72,94.92,94.92H624c8.84,0,16-7.16,16-16v-32C640,359.16,632.84,352,624,352z M560,256H416V144h44.1l99.9,99.9
                                                V256z"/>
                                                    </g>
                                            </svg>
                                            </div>
                                            <div class="pb-3">
                                                <h4 class="text-primary text-uppercase">Je profite de ma tenue</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class=" hover-shadow-lg text-center h-100">
                                            <div class=" icon-wrapper mx-auto">
                                                <svg class="features__icon" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:x="http://www.w3.org/1999/x" x="0px" y="0px"
                                                     viewBox="0 0 448 512" style="enable-background:new 0 0 448 512;"
                                                     xml:space="preserve">
                                                <style type="text/css">
                                                    .st0 {
                                                        fill: #787484;
                                                    }

                                                    .st1 {
                                                        fill: #413D49;
                                                    }

                                                </style>
                                                    <g>
                                                        <path class="st0" d="M298,300c13.49,0,26.45-5.27,36.11-14.69c1.16,6.17,1.79,12.42,1.89,18.69c0,61.86-50.14,112-112,112
                                                s-112-50.14-112-112c0.1-6.27,0.73-12.52,1.89-18.69c20.5,19.95,53.29,19.5,73.24-1c0,0,0,0,0,0c19.64,20.36,52.08,20.95,72.44,1.3
                                                c0.44-0.43,0.88-0.86,1.3-1.3C270.62,294.34,284.01,300,298,300z"/>
                                                        <path class="st1" d="M384,0H64C28.65,0,0,28.65,0,64v416c0,17.67,14.33,32,32,32h384c17.67,0,32-14.33,32-32V64
                                                C448,28.65,419.35,0,384,0z M184,64c13.25,0,24,10.75,24,24s-10.75,24-24,24s-24-10.75-24-24S170.75,64,184,64z M64,88
                                                c0-13.25,10.75-24,24-24s24,10.75,24,24s-10.75,24-24,24S64,101.25,64,88z M224,448c-79.53,0-144-64.47-144-144s64.47-144,144-144
                                                s144,64.47,144,144S303.53,448,224,448z"/>
                                                    </g>
                                        </svg>

                                            </div>
                                            <div class="pb-3">
                                                <h4 class="text-primary text-uppercase">Le pressing est offert</h4>

                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="calendar-rent-info calendar-options">
                                    <label class="font-weight-bolder text-uppercase d-block">Pour Combien de jours : </label>
                                    <div class="btn-group btn-group-toggle mb-4">
                                        @if(session()->has('selected_period'))
                                            <span>
                                            <label class="btn btn-primary">
                                                <input type="hidden" name="selectedPeriod" value="{{session()->get('selected_period')}}">
                                                {{$selectedPeriod}}J
                                            </label>
                                        </span>
                                        @else
                                            <span>
                                            <input type="radio" wire:model="selectedPeriod" name="selectedPeriod" value="3" id="period3" @if(session()->has('start_date')) @if($selectedPeriod == 3) checked @else disabled @endif @else checked @endif>
                                            <label class="btn btn-outline-primary" for="period3">
                                                 3J
                                            </label>
                                        </span>
                                            <span>
                                            <input type="radio" wire:model="selectedPeriod" name="selectedPeriod" value="6" id="period8" @if(session()->has('start_date')) @if($selectedPeriod == 6) checked @else disabled @endif @endif>
                                            <label class="btn btn-outline-primary" for="period8">
                                                  6J <sup class="badge badge-secondary">-20%</sup>
                                            </label>
                                        </span>
                                            <span>
                                            <input type="radio" wire:model="selectedPeriod" name="selectedPeriod" value="12" id="period12" @if(session()->has('start_date')) @if($selectedPeriod == 12) checked @else disabled @endif @endif>
                                            <label class="btn btn-outline-primary" for="period12">
                                                  12J <sup class="badge badge-secondary">-40%</sup>
                                            </label>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <h4 class="calendar-rent-dates calendar-options">
                                    Vous avez <b>2 jours</b> pour l'essayage
                                    <span class="d-none">
                                    <input type="radio" wire:model="selectedPeriod" name="selectedPeriod" value="2" id="period2" @if(session()->has('start_date')) @if($selectedPeriod == 2) checked @else disabled @endif @endif>
                                </span>

                                </h4>
                                @if(session()->has('start_date'))
                                    <label class="font-weight-bolder text-uppercase d-block">Du {{ session('start_date') }} au {{ session('end_date') }}</label>
                                @else
                                    <label class="font-weight-bolder text-uppercase d-block">Choisissez la date </label>
                                @endif


                                <div class="calendar mb-4 @if(session()->has('start_date')) disabled @endif"></div>

                                <h2 class=" mb-0 text-secondary"><small>Prix A Payer : </small><b class="rent-price-wrapper"><span id="rent-price">{{ $price }}.000</span>DT</b></h2>
                                <span class="text-secondary opacity-75">Avec une caution de : <b class="caution-price text-primary">{{ $product->value_price }}DT</b></span>

                                <input type="hidden" name="input_start_date" id="input-start-date" value="">
                                <input type="hidden" name="input_end_date" id="input-end-date" value="">
                                <input type="hidden" name="input_price" id="input-price" value="">
                                <input type="hidden" name="input_product_id" id="input-product_id" value="{{ $product->id }}">


                            </div>

                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                        <button type="submit" class="btn btn-primary" id="confirm-dates" disabled>Confirmer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @push('js')
        <script>
            (function () {

                let selectedDate = @if(session()->has('start_date')) '{{session('start_date')}}' @else null @endif;
                let $dayCell = $('.calendar .fc-day');
                let product_price = {{ $product->rent_price }} ;

                $('input[name=selectedPeriod]').click(function () {
                    $('#rent-price').html(0);
                    $('#input-price').val(0);
                    $('.calendar td').removeClass('bg-renting start end');
                    $('#confirm-dates').attr('disabled', 'true');
                })


                let displayRentingDates = function (selectedDate, period = {{ $selectedPeriod }}, locked = false) {

                    let startDate = window.momentDate(selectedDate, "YYYY-MM-DD");
                    let daysNumber = period;
                    let cssClass = 'bg-renting';


                    if (locked) {
                        cssClass = 'bg-locked';
                    } else {
                        $('.calendar td').removeClass('bg-renting start end')
                    }

                    let packageReceptionDate = window.momentDate(selectedDate, 'YYYY-MM-DD');
                    $('.calendar .fc-bg td[data-date="' + packageReceptionDate.format('YYYY-MM-DD') + '"]').addClass(cssClass + ' start');

                    let packageResendDate = window.momentDate(selectedDate).add(period - 1, 'days');
                    $('.calendar .fc-bg td[data-date="' + packageResendDate.format('YYYY-MM-DD') + '"]').addClass(cssClass + ' end');

                    while (parseInt(daysNumber) > 0) {

                        $('.calendar .fc-bg td[data-date="' + startDate.format('YYYY-MM-DD') + '"]').addClass(cssClass);

                        if ($('.calendar .fc-bg td[data-date="' + startDate.format('YYYY-MM-DD') + '"]').hasClass('bg-locked') && !locked) {
                            $('.calendar td').removeClass('bg-renting start end')
                            $('#rent-price').html(0.000);
                            $('#input-price').val(0.000);
                            $('#confirm-dates').attr('disabled', 'true');
                            return;
                            break;
                        }
                        startDate.add(1, 'days');
                        daysNumber--;
                    }
                    let min_period = 3;
                    let price = product_price;

                    // if( period > min_period ){
                    //     price = product_price + ( product_price * (period/min_period) * (period/min_period /10) );
                    // }
                    //
                    if (period == 6) {
                        price = product_price * 2 - ((product_price * 2) * 0.3);
                    }

                    if (period == 12) {
                        price = product_price * 4 - ((product_price * 4) * 0.4);
                    }

                    if (period == 2) {
                        price = 20
                    }

                    if (!locked) {
                        $('#input-start-date').val(packageReceptionDate.format('YYYY-MM-DD'))
                        $('#input-end-date').val(packageResendDate.format('YYYY-MM-DD '));
                        $('#input-price').val(price.toFixed(3));
                        $('#confirm-dates').removeAttr('disabled');
                        $('#rent-price').html(price.toFixed(3))
                    }
                };


                let renderBookedDates = function () {
                    let bookedDates = JSON.parse('{!! $this->bookedDates !!}');
                    for (let i = 0; i < bookedDates.length; i++) {
                        let startD = window.momentDate(bookedDates[i]['start_date'], "YYYY-MM-DD");
                        let endD = window.momentDate(bookedDates[i]['end_date'], "YYYY-MM-DD");
                        displayRentingDates(startD, endD.diff(startD, 'days') + 1, true);
                    }

                }

                calendar = $('.calendar').fullCalendar({
                    locale: 'fr',
                    header: {
                        left: 'prev',
                        center: 'title',
                        right: 'next'
                    },
                    validRange: {
                        start: '{{ $rangeStart }}',
                        end: '{{ $rangeEnd }}'
                    },
                    defaultDate: @if (session()->has('start_date')) '{{ session('start_date') }}' @else '{{ $activeCalendar }}' @endif,
                    dayClick: function (date, jsEvent, view) {

                        let current = $('.calendar td[data-date="' + date.format('YYYY-MM-DD') + '"]');
                        if (current.hasClass('bg-locked')) {
                            return false;
                        }

                        if (current.hasClass('fc-other-month')) {
                            if (date.diff(view.intervalEnd._d) >= 0) {
                                $('.calendar').fullCalendar('next');
                            } else {
                                $('.calendar').fullCalendar('prev');
                            }
                        }
                        selectedDate = date
                        $('#modalRental #form_startDate').val(date.format('YYYY-MM-DD'));
                        displayRentingDates(date, @this.selectedPeriod);
                    },
                    viewRender(view, element) {
                        if (selectedDate !== null) {
                            displayRentingDates(selectedDate, {{ $selectedPeriod }});
                        }
                        renderBookedDates();
                    }
                });


                $('#modalRent').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget) // Button that triggered the modal
                    var cssClass = button.data('type') // Extract info from data-* attributes
                    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.

                    $('.calendar-options').addClass('d-none');
                    $(cssClass).removeClass('d-none');

                    @if (!session()->has('start_date'))
                    $('#input-start-date').val('')
                    $('#input-end-date').val('');
                    $('#input-price').val(0);
                    $('#confirm-dates').attr('disabled', 'true');
                    $('#rent-price').html(0);
                    $('.calendar td').removeClass('bg-renting start end');
                    @endif
                })

            })()
        </script>
    @endpush
</div>
