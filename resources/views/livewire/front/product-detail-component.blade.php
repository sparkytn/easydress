<div>

    @section('page-title', $product->name  )
    @include('front.partials.page-header')


    <section class="section section-spacing">
        <div class="container">
            @php
                $cart = session('cart') ?? array();
            @endphp
            @if ( count($cart) >= config('app.max_order'))
                <div class="alert alert-danger">
                    <h2>Limite de location atteint ({{ config('app.max_order') }} articles).</h2>
                    <p>Vous avez {{ config('app.max_order') }} articles dans votre panier, veillez confirmer votre commande </p>
                </div>


            @else

                <div class="row">
                    <div class="col-md-6 col-lg-5">
                        <div class="d-block d-md-none">
                            <p class="subtitle">{{ $product->category->name  }}</p>
                            <h2 class="mb-4">
                                {{ $product->name  }}
                            </h2>
                        </div>

                        <div class="product_gallery">
                                @forelse($product->images as $image)
                                    @if($loop->first)
                                    <img class="xzoom img-fluid" src="{{ asset('storage/imgs/gallery_'.$image->file_name) }}" xoriginal="{{ asset('storage/imgs/'.$image->file_name) }}" />
                                    <div class="xzoom-thumbs mt-3">
                                    @endif
                                    <a href="#{{ asset('storage/imgs/gallery_'.$image->file_name) }}">
                                        <img src="{{ asset('storage/imgs/gallery_thumb_'.$image->file_name) }}" xpreview="{{ asset('storage/imgs/gallery_'.$image->file_name) }}" width="100px" class="img-fluid xzoom-gallery" alt="">
                                    </a>
                                    @if($loop->last)
                                    </div>
                                    @endif
                                @empty
                                    <img src="{{ asset('images/no-image.jpg') }}" class="img-fluid" width="100%" alt="">
                                @endforelse


                        </div>
                    </div>
                    <div class="col-md-6 col-lg-7">
                        <div class="d-none d-md-block">
                            <p class="subtitle">{{ $product->category->name  }}</p>
                            <h2 class="mb-4">
                                {{ $product->name  }}
                            </h2>
                        </div>

                        @foreach($product->attributes_values as $attribute_value)
                            <p class="product_meta mb-1">
                                <b>{{ $attribute_value->attribute->name}} :</b> {{ $attribute_value->value }}
                            </p>
                        @endforeach

                        <div class="mt-3 h2 text-primary"><strong>A partir de : </strong> <span class="pro-price"> {{ $product->rent_price  }}DT</span> <br> <small class="text-secondary opacity-75" style="font-size: 12px">Caution : {{ $product->value_price }}DT</small></div>

                        @livewire('front.components.calendar', [ 'product' => $product,'available'=>$available])

                        <p class="mt-3">Un doute sur la taille ? <a href="#" data-toggle="modal" data-target="#sizeModal">Cliquez ici</a></p>

                        <hr class="my-3">

                        <h5 class="mt-4">Style</h5>
                        {{ $product->style_description }}
                        <h5 class="mt-4">Taille</h5>
                        {{ $product->size_description }}
                        <h5 class="mt-4">Composition</h5>
                        {{ $product->composition_description }}
                    </div>
                </div>
            @endif

        </div>
    </section>


    <section class="section section-spacing section__product">
        <div class="container">
            <div class="section__header">
                <p class="subtitle">Découvrez</p>
                <h2 class="title">D'autres tenues </h2>
            </div>

            <!-- Slider main container -->
            <div class="swiper-container home-products-carousel mt-5">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    @foreach($products as $product)
                        <div class="swiper-slide">
                            <x-front.product-card :product="$product" :booked="$booked"/>
                        </div>
                    @endforeach

                </div>
                <!-- If we need pagination -->
                <div class="swiper-pagination swiper-pagination-primary"></div>
            </div>

            <div class="mt-4 text-center">
                <a href="{{ route('front.shop') }}" title="Voir tous les tenus diponibles" class="btn btn-primary btn-lg">Voir tous</a>
            </div>
        </div>
    </section>


    <!-- Modal -->
    <div class="modal fade" id="sizeModal" tabindex="-1" aria-labelledby="sizeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title text-primary" id="sizeModalLabel">UN DOUTE SUR LA TAILLE ?</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="fas fa-times"></span></button>
                </div>
                <div class="modal-body">

                    <h4 class="text-primary">1- L'OFFRE ESSAYAGE</h4>
                    <p>Pour 20Dt recevez jusqu'à trois modèles pour  toute une journée. Essayez-les tranquillement devant votre miroir.</p>
                    <h4 class="text-primary">2- CHOISISSEZ PLUSIEURS MODÈLES</h4>
                    <p>Empruntez jusqu’à 2 modèles*, choisissez tranquillement chez vous et soyez remboursée de ceux que vous n’avez pas portés.</p>
                    <h4 class="text-primary">3- LES MESURES</h4>
                    <p>Tous les modèles sont mesurés en arrivant chez nous.</p>

                    <p>
                        Afin de vous permettre de choisir au mieux votre taille, nous reprenons les mesures de chaque modèle et leur attribuons une taille standard correspondant à la grille de taille ci-dessous (basée sur une stature de 1,68m). Il vous suffit de comparer les résultats du tableau avec votre tour de poitrine, de taille et de hanches pour connaître votre taille « Easy Dress ».
                    </p>
                    <div class=" table-responsive">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>&nbsp;</th>
                                <th>34</th>
                                <th>36</th>
                                <th>38</th>
                                <th>40</th>
                                <th>42</th>
                                <th>44</th>
                            </tr>
                            <tr>
                                <th>TOUR DE POITRINE(CM)</th>
                                <td>80 à 84</td>
                                <td>84 à 88</td>
                                <td>88 à 91</td>
                                <td>91 à 94</td>
                                <td>94 à 98</td>
                                <td>98 à 102</td>
                            </tr>
                            <tr>
                                <th>TOUR DE TAILLE(CM)</th>
                                <td>60 à 64</td>
                                <td>64 à 68</td>
                                <td>68 à 73</td>
                                <td>73 à 79</td>
                                <td>79 à 85</td>
                                <td>85 à 91</td>
                            </tr>
                            <tr>
                                <th>TOUR DES HANCHES(CM)</th>
                                <td>88 à 92</td>
                                <td>92 à 96</td>
                                <td>96 à 101</td>
                                <td>101 à 107</td>
                                <td>107 à 113</td>
                                <td>113 à 119</td>
                            </tr>
                            <tr>
                                <th>LONGEUR TAILLE / SOL (CM)</th>
                                <td>105</td>
                                <td>106</td>
                                <td>106</td>
                                <td>107</td>
                                <td>107</td>
                                <td>108</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>

    @push('js')

        <script>
            $('#staticBackdrop').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('type') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('.modal-title').text('New message to ' + recipient)
                modal.find('.modal-body input').val(recipient)
            })

        </script>
    @endpush
</div>
