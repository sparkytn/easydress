
    <section class="col-lg-8 pt-lg-4 pb-4 mb-3">
        <div class="pt-2 px-4 ps-lg-0 pe-xl-5">
            <h2 class="h3 py-2">Ma Boutique</h2>
            <form wire:submit.prevent="submit">
                @csrf
                <div class="mb-3 pb-2">
                    <label class="form-label" for="unp-product-name" name="category">Categorie *</label>
                    <select class="form-control" id="unp-category" name="category" wire:model="category">
                        <option value="" @if(!old('category')) selected @endif>Selectionnez une catégorie</option>
                        @foreach($allCategories as $category)
                            <option value="{{ $category->id }}"
                                @if(old('category')==$category->id) selected="selected" @endif>{{ $category->name }}</option>
                        @endforeach

                    </select>
                    @error('category') <span class="error text-danger">{{ $message }}</span> @enderror
                </div>

                <div class="mb-3 pb-2">
                    <label class="form-label" for="unp-product-name">Titre *</label>
                    <input class="form-control" type="text" id="unp-product-name" name="name" wire:model="name" value="{{ old('name') }}" required>
                    @error('name') <span class="error text-danger">{{ $message }}</span> @enderror
                    <div class="form-text text-muted">Pas de HTML ni Emoji</div>
                </div>
                <div class="mb-3 pb-2">

                    <label  class="form-label">Images Produits su différents angles (JPG, PNG)</label>
                    <div class="custom-file mb-1">
                        <input class="custom-file-input"  accept="image/jpeg,image/jpg,image/png" type="file"  wire:model="img1"  id="product-image-1" required>
                        <label for="product-image-1" class="custom-file-label">Uploder une image du produits  1/3 *:</label>
                    </div>
                    <div class=" mb-3">
                        @if ($img1)
                            @if(substr($img1->getMimeType(), 0, 5) == 'image') {
                                <img src="{{ $img1->temporaryUrl() }}" width="100px">
                            @endif
                         @endif
                        @error('img1') <div class="error text-danger">{{ $message }}</div> @enderror
                    </div>

                    <div class="custom-file mb-1">
                        <input class="custom-file-input"  accept="image/jpeg,image/jpg,image/png" type="file" name="img2" wire:model="img2"  id="product-image-2">
                        <label for="product-image-2" class="custom-file-label">Uploder une image du produits  2/3 :</label>
                    </div>
                    <div class="mb-3">
                        @if ($img2)
                            @if(substr($img2->getMimeType(), 0, 5) == 'image') {
                            <img src="{{ $img2->temporaryUrl() }}" width="100px">
                            @endif
                        @endif
                        @error('img2') <div class="error text-danger">{{ $message }}</div> @enderror
                    </div>

                    <div class="custom-file mb-1">
                        <input class="custom-file-input"  accept="image/jpeg,image/jpg,image/png" type="file" name="img3" wire:model="img3"  id="product-image-3">
                        <label for="product-image-3" class="custom-file-label">Uploder une image du produits  3/3 :</label>
                    </div>
                    <div class="mb-3">
                        @if ($img3)
                            @if(substr($img3->getMimeType(), 0, 5) == 'image') {
                            <img src="{{ $img3->temporaryUrl() }}" width="100px">
                            @endif
                        @endif
                        @error('img3') <div class="error text-danger">{{ $message }}</div> @enderror
                    </div>

                    <div class="form-text text-muted">L'image ne doit pas dépasser 5Mo</div>
                </div>

                <div class="mb-3 py-2">
                    <label class="form-label" for="unp-product-description">Note</label>
                    <textarea class="form-control" rows="6" id="unp-product-description" wire:model="note" name="note" required>{{ old('note') }}</textarea>
                    @error('note') <span class="error">{{ $message }}</span> @enderror
                </div>
                <div class="row">
                    <div class="col-sm-6 mb-3">
                        <label class="form-label" for="unp-extended-price">Marque *</label>
                        <input class="form-control" type="text" id="unp-extended-price" wire:model="marque" name="marque">

                    </div>
                    <div class="col-sm-6 mb-3">
                        <label class="form-label" for="unp-extended-price">Taille</label>
                        <input class="form-control" type="text" id="unp-extended-price" wire:model="size" name="size">
                    </div>
                    <div class="col-sm-6 mb-3">
                        <label class="form-label" for="unp-extended-price">Couleur</label>
                        <input class="form-control" type="text" id="unp-extended-price" wire:model="color" name="color">
                    </div>

                </div>
                <div class="mb-3 py-2">
                    <label class="form-label" for="unp-extended-price">Prix location souhaité</label>
                    <div class="input-group">
                        <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                        <input class="form-control" type="text" id="unp-extended-price" wire:model="rent_price" name="rent_price" required>
                    </div>
                    @error('rent_price') <span class="error">{{ $message }}</span> @enderror
                </div>
                <div class="mb-3 py-2">
                    <label class="form-label" for="unp-extended-price">Prix d'achat</label>
                    <div class="input-group">
                        <span class="input-group-text"><i class="fas fa-dollar-sign"></i></span>
                        <input class="form-control" type="text" id="unp-extended-price" wire:model="value_price" name="value_price" required>
                    </div>
                    @error('value_price') <span class="error">{{ $message }}</span> @enderror
                </div>
                <button class="btn btn-primary d-block w-100 mt-3" type="submit"><i class="ci-cloud-upload fs-lg me-2"></i>Upload Product</button>
            </form>
        </div>
    </section>

