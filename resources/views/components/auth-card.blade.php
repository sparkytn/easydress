<div class="card">
    <div class="card-body">
        <div class="text-center">
            {{ $logo }}
        </div>


        {{ $slot }}
    </div>
</div>
