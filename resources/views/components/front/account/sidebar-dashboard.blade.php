<aside class="col-lg-4 pe-xl-5 mb-3">
    <!-- Account menu toggler (hidden on screens larger 992px)-->

    <!-- Actual menu-->
    <div class="h-100 border-end mb-2">
        <div class="d-block d-md-none">
            <a class="btn btn-primary d-block" href="#account-menu" data-toggle="collapse"><i class="fas fa-bars mr-2"></i>Menu du compte</a>
        </div>
        <div class="d-lg-block collapse" id="account-menu">

            <div class="list-group mt-lg-5">
                <a href="{{ route('front.account.profile') }}" class="list-group-item list-group-item-action @if(request()->routeIs('front.account.profile') ) active @endif " @if(request()->routeIs('front.account.profile') ) aria-current="true" @endif>
                    <i class="fas fa-id-card mr-2"></i> Mon Profil @if(request()->routeIs('front.account.profile') ) <i class="fas fa-angle-right float-right mt-1"></i> @endif
                </a>
                <a href="{{ route('front.account.addresses') }}" class="list-group-item list-group-item-action @if(request()->routeIs('front.account.addresses') || request()->routeIs('front.account.address.*') ) active @endif " @if(request()->routeIs('front.account.addresses') || request()->routeIs('front.account.address.*') ) aria-current="true" @endif>
                    <i class="fas fa-map-marker-alt mr-2"></i>Mes Adresses @if(request()->routeIs('front.account.addresses') || request()->routeIs('front.account.address.*') || request()->routeIs('front.account.address.edit') ) <i class="fas fa-angle-right float-right mt-1"></i> @endif
                </a>
                <a href="{{ route('front.account.order.history') }}" class="list-group-item list-group-item-action @if(request()->routeIs('front.account.order.history') ) active @endif " @if(request()->routeIs('front.account.order.history') ) aria-current="true" @endif>
                    <i class="fas fa-history mr-2"></i> Mes Commandes @if(request()->routeIs('front.account.order.history') ) <i class="fas fa-angle-right float-right mt-1"></i> @endif
                </a>
                @if(request()->routeIs('front.account.my-shop') || request()->routeIs('front.account.my-shop.add_product'))
                    <a href="{{ route('front.account.my-shop') }}" class="list-group-item list-group-item-action  active "  aria-current="true" >
                        <i class="fas fa-store mr-2"></i> MES DÉPÔTS  <i class="fas fa-angle-right float-right mt-1"></i>
                    </a>
                @else
                    <a href="{{ route('front.account.my-shop') }}" class="list-group-item list-group-item-action ">
                        <i class="fas fa-store mr-2"></i> MES DÉPÔTS
                    </a>
                @endif
                <a href="{{ route('front.account.my-shop.my-commissions') }}" class="list-group-item list-group-item-action @if(request()->routeIs('front.account.my-shop.my-commissions') ) active @endif " @if(request()->routeIs('front.account.my-shop.my-commissions') ) aria-current="true" @endif>
                    <i class="fas fa-money-bill mr-2"></i> MA CAGNOTTE @if(request()->routeIs('front.account.my-shop.my-commissions') ) <i class="fas fa-angle-right float-right mt-1"></i> @endif
                </a>
                <a href="{{ route('front.account.payment.methods') }}" class="list-group-item list-group-item-action @if(request()->routeIs('front.account.payment.methods') ) active @endif " @if(request()->routeIs('front.account.payment.methods') ) aria-current="true" @endif>
                    <i class="fas fa-money-bill mr-2"></i> Moyens de paiement @if(request()->routeIs('front.account.payment.methods') ) <i class="fas fa-angle-right float-right mt-1"></i> @endif
                </a>
                <a href="{{ route('front.faq') }}" class="list-group-item list-group-item-action @if(request()->routeIs('front.faq') ) active @endif " @if(request()->routeIs('front.faq') ) aria-current="true" @endif>
                    <i class="fas fa-question-circle mr-2"></i> FAQ @if(request()->routeIs('front.faq') ) <i class="fas fa-angle-right float-right mt-1"></i> @endif
                </a>
                <a href="{{ route('front.le-concept') }}" class="list-group-item list-group-item-action @if(request()->routeIs('front.le-concept') ) active @endif " @if(request()->routeIs('front.le-concept') ) aria-current="true" @endif>
                    <i class="fas fa-info-circle mr-2"></i> Le Concept @if(request()->routeIs('front.le-concept') ) <i class="fas fa-angle-right float-right mt-1"></i> @endif
                </a>
            </div>
        </div>
    </div>
</aside>
