@props(['show_cart_link' => true])

<div class="alert alert-danger">
    <h2>Limite de location atteint ({{ config('app.max_order') }} articles).</h2>
    <p>Vous avez {{ config('app.max_order') }} articles dans votre panier, veillez confirmer votre commande </p>
</div>
@if ($show_cart_link)
    <div class="text-center">
        <a href="{{ route('front.showcart') }}" class="btn btn-primary">mon Panier</a>
    </div>
@endif
