@props(['products','title','subtitle','carouselClass'=> 'home-products-carousel' ])

<section class="section section-spacing section__product">
    <div class="container">
        <div class="section__header">
            @if($subtitle)
            <p class="subtitle">{{ $subtitle }}</p>
            @endif
            <h2 class="title">{{ $title }}</h2>
        </div>

        <!-- Slider main container -->
        <div {{ $attributes->merge(['class' => $carouselClass]) }}>
            <!-- Additional required wrapper -->
            <div class="swiper-wrapper">
                <!-- Slides -->
                @for( $i=0 ; $i<=7 ; $i++)
                    <div class="swiper-slide">
                        <x-front.product-card :product="$product" />
                    </div>
                @endfor
            </div>
            <!-- If we need pagination -->
            <div class="swiper-pagination swiper-pagination-primary"></div>
        </div>

        <div class="mt-4 text-center">
            <a href="{{ route('shop') }}" title="Voir tous les tenus diponibles" class="btn btn-primary btn-lg">Voir tous</a>
        </div>
    </div>
</section>
