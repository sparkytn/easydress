@props(['product','booked'])

@php
    $is_available = in_array($product->id, $booked)  ? false : true ;
    $link =  $is_available ? route('front.product.detail', $product) : "#" ;
    $cart = session()->get('cart');
    $in_cart = false;
    if($cart){
        foreach ($cart as $item){
            if ($product->id == $item['product_id']) {
                $in_cart = true;
                break;
            }
        }
    }

@endphp
<article class="card product-card {{ $is_available ? '' : 'disabled' }}">
    <div class="p-2">




        <div class="img-wrapper">

            <a href="{{ $link }}">

                @forelse($product->images as $image)
                    @if($loop->first)
                        <img src="{{ asset('storage/imgs/card_thumb_'.$image->file_name) }}" class="img-fluid product-card__img --front" alt="{{ $product->name }}">
                        @if($loop->last)
                            <img src="{{ asset('storage/imgs/card_thumb_'.$image->file_name) }}" class="img-fluid product-card__img --back" alt="{{ $product->name }}">
                        @endif
                    @else
                        <img src="{{ asset('storage/imgs/card_thumb_'.$image->file_name) }}" class="img-fluid product-card__img --back" alt="{{ $product->name }}">
                        @break;
                    @endif
                @empty
                    <img src="{{ asset('images/no-image.jpg') }}" class="img-fluid product-card__img --front" alt="">
                    <img src="{{ asset('images/no-image.jpg') }}" class="img-fluid product-card__img --back" alt="">
                @endforelse
            </a>
            @if($is_available)
                <a href="{{ $link }}" class="product-card__brand btn btn-primary border-0">Je reserve</a>
            @else
                @if(!$in_cart)
                    <a href="#" class="product-card__brand btn btn-primary border-0 disabled" disabled>Non Disponible</a>
                @else
                    <span class="product-card__brand btn btn-primary border-0 disabled" disabled>Dans votre panier</span>
                @endif

            @endif
        </div>

        <a href="{{ $link }}" class="product-card__category">{{ $product->category->name }}</a>
        <a href="{{ $link }}"><h3 class="product-card__title">{{ $product->name }}</h3></a>

        <p class="product-card__details small text-muted">
            <b>La Marque :</b> {{ $product->brand->name }} <br>
        @foreach($product->attributes_values as $attribute_value)
            @if( $attribute_value->attribute->name  === 'Taille')
                <b>{{ $attribute_value->attribute->name}} :</b>  {{ $attribute_value->value }}
                @break
            @endif
        @endforeach
        </p>

        <div class="product-card__price text-center h4 mb-0 text-primary">{{ $product->rent_price }}<sup>DT</sup> </div>
    </div>
</article>
