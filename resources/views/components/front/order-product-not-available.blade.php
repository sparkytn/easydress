@props(['productNotAvailable'])

<div class="alert alert-warning">
    <h2>Un client a confirmé son panier avant vous</h2>
    <p>Il y a {{ $productNotAvailable > 1 ? 'des articles qui ne sont plus': 'un article qui n\'est plus'  }} disponible du {{ session()->get('start_date') }} au {{session()->get('end_date')}}</p>
</div>
