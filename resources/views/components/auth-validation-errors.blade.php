@props(['errors'])

@if ($errors->any())
    <div {{ $attributes->merge(['class'=> 'alert alert-danger']) }}  role="alert">
        <div class="font-medium text-red-600 h4 ">
            {{ __('Whoops! Something went wrong.') }}
        </div>

        <ul class="mt-3">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
