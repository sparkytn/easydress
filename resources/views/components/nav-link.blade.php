@props(['active'])

@php
$classes = ($active ?? false)
            ? 'text-primary block mt-4 lg:inline-block lg:mt-0 hover:text-primary mr-4 transition duration-150 ease-in-out'
            : 'block mt-4 lg:inline-block lg:mt-0 hover:text-primary mr-4 transition duration-150 ease-in-out';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>
