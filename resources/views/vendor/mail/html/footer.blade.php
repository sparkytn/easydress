<tr>
<td>
<table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td class="content-cell" align="center">
{{ Illuminate\Mail\Markdown::parse($slot) }}

    <p style="text-align: center;">
        <a href="https://www.facebook.com/easydress" style="display: inline-block; width: 24px; height: 24px; margin: 0 6px; text-decoration: none;"><img src="{{ asset('images/fb-icon-mailing.png') }}" alt="EasyDress - Facebook" width="24px" height="24px"></a>
        <a href="https://www.instagram.com/easydress" style="display: inline-block; width: 24px; height: 24px; margin: 0 6px; text-decoration: none;"><img src="{{ asset('images/insta-icon-mailing.png') }}" alt="EasyDress - Instagram" width="24px" height="24px"></a>
    </p>


</td>
</tr>
    <tr>
        <td class="content-cell" align="center">

        </td>
    </tr>
</table>
</td>
</tr>
