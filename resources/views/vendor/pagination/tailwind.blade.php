@if ($paginator->hasPages())
<nav aria-label="Pagination Navigation">
    <ul class="pagination pagination-sm">

        @if (!$paginator->onFirstPage())
            <li class="page-item"> 
                <a class="page-link" href="{{ $paginator->previousPageUrl() }}" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span><span class="sr-only">Precedent</span>
                </a>
            </li>
        @endif

        @foreach ($elements as $element)
            @if (is_string($element))
                <li class="page-item disabled">
                    <span class="page-link" >{{ $element }}</span>
                </li>
            @endif

            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active" aria-current="page">
                            <span class="page-link">
                                {{ $page }}
                            <span class="sr-only">(current)</span>
                            </span>
                        </li>
                    @else
                        <li class="page-item">
                            <a class="page-link" href="{{ $url }}">{{ $page }}</a>
                        </li>
                    @endif
                @endforeach
            @endif
           
        @endforeach

        @if ($paginator->hasMorePages())
            <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span> <span class="sr-only">Suivant</span>
                </a>
            </li>            

        @endif

    </ul>
</nav>
@endif
