@foreach($attributes_fields as $attribute)

    @php
        $field_name ='attribute_'.$attribute['id'];
            if(isset($old_attributes[$attribute['id']]))
                $pop_value = old($field_name,$old_attributes[$attribute['id']]);
            else
                $pop_value = old($field_name)?? false;
    @endphp

    <div class="form-group row mb-3">
        <label class="col-md-3 col-form-label" for="user">{{$attribute['label']}}</label>
        <div class="col-md-6">
            <select name="{{$field_name}}" class="form-control @error($field_name) is-invalid @enderror">
                <option value="">Selectionnez...</option>
                @foreach($attribute['values'] as $value)
                    @php
                        $selected = ($pop_value == $value->id) ? ' selected="selected" ' : '';
                    @endphp
                    <option {{ $selected }} value="{{ $value->id}}">
                        {{ $value->value}}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
@endforeach
