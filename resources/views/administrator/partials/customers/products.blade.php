<table class="table table-sm table-responsive-sm table-striped">
    <thead>
        <tr>
            <th>Réf#</th>
            <th>Image</th>
            <th>Nom</th>
            <th>Marque</th>
            <th>Categorie</th>
            <th>Prix location</th>
            <th>Actif</th>
        </tr>
    </thead>
    <tbody>

        @forelse ($products as $product)

            @php

                $image = $product->images->first();

                if ($image) {
                    $image_path = asset('storage/imgs') . '/' . $image->file_name;
                } else {
                    $image_path = 'https://via.placeholder.com/40x40?text=No+image';
                }

            @endphp

            <tr>
                <td>
                    {{ ProductService::generateRef($product) }}
                </td>
                <td style="max-width:50px">
                    <a href="{{ route('admin.products.show', $product) }}">
                        <img src="{{ $image_path }}" width="100%" class="img-fluid img-thumbnail float-left" />
                    </a>
                </td>
                <td>
                    <a
                        href="{{ route('admin.products.show', $product) }}">{{ Str::limit($product->name, 40) }}</a>
                </td>
                <td>
                    {{ $product->brand->name }}
                </td>
                <td>
                    {{ $product->category->name }}
                </td>
                <td>
                    {{ $product->rent_price }} DT
                    <br>
                    <small>Prix achat: <i>{{ $product->value_price }} DT</i></small>
                </td>
                <td>
                    @if ($product->is_online)
                        <span class="badge badge-success">En ligne</span>
                    @else
                        <span class="badge badge-secondary">Hors Ligne</span>
                    @endif
                </td>

            </tr>
        @empty
            <tr>
                <td colspan="7">
                    <center><i>Pas de données</i></center>
                </td>
            </tr>

        @endforelse

    </tbody>
</table>
