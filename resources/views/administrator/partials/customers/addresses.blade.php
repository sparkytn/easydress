<table class="table table-sm table-responsive-sm table-striped">
    <thead>
        <tr>
            <th>Contact</th>
            <th>Telephone</th>
            <th>Adresse</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($addresses as $address)
        <tr>
            <td>{{ $address->contact_person }}</td>
            <td>{{ $address->contact_phone }}</td>
            <td>     <b>{{ $address->address_label }} : </b> <br>
                {{ $address->street }}, {{  $address->zip_code  }}, {{  $address->city  }}</td>
        </tr>
        @empty
        <tr>
            <td colspan="3">
                <center><i>Pas de données</i></center>
            </td>
        </tr>

    @endforelse

</tbody>
</table>
