<table class="table table-sm table-responsive-sm table-striped">
    <thead>
        <tr>
            <th>Code</th>
            <th>Periode</th>
            <th>Etat</th>
            <th>Paiment</th>
            <th>Montant</th>
            <th>Caution</th>
        </tr>
    </thead>
    <tbody>

        @forelse ($bookings as $booking)

            @php
                $status = @$booking->currentStatus ?? '';

                $status_name = $status->name;

                $badge_class = '';

                switch ($status->id) {
                    case 1:
                        $badge_class = 'warning text-white';
                        break;
                    case 2:
                        $badge_class = 'info text-white';
                        break;
                    case 3:
                        $badge_class = 'success text-white';
                        break;
                    case 4:
                        $badge_class = 'danger text-white';
                        break;
                    case 5:
                        $badge_class = 'secondary';
                        break;
                    case 6:
                        $badge_class = 'dark text-white';
                        break;
                    default:
                        # code...
                        break;
                }


            @endphp

            <tr>
                <td>
                    <a href="{{ route('admin.bookings.details', $booking) }}">#{{ $booking->code }}</a>
                </td>

                <td>
                    <span
                        class="small">{{ Str::ucfirst($booking->start_date->translatedFormat('l d/m')) }}
                        -
                        {{ Str::ucfirst($booking->end_date->translatedFormat('l d/m')) }}<br>
                        {{ $booking->start_date->diffInDays($booking->end_date) }} jours</span>
                </td>
                <td>
                    <span class="badge bg-{{ $booking->badge_class }}" data-toggle="todotooltip"
                        data-placement="top" title="">
                        {{ $status_name }}
                    </span>
                </td>

                <td>
                    @if ($booking->payment_recieved == 1)
                        <span class="badge bg-success text-white">
                            PAIEMENT RECU
                        </span>
                    @endif

                    @if ($booking->deposit_refunded == 1)
                        <span class="badge bg-primary text-white">
                            CAUTION RENDU
                        </span>
                    @endif
                </td>
                <td>
                    <strong>{{ $booking->price }} DT</strong>
                </td>

                <td>
                    {{ $booking->deposit }} DT
                </td>

            </tr>
        @empty
            <tr>
                <td colspan="8">
                    <center><i>Pas de données</i></center>
                </td>
            </tr>

        @endforelse

    </tbody>
</table>
