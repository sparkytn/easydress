<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand">
        <a href="{{ route('administrator.dashboard') }}">
        <img class="c-sidebar-brand-full" width="118"
        height="46" src="{{ asset('images/EasyDress-logo-white.svg') }}" alt="Easy Dress">
        </a>

    </div>
    <nav class="c-sidebar-nav">
        <ul class="c-sidebar-nav">
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="{{ route('administrator.dashboard') }}">
                    <svg class="c-sidebar-nav-icon">
                        <use
                            xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-speedometer') }}">
                        </use>
                    </svg>
                    Dashboard</a>
            </li>
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                    <svg class="c-sidebar-nav-icon">
                        <use
                            xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-cart') }}">
                        </use>
                    </svg>Commandes</a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.bookings.index') }}"><span class="c-sidebar-nav-icon"></span> Toutes
                            les commandes</a></li>
                    @foreach ($allStatus as $status)
                        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                                href="{{ route('admin.bookings.index', ['status' => $status->id]) }}"><span
                                    class="c-sidebar-nav-icon"></span> > {{ $status->name }}</a></li>
                    @endforeach

                </ul>
            </li>

            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                    <svg class="c-sidebar-nav-icon">
                        <use
                            xlink:href="{{ asset('/admin_assets/icons/sprites/free.svg#cil-truck') }}">
                        </use>
                    </svg>
                    Expeditions et Retours</a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.delivery') }}"><span class="c-sidebar-nav-icon"></span> Les expeditions</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.delivery.returns') }}"><span
                                class="c-sidebar-nav-icon"></span> Les retours</a></li>

                </ul>
            </li>

            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                    <svg class="c-sidebar-nav-icon">
                        <use
                            xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-people') }}">
                        </use>
                    </svg>
                    Clients</a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.customers.index') }}"><span class="c-sidebar-nav-icon"></span> Liste
                            clients</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.submitedproducts.index') }}"><span
                                class="c-sidebar-nav-icon"></span> Demande d'ajout produit</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.commissions.index') }}"><span
                                class="c-sidebar-nav-icon"></span> Commissions</a></li>
                </ul>
            </li>

            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
                    <svg class="c-sidebar-nav-icon">
                        <use
                            xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-view-quilt') }}">
                        </use>
                    </svg> Catalogue</a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.products.index') }}"><span class="c-sidebar-nav-icon"></span>
                            Produits</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.brands.index') }}"><span class="c-sidebar-nav-icon"></span>
                            Marques</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.categories.index') }}"><span class="c-sidebar-nav-icon"></span>
                            Categories</a></li>
                    <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link"
                            href="{{ route('admin.attributes.index') }}"><span class="c-sidebar-nav-icon"></span>
                            Attribues</a></li>
                </ul>
            </li>


            <li class="c-sidebar-nav-item">
                <form action="{{ route('logout') }}" method="POST">
                    @csrf
                    <button type="submit" class="c-sidebar-nav-link" href="" target="_top">
                        <svg class="c-sidebar-nav-icon">
                            <use
                                xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-account-logout') }}">
                            </use>
                        </svg>
                    Deconnexion
                    </button>
                </form>
            </li>

        </ul>
    </nav>
    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent"
        data-class="c-sidebar-minimized"></button>
</div>
