<div class="container">
    <div class="row">
        <div class="col-md-8">
            <dl class="row">
                <dt class="col-sm-3 border-bottom">Réf#</dt>
                <dd class="col-sm-9 ">{{ ProductService::generateRef($product) }}</dd>
                <dt class="col-sm-3 border-bottom">Proprietaire</dt>
                <dd class="col-sm-9 ">{{ $product->user->name }}</dd>
                <dt class="col-sm-3 border-bottom">Marque</dt>
                <dd class="col-sm-9 ">{{ $product->brand->name }}</dd>
                <dt class="col-sm-3 border-bottom">Categorie</dt>
                <dd class="col-sm-9 ">{{ $product->category->name }}</dd>
                <dt class="col-sm-3 border-bottom">Prix location</dt>
                <dd class="col-sm-9  ">{{ $product->rent_price }} DT</dd>
                <dt class="col-sm-3 border-bottom">Prix d'achat</dt>
                <dd class="col-sm-9">{{ $product->value_price }} DT</dd>
                @foreach ($product->attributes_values as $val)
                    <dt class="col-sm-3 border-bottom">{{ $val->attribute->name }}</dt>
                    <dd class="col-sm-9">{{ $product->attributes_values()->find($val->id)->value }}
                    </dd>
                @endforeach
                <dt class="col-sm-3 border-bottom">Style</dt>
                <dd class="col-sm-9">{{ $product->style_description }}</dd>
                <dt class="col-sm-3 border-bottom">Composition</dt>
                <dd class="col-sm-9">{{ $product->composition_description }}</dd>
                <dt class="col-sm-3 border-bottom">Size</dt>
                <dd class="col-sm-9">{{ $product->size_description }}</dd>
            </dl>
        </div>
        <div class="col-md-4">
            <img id="main_image" src="https://via.placeholder.com/200x360.png/CCCCCC?text=EasyDress"
                class="img-fluid img-thumbnail float-right" alt="Responsive image" />

        </div>
    </div>
</div>