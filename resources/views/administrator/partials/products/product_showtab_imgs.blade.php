<div class="container">
    <div class="component">

        <div class="overlay">
            <div class="d-flex justify-content-center">
                <div class="spinner-border text-primary mt-5 pt-5" style="width: 5rem; height: 5rem;" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>

        </div>

        <input type="hidden" id="product_id" value="{{ $product->id }}"/>

        <div id="drop-area" class="dm-uploader p-2 text-center">
            <h4 class="mb-1 mt-2 text-muted">Deposez des images ici</h4>
            <small class="d-block mb-3">Resolution min. : 600px X 900px (ratio: 2:3) | Extention : .jpg ou .jpeg | max 2Mo</small>
            <div class="btn btn-primary  mb-3">
                <span>Ajouter des images</span>
                <input type="file" title="Ajouter des images" accept="image/jpeg,image/jpg">
            </div>
            <br>

        </div>

        <div id="gallery_container">


        </div>

    </div>
</div>


<!-- Error Modal -->
<div class="modal fade" id="errorModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Erreur</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>
            </div>
            <div class="modal-body">
                <div id="errorImageMessage"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>


@push('scripts')
    <script type="text/javascript" src="{{ asset('admin_assets/js/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin_assets/js/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin_assets/js/jquery.dm-uploader.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('admin_assets/js/image_gallery.js') }}"></script>
    <script>
        $('document').ready(function () {
            $('#drop-area').dmUploader({
                url: "{{ route('admin.products.upload_images') }}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                maxFileSize: 2000000,
                multiple: false,
                extFilter: ["jpg", "jpeg"],
                allowedTypes: "image/*",
                fieldName: 'image',
                extraData: {
                    "product_id": $('#product_id').val()
                },
                onUploadError: function (id, xhr) {
                    response = xhr.responseJSON;
                    $('#errorImageMessage').html('');
                    console.log(response.errors);
                    if (response.errors) {
                        $.each(response.errors, function (key, messages) {
                            $msgs = ''
                            $.each(messages, function (i, msg) {
                                $msgs += ' - ' + msg + ' <br>'
                            })
                            $('#errorImageMessage').append(`<p><b> ${key} : </b> <br> ${$msgs}</p>`)
                        })
                        $('#errorModal').modal('show');

                    }
                },
                onComplete: function (response) {
                    load_gallery();
                },
                onNewFile: function () {
                    $('.overlay').show();
                },
                onUploadProgress: function (id, percent) {
                }
            });

            var load_gallery = function () {
                $('.overlay').show();
                $.get({
                    url: "{{ route('admin.products.load_gallery', $product) }}"
                })
                    .done(function (data) {

                        $('#gallery_container').html(data.gallery);
                        $('#image_count').html(data.imgs_count);

                        $("#sortable").sortable({
                            cursor: 'move',
                            forcePlaceholderSize: true,
                            placeholder: 'placeholder',
                            update: function (event, ui) {
                                //reorder
                                var sortIDs = $(this).sortable('toArray').toString();
                                var reorder_url =
                                    "{{ route('admin.products.reorder_images', ':order') }}";
                                reorder_url = reorder_url.replace(':order', sortIDs);
                                $.get({
                                    url: reorder_url
                                })
                                    .done(function (data) {
                                        load_gallery();
                                    });
                            }
                        });

                        $('.delete_img').click(function (el) {
                            if (confirm('Voulez vous vraiement supprimer cette image?')) {
                                $('.overlay').show();
                                let image_id = $(this).children('input.image_id').val();
                                let product_id = $('#product_id').val();
                                let del_url =
                                    "{{ route('admin.products.delete_image', [':image_id', ':product_id']) }}";
                                del_url = del_url.replace(':image_id', image_id);
                                del_url = del_url.replace(':product_id', product_id);
                                $.get({
                                    url: del_url
                                })
                                    .done(function (data) {
                                        load_gallery();
                                    });
                            }
                        });
                        var first_img_path = $('#sortable').find('img').first().attr('src');
                        $('#main_image').attr('src', first_img_path);
                        $('.overlay').hide();
                    });


            };
            load_gallery();
        });

    </script>

@endpush

@push('styles')
    <link rel="stylesheet" href="{{ asset('admin_assets/css/image_gallery.css') }}"/>
@endpush
