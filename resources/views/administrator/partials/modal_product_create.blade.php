<div class="modal fade" id="add_product_modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('admin.products.create_new') }}" method="POST" id="modal_form">
                
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalLabel">Nouveau produit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label for="category" class="col-form-label">Choisir la categorie produit</label>
                        <select class="form-control" id="category" name="category">
                            <option value="0"></option>
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="user" class="col-form-label">A qui appartient ce produit?</label>
                        <select class="form-control" id="user" name="user">
                            <option value="0"></option>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                    </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button type="button" class="btn btn-primary" id="save_product_btn">Ajouter</button>
        </div>
        </form>
    </div>
</div>
</div>

@push('scripts')
<script type="text/javascript" src="{{ asset('admin_assets/js/jquery.js') }}"></script>
<script>
    $('document').ready(function(){

        $('#add_product_btn').click(function(){
            
            $('#add_product_modal').modal({backdrop: 'static', keyboard: false});
            
        });
 
        $('#save_product_btn').click(function(){
             if($('#category').val() != '0' && $('#user').val() !='0'){
                $('#modal_form').submit();
             }else{
                alert('Veuillez choisir une categorie et un utilisateur !');
             }
        });
    });
</script>

@endpush