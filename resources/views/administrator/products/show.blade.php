@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')


    <div class="container-fluid">
        <p class="h3">{{ $product->name }} </p>
        <small><a href="#" target="blank">Voir en ligne</a></small>
        <hr>
        <div class="nav-tabs-boxed">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home-1" role="tab"
                        aria-controls="home" aria-selected="true">General</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile-1" role="tab"
                        aria-controls="profile" aria-selected="false">Images <span class="badge bg-secondary ml-1" id="image_count">0</span></a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#messages-1" role="tab"
                        aria-controls="messages" aria-selected="false">Historique commande</a></li>
            </ul>
            <div class="tab-content ">
                <div class="tab-pane active" id="home-1" role="tabpanel">

                    @include('administrator.partials.products.product_showtab_general')  

                </div>
                <div class="tab-pane" id="profile-1" role="tabpanel">
                    @include('administrator.partials.products.product_showtab_imgs')
                </div>
                <div class="tab-pane" id="messages-1" role="tabpanel">Historique des ventes, mise a jour....</div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')


@endsection
