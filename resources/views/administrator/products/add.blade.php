@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')

    <div class="container-fluid">

        <div class="card">


            <form class="form-horizontal" action="{{ route('admin.products.store') }}" method="post">

                @csrf

                <div class="d-flex justify-content-between card-header">
                    <div>
                        <h4 class="card-title mb-0">Ajouter un produit</h4>

                    </div>

                </div>
                <div class="card-body">

                    <div class="form-group row mb-3">
                        <label class="col-md-3 col-form-label" for="name">Nom</label>
                        <div class="col-md-9">
                            <input
                                class="form-control  @error('name') is-invalid @enderror"
                                id="name"
                                type="text"
                                name="name"
                                required
                                value="{{ old('name') }}"
                                autocomplete="text">
                            @error('name')
                            <div class="text-danger p-1 small">{{ $message }}</div>
                            @enderror

                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label class="col-md-3 col-form-label" for="name">Slug (Url du produit)</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">{{ route('front.shop') }}/</span>
                                </div>
                                <input type="text" value="{{ old('slug') }}" name="slug" id="slug" class="form-control @error('slug') is-invalid @enderror" placeholder="slug" required autocomplete="off">
                            </div>
                            @error('slug')
                            <div class="text-danger p-1 small">{{ $message }}</div>
                            @enderror

                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label class="col-md-3 col-form-label" for="user">Marque</label>
                        <div class="col-md-6">
                            <select name="brand"
                            class="form-control @error('brand') is-invalid @enderror">
                            <option value="">Selectionnez...</option>
                                @foreach($brands as $brand)

                                    <option
                                    @if(old('brand')==$brand->id) selected="selected" @endif
                                        value="{{ $brand->id }}">
                                            {{ $brand->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row  mb-3">
                        <label class="col-md-3 col-form-label" for="rent_price">Prix location</label>
                        <div class="col-md-3 input-group">
                            <input
                                class="form-control  @error('rent_price') is-invalid @enderror"
                                id="rent_price"
                                type="text"
                                name="rent_price"
                                required
                                value="{{ old('rent_price') }}">
                                <div class="input-group-append"><span class="input-group-text">DT</span></div>
                        </div>
                        <label class="col-md-3 col-form-label text-right" for="value_price">Prix Achat</label>
                        <div class="col-md-3 input-group">
                            <input
                                class="form-control  @error('value_price') is-invalid @enderror"
                                id="value_price"
                                type="text"
                                name="value_price"
                                value="{{ old('value_price') }}"
                                required>
                            <div class="input-group-append"><span class="input-group-text">DT</span></div>
                        </div>

                    </div>

                    @include('administrator.partials.attributes_fields')

                    <div class="form-group row mb-3">
                        <label class="col-md-3 col-form-label" for="name">Style</label>
                        <div class="col-md-9">
                            <textarea
                                class="form-control  @error('style_description') is-invalid @enderror"
                                id="style_description"
                                name="style_description">{{ old('style_description') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label class="col-md-3 col-form-label" for="composition_description">Composition</label>
                        <div class="col-md-9">
                            <textarea
                                class="form-control  @error('composition_description') is-invalid @enderror"
                                id="composition_description"
                                name="composition_description">{{ old('composition_description') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row mb-3">
                        <label class="col-md-3 col-form-label" for="size_description">Taille et mesure</label>
                        <div class="col-md-9">
                            <textarea
                                class="form-control  @error('size_description') is-invalid @enderror"
                                id="size_description"
                                name="size_description">{{ old('size_description') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="is_online">Mettre en ligne</label>
                        <div class="col-md-9">

                            <label class="c-switch c-switch-label c-switch-pill c-switch-success">
                                <input id="is_online" class="c-switch-input" name="is_online" value="1" type="checkbox" @if(old('is_online' ) == true) checked="" @endif><span class="c-switch-slider" data-checked="On" data-unchecked="Off"></span>
                            </label>
                        </div>
                    </div>

                    <input type="hidden" name="category" value="{{$selected_category }}"/>
                    <input type="hidden" name="user" value="{{ $selected_user }}"/>
                    @if(old('submited_id'))
                    <input type="hidden" name="submited_id" value="{{ old('submited_id') }}"/>
                    @endif
                </div>
                <div class="card-footer ">
                    <button class="btn btn btn-primary" type="submit"> Enregistrer</button>
                    <button class="btn btn btn-secondary" type="reset"
                        onclick="window.location.href='{{ route('admin.products.index') }}'"> Annuler</button>
                </div>

            </form>



        </div>

    </div>

@endsection

@section('javascript')
    <script type="text/javascript" src="{{ asset('admin_assets/js/jquery.js') }}"></script>
    <script>
        $(document).ready(function () {
            String.prototype.slugify = function (separator = "-") {
                return this
                    .toString()
                    .normalize('NFD')                   // split an accented letter in the base letter and the acent
                    .replace(/[\u0300-\u036f]/g, '')   // remove all previously split accents
                    .toLowerCase()
                    .trim()
                    .replace(/[^a-z0-9 ]/g, '')   // remove all chars not letters, numbers and spaces (to be replaced)
                    .replace(/\s+/g, separator);
            };

            $('#name').on('change', function () {
                $('#slug').val($(this).val().slugify())
            })


        })
    </script>


@endsection
