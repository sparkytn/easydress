@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')

    @include('administrator.partials.modal_product_create')

    <div class="container-fluid">

        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between mb-3">
                    <div>
                        <h4 class="card-title mb-0">Produits </h4>

                    </div>
                    <div class="btn-toolbar d-none d-md-block" role="toolbar" aria-label="Toolbar with buttons">

                        <button type="submit" class="btn btn-success text-decoration-none" id="add_product_btn">
                            <svg class="c-icon">
                                <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-plus"></use>
                            </svg>
                            Ajouter
                        </button>
                    </div>
                </div>


                <form action="{{ route('admin.products.index','search') }}" method="GET">
                    @csrf
                    <div class="form-row">
                        <div class="col">
                            <input type="text" name='product_name' class="form-control" placeholder="Nom du produit">
                        </div>
                        <div class="col">
                            <select name="category" id="inputState" class="form-control">
                                <option value="" >Selectionnez une categorie</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                 @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <select value=""name="user"  id="inputState" class="form-control">
                                <option value="" >Selectionnez un proprietaire</option>
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-primary mb-2">Submit</button>
                        </div>
                    </div>
                </form>


                <table class="table table-sm table-responsive-sm table-striped">
                    <thead>
                        <tr>
                            <th>Réf#</th>
                            <th>Image</th>
                            <th>Nom</th>
                            <th>Marque</th>
                            <th>Categorie</th>
                            <th>Prix location</th>
                            <th>Proprio.</th>
                            <th>Actif</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @forelse ($products as $product)

                            @php

                                $image = $product->images->first();

                                if ($image) {
                                    $image_path = asset('storage/imgs/'. $image->file_name) ;
                                } else {
                                    $image_path = 'https://via.placeholder.com/40x40?text=No+image';
                                }

                            @endphp

                            <tr>
                                <td>
                                    {{ ProductService::generateRef($product) }}
                                </td>
                                <td style="max-width:50px">
                                    <a href="{{ route('admin.products.show', $product) }}">

                                        <img src="{{ $image_path }}" width="90" class="img-fluid img-thumbnail float-left" />
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.products.show', $product) }}">
                                        <b>{{ Str::limit($product->name, 40) }}</b>
                                    </a>
                                    <br>
                                    <small>Date d'ajout : {{ $product->created_at->format('d/m/Y') }}</small>
                                </td>
                                <td>
                                    {{ $product->brand->name }}
                                </td>
                                <td>
                                    {{ $product->category->name }}
                                </td>
                                <td>
                                    {{ $product->rent_price }} DT
                                    <br>
                                    <small>Prix achat: <i>{{ $product->value_price }} DT</i></small>
                                </td>
                                <td>
                                    {{ $product->user->name }}
                                </td>
                                <td>
                                    @if ($product->is_online)
                                        <span class="badge badge-success">En ligne</span>
                                    @else
                                        <span class="badge badge-secondary">Hors Ligne</span>
                                    @endif

                                </td>
                                <td class="text-right">
                                    <form action="{{ route('admin.products.destroy', $product) }}" method="POST">
                                        <a class="btn-sm" href="{{ route('admin.products.edit', $product) }}">
                                            <svg class="c-icon">
                                                <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-pencil"></use>
                                            </svg></a>

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-link d-inline"
                                            onclick="return confirm('Voulez vous supprimer cet enregistrement?')">

                                            <svg class="c-icon">
                                                <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-trash"></use>
                                            </svg></button>

                                    </form>


                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2">
                                    <center><i>Pas de données</i></center>
                                </td>
                            </tr>

                        @endforelse

                    </tbody>
                </table>

                {{ $products->links() }}
            </div>
        </div>

    </div>

@endsection

@section('javascript')


@endsection
