
    <div class="d-flex flex-row" id="sortable">
@forelse($images as $image)
        <div class="card  pl-0 pr-0 mx-2" id="sortable_{{ $image->id}}">

            <img class="bd-placeholder-img card-img-top" width="120" height="180" src="{{ asset('storage/imgs/gallery_thumb_'.$image->file_name) }}" />

            <div class="card-body">
                <a class="delete_img"  role="button">
                    <input type="hidden" value="{{ $image->id}}" class="image_id" />
                    <svg class="c-icon">
                      <use xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-trash')}}"></use>
                  </svg>
                </a>

            <span class="float-right">#{{ $image->order}}</span>
            </div>
        </div>
@empty
    <center><i>Aucune image</i></center>
@endforelse
    </div>

