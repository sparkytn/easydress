@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')

    <div class="container-fluid">

        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between mb-3">
                    <div>
                        <h4 class="card-title mb-0">Détails produit proposé
                            @if ($product->is_accepted==1)
                            <span class="badge badge-success">Validé</span>
                            @elseif($product->is_accepted==0)
                            <span class="badge badge-secondary">Pas encore validé</span>
                            @elseif($product->is_accepted==2)
                            <span class="badge badge-warning">Archivé</span>
                            @endif

                            @if ($product->migrated!=0)
                            <span class="badge badge-info">MIGRE</span>
                            @endif
                    </h4>

                    </div>

                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <dl class="row">
                            <dt class="col-sm-3 border-bottom">Nom</dt>
                            <dd class="col-sm-9 ">{{ $product->name }}</dd>
                            <dt class="col-sm-3 border-bottom">Proprietaire</dt>
                            <dd class="col-sm-9 ">{{ $product->user->name }}</dd>
                            <dt class="col-sm-3 border-bottom">Marque</dt>
                            <dd class="col-sm-9 ">{{ $product->marque }}</dd>
                            <dt class="col-sm-3 border-bottom">Categorie</dt>
                            <dd class="col-sm-9 ">{{ $product->category->name }}</dd>
                            <dt class="col-sm-3 border-bottom">Prix location</dt>
                            <dd class="col-sm-9  ">{{ $product->rent_price }} DT</dd>
                            <dt class="col-sm-3 border-bottom">Prix d'achat</dt>
                            <dd class="col-sm-9">{{ $product->value_price }} DT</dd>
                            @foreach ($product->attributes_values as $val)
                                <dt class="col-sm-3 border-bottom">{{ $val->attribute->name }}</dt>
                                <dd class="col-sm-9">{{ $product->attributes_values()->find($val->id)->value }}
                                </dd>
                            @endforeach
                            <dt class="col-sm-3 border-bottom">Note</dt>
                            <dd class="col-sm-9">{{ $product->note }}</dd>
                        </dl>
                    </div>
                    <div class="col-md-6">
                        <img src="{{ asset('storage/images/submited_products/'.$product->img1) }}"
                                            class="img-fluid img-thumbnail float-left" />
                        <br />
                        @if ($product->img2 != null)
                            <img  src="{{ asset('storage/images/submited_products/'.$product->img2) }}"
                                class="img-fluid img-thumbnail float-right" alt="Responsive image" /><br />
                        @endif
                        @if ($product->img3 != null)
                            <img src="{{ asset('storage/images/submited_products/'.$product->img3) }}"
                                class="img-fluid img-thumbnail float-right" alt="Responsive image" />
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-footer">
                @if($product->is_accepted==0)
                <form action="{{ route('admin.submitedproducts.accept') }}" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{ $product->id }}" />
                    <button class="btn btn btn-success" type="submit" name="accept" value="accept">ACCEPTER</button>
                    <button onclick="return confirm('Voulez vous vraiment archiver cette proposition?')" class="btn btn btn-warning" type="submit" name="archive" value="archive">ARCHIVER </button>
                </form>
                @elseif($product->is_accepted==1 && $product->migrated==0)
                <form action="{{ route('admin.submitedproducts.migrate') }}" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{ $product->id }}" />
                    <button class="btn btn btn-info" type="submit" name="migrate" value="migrate">MIGRER</button>
                </form>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('javascript')


@endsection
