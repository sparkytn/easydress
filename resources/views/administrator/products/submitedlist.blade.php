@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')



    <div class="container-fluid">

        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between mb-3">
                    <div>
                        <h4 class="card-title mb-0">Produits proposés</h4>

                    </div>

                </div>
<hr />
                <nav class="navbar navbar-light bg-light">
                <form action="{{ route('admin.submitedproducts.index') }}" method="GET">
                    @csrf
                    <div class="form-row">
                        <div class="col">
                            <input type="text"
                                    name='product_name'
                                    class="form-control"
                                    placeholder="Nom du produit"
                                    value="{{ request('product_name') }}">
                        </div>
                        <div class="col">
                            <select name="category" id="inputState" class="form-control">
                                <option value="">Selectionnez une categorie</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}" @if (request('category') == $category->id) selected="selected" @endif>{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <select value="" name="user" id="inputState" class="form-control">
                                <option value="">Selectionnez un proprietaire</option>
                                @foreach ($users as $user)
                                    <option value="{{ $user->id }}" @if (request('user') == $user->id) selected="selected" @endif>{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <select value="" name="accepted" id="inputState" class="form-control">
                                <option value="">Selectionnez un etat</option>
                                <option value="0" @if (request('accepted') == "0") selected="selected" @endif>Pas encore validé</option>
                                <option value="1" @if (request('accepted') == "1") selected="selected" @endif>Validé</option>
                            </select>
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-primary mb-2">Submit</button>
                            <button type="reset" class="btn btn-secondary mb-2" onclick="window.location.href='{{ route('admin.submitedproducts.index') }}'">Annuler</button>
                        </div>
                    </div>
                </form>
                </nav>

                <table class="table table-sm table-responsive-sm table-striped">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Nom</th>
                            <th>Marque</th>
                            <th>Categorie</th>
                            <th>Prix location</th>
                            <th>Proprio.</th>
                            <th>Date ajout</th>
                            <th>Validé</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @forelse ($products as $product)

                            <tr>
                                <td style="max-width:50px">
                                    <a href="{{ route('admin.submitedproducts.show', $product) }}">

                                        <img src="{{ asset('storage/images/submited_products/'.$product->img1) }}" width="100%"
                                            class="img-fluid img-thumbnail float-left" />
                                    </a>
                                </td>
                                <td>
                                    <a
                                        href="{{ route('admin.submitedproducts.show', $product) }}">{{ Str::limit($product->name, 40) }}</a>
                                </td>
                                <td>
                                    {{ $product->marque }}
                                </td>
                                <td>
                                    {{ $product->category->name }}
                                </td>
                                <td>
                                    {{ $product->rent_price }} DT
                                    <br>
                                    <small>Prix achat: <i>{{ $product->value_price }} DT</i></small>
                                </td>
                                <td>
                                    {{ $product->user->name }}
                                </td>
                                <td>
                                    {{ $product->created_at->format('d/m/Y H:i') }}
                                </td>
                                <td>
                                    @if ($product->is_accepted)
                                        <span class="badge badge-success">Validé</span>
                                    @else
                                        <span class="badge badge-secondary">Pas encore validé</span>
                                    @endif
                                    @if ($product->migrated)
                                        <span class="badge badge-info">MIGRE</span>
                                    @endif
                                </td>
                                <td class="text-right">
                                    <form action="{{ route('admin.submitedproducts.accept') }}" method="POST">
                                        @csrf
                                        <input type="hidden" value="{{ $product->id }}" name="id" />

                                        <button type="submit" name="archive" value="archive" class="btn btn-link d-inline"
                                            onclick="return confirm('Voulez vous vraiement archivé cette proposition?')">

                                            <svg class="c-icon">
                                                <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-trash"></use>
                                            </svg></button>

                                    </form>


                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="8">
                                    <center><i>Pas de données</i></center>
                                </td>
                            </tr>

                        @endforelse

                    </tbody>
                </table>

                {{ $products->links() }}
            </div>
        </div>

    </div>

@endsection

@section('javascript')


@endsection
