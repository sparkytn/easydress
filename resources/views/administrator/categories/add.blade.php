@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')

    <div class="container-fluid">

        <div class="card">


            <form class="form-horizontal" action="{{ route('admin.categories.store') }}" method="post">
                <div class="d-flex justify-content-between mb-5 card-header">
                    <div>
                        <h4 class="card-title mb-0">Ajouter une categorie</h4>

                    </div>

                </div>
                <div class="card-body">

                    @csrf
                    <div class="row mb-3">
                        <label class="col-md-3 col-form-label" for="name">Nom</label>
                        <div class="col-md-9">
                            <input class="form-control  @if ($errors->any()) is-invalid @endif" id="name" type="text" name="name"
                            value="{{ old('name') }}"
                            autocomplete="text"><span class="form-text">Saisir le nom de categorie</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Attribues</label>
                        <div class="col-md-9 col-form-label">

                            @foreach ($attributes as $attribute)
                                
                                <div class="form-check checkbox">
                                    <input name="attributes[]" 
                                       @if(old('attributes') && in_array($attribute->id,old('attributes')) )
                                         checked="checked" 
                                        @endif
                                        class="form-check-input" id="check{{ $attribute->id}}" type="checkbox" value="{{ $attribute->id}}">
                                    <label class="form-check-label" for="check{{ $attribute->id}}">{{ $attribute->name}}</label>
                                </div>

                            @endforeach

                        </div>
                    </div>
                </div>
                <div class="card-footer ">
                    <button class="btn btn btn-primary" type="submit"> Enregistrer</button>
                    <button class="btn btn btn-secondary" type="reset"
                        onclick="window.location.href='{{ route('admin.categories.index') }}'"> Annuler</button>
                </div>

            </form>



        </div>

    </div>

@endsection

@section('javascript')


@endsection
