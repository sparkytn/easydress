@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')



    <div class="container-fluid">

        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between mb-3">
                    <div>
                        <h4 class="card-title mb-0">Commissions </h4>

                    </div>

                </div>

                <hr>
                <nav class="navbar navbar-light bg-light">
                    <form action="" method="GET">
                        @csrf
                        <div class="form-row">
                            <div class="col">
                                <select name="user" id="inputState" class="form-control">
                                    <option value="">Selectionnez un proprietaire</option>
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}" @if (request('user') == $user->id) selected="selected" @endif>{{ $user->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col">
                                <select name="status" id="inputState" class="form-control">
                                    <option value="">Selectionnez un etat</option>

                                    <option value="1" @if (request('status') == '1') selected="selected" @endif>Pas encore payée
                                    </option>
                                    <option value="2" @if (request('status') == '2') selected="selected" @endif>Payée
                                    </option>
                                </select>
                            </div>
                            <div class="col">
                                <input type="text" name='booking' class="form-control" placeholder="No commande"
                                    value="{{ request('booking') }}">
                            </div>
                            <div class="col">
                                <button type="submit" class="btn btn-primary mb-2" name="filter">Chercher</button>
                                <button type="reset" class="btn btn-secondary mb-2"
                                    onclick="window.location.href='{{ route('admin.commissions.index') }}'">Annuler</button>
                            </div>
                        </div>
                    </form>
                </nav>

                <table class="table table-sm table-responsive-sm table-striped">
                    <thead>
                        <tr>
                            <th>Propriétaire</th>
                            <th>No. Commande</th>
                            <th>Montant</th>
                            <th>Etat</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @forelse ($commissions as $commission)

                            <tr>
                                <td> <b>#{{ $commission->user->id }} - {{ $commission->user->name }}</b> <br>  <small>{{ $commission->user->email }}</small> </td>
                                <td><b>{{ $commission->booking->code }}</b><br> <small>LE {{ $commission->booking->created_at->format('Y-m-d') }}</small></td>
                                <td>{{ $commission->amount }} DT</td>
                                <td>
                                    @if ($commission->status == '1') Pas encore payée
                                    @endif
                                    @if ($commission->status == '2') Payée le:
                                        {{ $commission->payment_date->translatedFormat('d/m/Y H:i') }}@endif
                                </td>
                                <td>
                                    @if ($commission->status == '1')
                                        <a href="{{ route('admin.commissions.pay', $commission->id) }}"
                                            onclick="return confirm('Voulez vous payer cette commission?\n\rMontant: {{ $commission->amount }} DT');">PAYER</a>
                                    @endif
                                </td>
                            </tr>

                            @empty
                                <tr>
                                    <td colspan="6">
                                        <center><i>Pas de données</i></center>
                                    </td>
                                </tr>

                            @endforelse

                        </tbody>
                    </table>

                    {{ $commissions->links() }}
                </div>
            </div>

        </div>

    @endsection

    @push('scripts')

    @endpush
    @section('javascript')


    @endsection
