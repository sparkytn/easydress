@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')


    <div class="container-fluid">

        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between mb-5">
                    <div>
                        <h4 class="card-title mb-0">Attribues </h4>

                    </div>
                    <div class="btn-toolbar d-none d-md-block" role="toolbar" aria-label="Toolbar with buttons">

                        <a class="btn btn-success text-decoration-none" href="{{ route('admin.attributes.create') }}">
                            <svg class="c-icon">
                                <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-plus"></use>
                            </svg>
                            Ajouter
                        </a>
                    </div>
                </div>

                <table class="table table-responsive-sm table-striped">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @forelse ($attributes as $attribute)
                            <tr>
                                <td>
                                   {{ $attribute->name }}
                                            <br />
                                    <small><a href="{{ route('admin.attributes.show',$attribute) }}" class='pl-3'>
                                    <svg class="c-icon">
                                        <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-playlist-add"></use>
                                    </svg>
                                    Valeures
                                    </a></small>
                                </td>
                                <td class="text-right">
                                    <form action="{{ route('admin.attributes.destroy', $attribute->id) }}" method="POST">
                                    <a class="btn-sm" href="{{ route('admin.attributes.edit', $attribute->id) }}">
                                        <svg class="c-icon">
                                            <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-pencil"></use>
                                        </svg></a>
                                    
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-link d-inline" onclick="return confirm('Voulez vous supprimer cet enregistrement?')">
                                            
                                                <svg class="c-icon">
                                                    <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-trash"></use>
                                                </svg></button>

                                    </form>
                                    

                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="2"><center><i>Pas de données</i></center></td>
                            </tr>

                        @endforelse

                    </tbody>
                </table>

               
            </div>
        </div>

    </div>

@endsection

@section('javascript')


@endsection
