@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')

<?php //dd($attribute->values) ; ?> 
    <div class="container-fluid">

        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between mb-5">
                    <div>
                        <h4 class="card-title mb-0">Valeures pour : {{ $attribute->name }} </h4>

                    </div>

                </div>
                <hr>

                <form class="form-inline " method="POST"
                    action="{{ route('admin.attributes.attribute_values.store', [$attribute]) }}">
                    @csrf

                    <div class="form-group mb-2">
                        <label class="">Ajouter une nouvelle valeur</label>
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <input type="text" class="form-control" name="value">
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Enregistrer</button>
                </form>

                <table class="table table-responsive-sm table-striped">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($attribute->values as $value)
                            <tr>
                                <td>
                                    {{ $value->value }}
                                </td>
                                <td>
                                    <form action="{{ route('admin.attributes.attribute_values.destroy', [$attribute,$value]) }}"
                                        method="POST">

                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-link d-inline"
                                            onclick="return confirm('Voulez vous supprimer cet enregistrement?')">

                                            <svg class="c-icon">
                                                <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-trash"></use>
                                            </svg></button>

                                    </form>
                                </td>
                            </tr>

                        @empty
                            <tr>
                                <td colspan="2">
                                    <center><i>Pas d'enregistrement </i></center>
                                </td>
                            </tr>
                        @endforelse





                    </tbody>
                </table>


            </div>
        </div>

    </div>

@endsection

@section('javascript')


@endsection
