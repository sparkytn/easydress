@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')


    <div class="container-fluid">

        <div class="card">

           
                <form class="form-horizontal" action="{{ route('admin.attributes.update',$attribute->id) }}" method="post">
                    <div class="d-flex justify-content-between mb-5 card-header">
                        <div>
                            <h4 class="card-title mb-0">Modifier attribue</h4>

                        </div>

                    </div>
                    <div class="card-body">

                    @csrf
                    @method('PUT')
                    
                    <div class="row mb-3">
                        <label class="col-md-3 col-form-label" for="name">Nom</label>
                        <div class="col-md-9">
                            <input class="form-control @if($errors->any()) is-invalid @endif" id="name" type="text" name="name" value="{{ old('name', $attribute->name) }}"
                                autocomplete="text">
                                @if($errors->any())
                                    <span class="form-text invalid-feedback">Ce champ est obligatoire</span>                                  
                                @else
                                    <span class="form-text">Saisir le nom de l'attribue</span>   
                                @endif
                        </div>
                    </div>
                </div>
                    <div class="card-footer align-left">
                        <button class="btn btn btn-primary" type="submit"> Enregistrer</button>
                        <button class="btn btn btn-secondary" type="reset" onclick="window.location.href='{{ route('admin.attributes.index') }}'"> Annuler</button>
                    </div>
                </form>

            

        </div>

    </div>

@endsection

@section('javascript')


@endsection
