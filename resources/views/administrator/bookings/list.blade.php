@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')



    <div class="container-fluid">

        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between mb-3">
                    <div>
                        <h4 class="card-title mb-0">Commandes </h4>

                    </div>

                </div>

                <hr>
                <nav class="navbar navbar-light bg-light">
                    <form action="" method="GET">
                        @csrf
                        <div class="form-row">
                            <div class="col-1">
                                <input type="text" name='code' class="form-control" placeholder="Code"
                                    value="{{ request('code') }}">
                            </div>
                            <div class="col">
                                <input type="text" name='product_name' class="form-control" placeholder="Nom du produit"
                                    value="{{ request('product_name') }}">
                            </div>
                            <div class="col">
                                <select name="user" id="inputState" class="form-control">
                                    <option value="">Selectionnez un proprietaire</option>
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}" @if (request('user') == $user->id) selected="selected" @endif>{{ $user->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col">
                                <select name="status" id="inputState" class="form-control">
                                    <option value="">Selectionnez un etat</option>
                                    @foreach ($status as $s)
                                        <option value="{{ $s->id }}" @if (request('status') == $s->id) selected="selected" @endif>{{ $s->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col input-group date">
                                <input type="text" class="form-control" name="sdate" value="{{ request('sdate') }}"
                                    placeholder="Date debut" autocomplete="off"><span class="input-group-addon"><i
                                        class="glyphicon glyphicon-th"></i></span>
                            </div>
                            <div class="col">
                                <button type="submit" class="btn btn-primary mb-2" name="filter">Chercher</button>
                                <button type="reset" class="btn btn-secondary mb-2"
                                    onclick="window.location.href='{{ route('admin.bookings.index') }}'">Annuler</button>
                            </div>
                        </div>
                    </form>
                </nav>

                <table class="table table-sm table-responsive-sm table-striped">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Client</th>
                            <th>Produits</th>
                            <th>Periode</th>
                            <th>Etat</th>
                            <th>Statut du Paiment</th>
                            <th>Montant</th>
                            <th>Caution</th>
                            <th>Date de demande</th>
                        </tr>
                    </thead>
                    <tbody>

                        @forelse ($bookings as $booking)

                            <tr>

                                <td>
                                    <a href="{{ route('admin.bookings.details', $booking) }}">#{{ $booking->code }}</a>
                                </td>

                                <td>
                                    #{{ $booking->user_id }} {{ $booking->user->name }}
                                </td>
                                <td>
                                    <ul class="p-0">
                                        @foreach ($booking->products as $product)
                                            <li>{{ Str::limit($product->name, 20) }}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td c>
                                    {{ $booking->start_date->format('Y-m-d') }}
                                    -
                                    {{ $booking->end_date->translatedFormat('Y-m-d') }}
                                    <br>
                                    {{$booking->start_date->diffForHumans()}} - Pour {{ $booking->start_date->diffInDays($booking->end_date->addHours(24)) }} Jours
                                </td>
                                <td>
                                    <span class="badge {{ $booking->badge_class }}">
                                        {{ $booking->currentStatus->name }}
                                    </span>
                                </td>

                                <td>
                                    @if ($booking->payment_recieved == 1)
                                        <span class="badge bg-success text-white">
                                            PAIEMENT RECU
                                        </span>
                                    @endif

                                    @if ($booking->deposit_refunded == 1)
                                        <span class="badge bg-primary text-white">
                                            CAUTION RENDU
                                        </span>
                                    @endif
                                </td>
                                <td>
                                    <strong>{{ $booking->price }} DT</strong>
                                </td>

                                <td>
                                    {{ $booking->deposit }} DT
                                </td>
                                <td class="small">
                                    {{ $booking->created_at }}
                                </td>

                            </tr>
                        @empty
                            <tr>
                                <td colspan="9">
                                    <center><i>Pas de données</i></center>
                                </td>
                            </tr>

                        @endforelse

                    </tbody>
                </table>

                {{ $bookings->withQueryString()->links() }}
            </div>
        </div>

    </div>

@endsection

@push('scripts')
    <link rel="stylesheet" href="{{ asset('admin_assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="{{ asset('admin_assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('admin_assets/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min.js') }}"></script>

    <script src="{{ asset('admin_assets/js/popper.min.js') }}"></script>
@endpush
@section('javascript')

    <script>
        jQuery(document).ready(function() {

            $('.input-group.date').datepicker({
                format: "dd/mm/yyyy",
                todayBtn: true,
                language: "fr",
                todayHighlight: true
            });

            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
