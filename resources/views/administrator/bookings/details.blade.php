@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')


    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="card border-success">
                        <div class="d-flex justify-content-between card-header">
                            <div>
                                <h4 class="card-title mb-0">Details commande <span class="text-uppercase">#{{ $booking->code }}</span> <span class="badge booking-type">{{$booking->type}}</span> {!!  $booking_badge->generateStatusBadge($booking)  !!} </h4>
                                @if ($booking->payment_recieved == 1)
                                    <span class="badge bg-success text-white">
                                        PAIEMENT REÇU
                                    </span>
                                @else
                                    <span class="badge bg-danger text-white">
                                        PAIEMENT NON REÇU
                                    </span>
                                @endif

                                @if ($booking->deposit_refunded == 1)
                                    <span class="badge bg-success text-white">
                                        CAUTION RENDU
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="row align-items-stretch align-content-center">

                                <div class="col-sm-3 py-2 small">Date de la demande </div>
                                <div class="col-sm-9 py-2 mb-0  small">
                                    {{ Str::ucfirst($booking->created_at->translatedFormat('l d/m/Y H:i')) }}
                                </div>
                                <div class="col-12">
                                    <hr class="my-1">
                                </div>


                                <div class="col-sm-3 py-2">Client</div>
                                <div class="col-sm-9 py-2 mb-0 ">

                                    <a href="{{ route('admin.customers.details', $booking->user->id) }}"><b> #{{$booking->user->id}} - {{ $booking->user->name }}</b></a>
                                    <br>
                                   <div class="text-weight-bold">
                                       <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope-at-fill  c-icon pl-1" viewBox="0 0 16 16">
                                           <path d="M2 2A2 2 0 0 0 .05 3.555L8 8.414l7.95-4.859A2 2 0 0 0 14 2H2Zm-2 9.8V4.698l5.803 3.546L0 11.801Zm6.761-2.97-6.57 4.026A2 2 0 0 0 2 14h6.256A4.493 4.493 0 0 1 8 12.5a4.49 4.49 0 0 1 1.606-3.446l-.367-.225L8 9.586l-1.239-.757ZM16 9.671V4.697l-5.803 3.546.338.208A4.482 4.482 0 0 1 12.5 8c1.414 0 2.675.652 3.5 1.671Z"/>
                                           <path d="M15.834 12.244c0 1.168-.577 2.025-1.587 2.025-.503 0-1.002-.228-1.12-.648h-.043c-.118.416-.543.643-1.015.643-.77 0-1.259-.542-1.259-1.434v-.529c0-.844.481-1.4 1.26-1.4.585 0 .87.333.953.63h.03v-.568h.905v2.19c0 .272.18.42.411.42.315 0 .639-.415.639-1.39v-.118c0-1.277-.95-2.326-2.484-2.326h-.04c-1.582 0-2.64 1.067-2.64 2.724v.157c0 1.867 1.237 2.654 2.57 2.654h.045c.507 0 .935-.07 1.18-.18v.731c-.219.1-.643.175-1.237.175h-.044C10.438 16 9 14.82 9 12.646v-.214C9 10.36 10.421 9 12.485 9h.035c2.12 0 3.314 1.43 3.314 3.034v.21Zm-4.04.21v.227c0 .586.227.8.581.8.31 0 .564-.17.564-.743v-.367c0-.516-.275-.708-.572-.708-.346 0-.573.245-.573.791Z"/>
                                       </svg>
                                       {{ $booking->user->email }}
                                       @if ($booking->user->email_verified_at != null)
                                           <svg class="c-icon pr-1 text-success">
                                               <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-check-circle"></use>
                                           </svg>
                                       @endif
                                       <span class="pl-2">
                                           <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telephone-fill c-icon pl-1" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M1.885.511a1.745 1.745 0 0 1 2.61.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
                                        </svg>
                                        {{ $booking->user->phone }}
                                       </span>
                                   </div>
                                </div>



                            </div>
                            <hr>
                            <h6 class=""><b>Adresse de Livraison :</b></h6>
                            <div class="alert alert-secondary text0" role="alert">
                                <strong>Contact: </strong>{{ @$booking->deliveryAddress->contact_person }}<br>
                                <strong>Telephone: </strong>{{ @$booking->deliveryAddress->contact_phone }}<br>
                                <strong>Adresse: </strong><br>
                                <b>{{ @$booking->deliveryAddress->label }}</b>
                                {{ @$booking->deliveryAddress->street }} <br>
                                {{ @$booking->deliveryAddress->zip_code }} - {{ @$booking->deliveryAddress->city }}
                            </div>
                            @if($booking->note)
                                <div class="col-sm-9 py-2 mb-0 "> Note : <br>{{ $booking->note }}</div>
                            @endif
                        </div>
                        <div class="card-footer">
                            <h5>Statut actuel de la commande : {!!  $booking_badge->generateStatusBadge($booking)  !!}</h5>
                            @if($booking->status_event_id === 1)
                                <h5 class="text-danger">Pour confirmer la commande vous devez saisir le paiement !</h5>
                            @endif

                            @if(!$archived)
                                <form action="{{ route('admin.bookings.change_status') }}" method="post">
                                    @csrf
                                    <div class="form-row">
                                        <div class="col">
                                            <select name="status_event_id" class="form-control" required>
                                                <option value=""> Selectionnez...</option>
                                                @foreach ($status_events as $status_event)
                                                    <option value="{{ $status_event->id }}" {{ $booking->currentStatus->id === $status_event->id ? 'selected' : ''  }}>{{ $status_event->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col">
                                            <input type="text" name="details" class="form-control" placeholder="Note..."/>
                                            <input type="hidden" name="booking_id" class="form-control"
                                                   value="{{ $booking->id }}"/>
                                        </div>
                                        <div class="col">
                                            <button class="btn btn btn-primary" type="submit"> Changer le status</button>
                                        </div>
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="card card-accent-success">
                        <div class="d-flex justify-content-between card-header">
                            <div>
                                <h4 class="card-title mb-0">Articles <span class="badge badge-primary">Nbr : {{ count($booking->products) }} </span></h4>
                            </div>
                        </div>
                        <div class="card-body pt-0">
                            <div class="list-group list-group-flush">
                                @foreach($booking->items as $item)
                                <div class="list-group-item d-flex flex-row">

                                    @php
                                        $image = $item->product->image;

                                        if ($image) {
                                            $image_path = asset('storage/imgs/'. $image->file_name) ;
                                        } else {
                                            $image_path = 'https://via.placeholder.com/40x40?text=No+image';
                                        }
                                    @endphp

                                        <div class="mr-3"><img src="{{ $image_path }}" width="50px" alt=""></div>

                                    <div>
                                        <h5 class="mb-1">{!!  '(' . $item->product->ref . ') <b>' . $item->product->name  !!}</b></h5>
                                        <div class="small">Brand : <b>{{$item->product->brand->name}}</b></div>
                                        <div class="small">Prix : <b> {{ $item->price }}DT - {{ $item->product->rent_price  }} DT/Jour</b></div>
                                        <div class="small">Propriétaire : <b>{{$item->product->user->name}}</b></div>
                                        <div class="small">Commission : <b>{{ number_format(round(($item->price * config('app.commission')), 1 ), 3, '.', ' ')  }}DT</b> </div>
                                        <div class="small">

                                        </div>
                                        <a class="small" href="{{ route('admin.products.show', $item->product) }}">Voir</a>
                                    </div>

                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="">
                                <div>Période de location</div>
                                <div class="d-flex flex-row justify-content-between">
                                    <b>Du : {{ $booking->start_date->format('Y-m-d') }}</b>
                                    <b>Au : {{ $booking->end_date->format('Y-m-d') }}</b>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex flex-row justify-content-between">
                                <div>Caution</div>
                                <div><b>{{ $booking->deposit }} DT</b></div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex flex-row justify-content-between">
                                <div>Période :</div>
                                <div><b>{{ $booking->period}} Jours</b></div>
                            </div>
                        </div>

                        <div class="card-footer bg-success text-white">
                            <h5 class="d-flex flex-row justify-content-between">
                                <div>Prix</div>
                                <div><b>{{ $booking->price }} DT</b></div>
                            </h5>
                            <div class="d-flex flex-row justify-content-between small">
                                <div>Bénifice</div>
                                <div><b>{{ number_format(round(($item->price * (1-config('app.commission'))), 3 ), 3, '.', ' ')  }}DT</b></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-7">
                    <div class="card card-accent-success">
                        <div class="d-flex justify-content-between card-header">
                            <div>
                                <h4 class="card-title mb-0">Detail paiement</h4>

                            </div>

                        </div>
                        <div class="card-body">
                            <div class="mb-1">
                                <span class="h5">Paiement: </span><b>{{ $booking->price }} DT</b><br>

                                @if ($booking->payment_recieved == 0)
                                    <form action="{{ route('admin.bookings.payment_recieved') }}" method="post" onSubmit="if(!confirm('Paiement  {{ $booking->price }} DT reçu et je confime la commande !')){return false;}">
                                        <br>
                                        @csrf
                                        <div class="form-row">
                                            <div class="col-12">
                                                <label for="payment_note" class="d-block">Note ( exp : par virement, par chéque n°22123...) :</label>
                                                <p class="text-danger small font-weight-bold">Une fois vous cliqué sur le bouton 'Paiement reçu', la commande passe a l'état confirmée et le client recevra un email de confirmation.</p>
                                            </div>
                                            <div class="col">

                                                <input type="text" name="payment_note" id="payment_note" class="form-control @if($errors->payment->has('payment_note')) is-invalid @endif"
                                                       placeholder="Commentaire ?"/>

                                                <input type="hidden" name="booking_id" class="form-control"
                                                       value="{{ $booking->id }}"/>
                                            </div>
                                            <div class="col-auto">
                                                <button class="btn btn btn-success" type="submit"> Paiement reçu</button>
                                            </div>
                                        </div>
                                    </form>
                                @else
                                    <p>{!! $booking->payment_note !!}</p>
                                    <div>
                                        <span class="h5">Caution: </span><br>
                                        @if ($booking->deposit_refunded == 0)
                                            <form action="{{ route('admin.bookings.deposit_refunded') }}" method="post">
                                                @csrf
                                                <div class="form-row mt-1">
                                                    <div class="col">
                                                        <input type="hidden" name="booking_id" class="form-control"
                                                               value="{{ $booking->id }}"/>
                                                        <button class="btn btn btn-warning text-white" type="submit">
                                                            Caution
                                                            remboursé
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        @else
                                            <p> Caution rendu !</p>
                                        @endif
                                    </div>
                                @endif
                            </div>
                            <br/>

                            <p>
                            </p>
                        </div>


                    </div>
                </div>
                <div class="col-md-5">
                    <div class="card card-accent-info">
                        <div class="d-flex justify-content-between card-header">
                            <div>
                                <h5 class="card-title mb-0">Historique des etats de la commande</h5>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-sm table-responsive-sm table-striped">
                                <thead>
                                <tr>
                                    <th>Etat</th>
                                    <th>Date</th>
                                    <th>Details</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($old_status as $status)

                                    <tr>
                                        <td><span class="badge bg-{{ $booking_badge->generateStatusBadgeClass($status->id) }}" data-toggle="todotooltip"
                                                  data-placement="top" title="">
                                                    {{ $status->name }}
                                                </span></td>
                                        <td>{{ $status->pivot->created_at->format('d/m/Y H:i') }}</td>
                                        <td> {{ $status->pivot->details }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
            </div>

@endsection

@section('javascript')


@endsection
