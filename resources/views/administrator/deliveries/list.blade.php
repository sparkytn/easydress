@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')



    <div class="container-fluid">

        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between mb-3">
                    <div>
                        <h4 class="card-title mb-0">List des expeditions Date:
                            {{ Carbon::parse($selected_date)->translatedFormat('l d/m/Y') }} </h4>

                    </div>

                </div>


                <ul class="nav nav-tabs" id="myTab" role="tablist">

                    @foreach ($dates as $date)
                        <li class="nav-item">
                            <a class="nav-link font-weight-bolder @if ($date['selected'] == true) active @endif"
                                href="{{ route('admin.delivery', ['date' => $date['value']]) }}" role="tab"
                                aria-controls="home" @if ($date['selected'] == true) aria-selected="true" @endif>{{ $date['label'] }}</a>
                        </li>
                    @endforeach

                </ul>

                <div class="d-flex mb-2 mt-2 float-right">
                    <div class="btn-toolbar d-none d-md-block float-right" role="toolbar" aria-label="Toolbar with buttons">

                        <a class="btn btn-warning text-decoration-none"
                            href="{{ route('admin.delivery.export_list', ['date' => $selected_date]) }}" target="_blank">
                            <svg class="c-icon">
                                <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-pdf"></use>
                            </svg>
                            Télecharger la liste
                        </a>
                        <a class="btn btn-info text-decoration-none"
                            href="{{ route('admin.delivery.export_invoices', ['date' => $selected_date]) }}" target="_blank">
                            <svg class="c-icon">
                                <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-truck"></use>
                            </svg>
                            Télecharger les fiches
                        </a>
                    </div>
                </div>
                <table class="table table-sm table-responsive-sm table-striped">
                    <thead>
                        <tr>
                            <th>Commande</th>
                            <th>Produits</th>
                            <th>Client</th>
                            <th>Adresse</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @forelse ($deliveries as $booking)

                            @php
                                
                                $products_list = '';
                                
                                foreach ($booking->products as $product) {
                                    $products_list .= ' <a href="' . 
                                                        route('admin.products.show', $product) . '">' . 
                                                        ProductService::generateRef($product) . ' - ' . 
                                                        Str::limit($product->name, 40) . '</a><br>';
                                }
                                
                            @endphp

                            <tr>
                                <td>
                                    {{ $booking->code }}
                                </td>
                                <td>
                                {!! $products_list !!}
                                </td>
                                <td>

                                </td>

                                <td>
                                    {{ $booking->user->name }}
                                </td>
                                <td>
                                    <strong>Contact: </strong>{{ @$booking->deliveryAddress->contact_person }}<br>
                                    <strong>Telephone:
                                    </strong>{{ @$booking->deliveryAddress->contact_phone }}<br>
                                    {{ @$booking->deliveryAddress->street }}<br>
                                    {{ @$booking->deliveryAddress->zip_code }} -
                                    {{ @$booking->deliveryAddress->city }}
                                </td>
                                <td>
                                    <a class="btn btn-info text-decoration-none"
                                        href="{{ route('admin.delivery.export_invoices', ['booking' => $booking->id, 'date' => $selected_date]) }}"
                                        target="_blank">
                                        <svg class="c-icon">
                                            <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-truck"></use>
                                        </svg>
                                        Télecharger la fiche
                                    </a>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">
                                    <center><i>Pas de livraison pour cette date</i></center>
                                </td>
                            </tr>

                        @endforelse

                    </tbody>
                </table>


            </div>
        </div>

    </div>

@endsection

@section('javascript')


@endsection
