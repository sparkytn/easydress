<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->

    <style>
        html {
            font-family: sans-serif;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            font-size: 30%;
            box-sizing: border-box;
        }

        body {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 12px;
            line-height: 1.428571429;
            color: #333;
            background-color: #fff;
        }

        .container {
            margin-right: auto;
            margin-left: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        div {
            display: block;
        }

        .row {
            margin-left: -15px;
            margin-right: -15px;
        }

        .col-xs-12 {
            width: 100%;
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
            float: left;
        }

        h1,
        .h1,
        h2,
        .h2,
        h3,
        .h3 {
            margin-bottom: 10px;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-weight: 500;
            line-height: 1.1;
            color: inherit;

        }

        h2,
        .h2 {
            font-size: 24px;
        }

        h3,
        .h3 {
            font-size: 18px;
        }

        .pull-right {
            float: right !important;
        }

        hr {
            margin-top: 20px;
            margin-bottom: 20px;
            border: 0;
            border-top: 2px solid;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            height: 0;
        }

        .panel {
            margin-bottom: 20px;
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
            box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
        }

        .table
         {
            width: 100%;
            margin-bottom: 20px;
            border-collapse: collapse;
            border-spacing: 0;
            max-width: 100%;
            background-color: transparent;
            padding: 5px;
            line-height: 1.428571429;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        .table>thead>tr>th,
        .table>tbody>tr>th,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>tbody>tr>td,
        .table>tfoot>tr>td {
            padding: 5px;
            line-height: 1.428571429;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        thead {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
        }

        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }

        tbody {
            display: table-row-group;
            vertical-align: middle;
            border-color: inherit;
        }

        tr {
            display: table-row;
            vertical-align: inherit;
            border-color: inherit;
        }

        .panel-body {
            padding: 15px;
        }


        .table>tbody>tr>.no-line {
            border-top: none;
        }

        .table>thead>tr>.no-line {
            border-bottom: none;
        }

        .table>tbody>tr>.thick-line {
            border-top: 2px solid;
        }

        .page_break{page-break-after: always; }

    </style>
</head>

<body>

    @foreach($bookings as $booking)

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <table style="width: 100%">

                    <tr>
                        <td class=" " style="width:50%;font-size:200%"><strong>EasyDress.com</strong></td>
                        <td class="text-right" style="font-size:150%"><strong>Commande </strong>#{{ $booking->code }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table style="width: 100%; margin-bottom:40px">
                            <tr style="vertical-align:text-top">
                                <td style="width: 50%">
                                <strong>Client:</strong><br>
                                {{ $booking->user->name}}<br>
                                {{ $booking->user->email}}<br>
                                {{ $booking->user->phone}}

                                </td>
                                <td class="text-right"><strong>Adresse de livraison:</strong><br>
                                    Contact: {{ @$booking->deliveryAddress->contact_person }}<br>
                                    Telephone: {{ @$booking->deliveryAddress->contact_phone }}<br>
                                    {{ @$booking->deliveryAddress->street }}<br>
                                    {{ @$booking->deliveryAddress->zip_code }} - {{ @$booking->deliveryAddress->city }}
                                </td>
                            </tr>
                            <tr>
                                <td>Date commande: {{ $booking->created_at->translatedFormat('d/m/Y H:i') }}</td>
                                <td></td>
                            </tr>
                            </table>
                            
                        <div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                    <tr style="background-color: #eee">
                                        <th><strong>Ref.</strong></th>
                                        <th ><strong>Nom produit</strong></th>
                                        <th ><strong>Periode location</strong></th>
                                        <th class="text-right"><strong>Prix location</strong></th>
                                    </tr>
                                    
                                </thead>
                                <tbody>
                                    @foreach($booking->items as $item)
                                    
                                    <tr style=" vertical-align:text-top">
                                        <td style="height:{{ 350/count($booking->items) }}px;">{{ ProductService::generateRef($item->product) }}</td>
                                        <td >{{ $item->product->name }}</td>
                                        <td >Du: {{$booking->start_date->translatedFormat('d/m/Y') }} au: {{$booking->end_date->translatedFormat('d/m/Y') }}</td>
                                        <td class="text-right">{{ $item->price }} DT</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="4" style="border-top:none"><i>Caution: {{ $booking->deposit }} DT</i></td>
                                    </tr>
                                    <tr>
                                        <td class="thick-line text-center"></td>
                                        <td class="thick-line "></td>
                                        <td class="thick-line text-right"><strong>TOTAL</strong></td>
                                        <td class="thick-line text-right">{{ $booking->price }} DT</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                        <div style="margin-top:40px">
Merci de bien rendre l'article dans le même état dont vous l'avez reçu.<br>
Veuillez aussi à rendre l'article à la date prévue: <strong>{{$booking->end_date->translatedFormat('l d/m/Y') }} </strong><br>
Dés la réception et la vérification de l'article par notre équipe nous vous rembourserons le caution (Montant caution: {{ $booking->deposit }} DT)
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@if(!$loop->last)
    <div class="page_break"></div>
@endif
@endforeach

</body>

</html>
