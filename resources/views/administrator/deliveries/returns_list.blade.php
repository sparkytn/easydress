@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')



    <div class="container-fluid">

        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between mb-3">
                    <div>
                        <h4 class="card-title mb-0">List des articles en retour Date:
                            {{ Carbon::parse($selected_date)->translatedFormat('l d/m/Y') }} </h4>

                    </div>

                </div>


                <ul class="nav nav-tabs" id="myTab" role="tablist">

                    @foreach($dates as $date)
                    <li class="nav-item">
                        <a class="nav-link font-weight-bolder @if($date['selected']==true) active @endif"   href="{{ route('admin.delivery.returns',['date'=>$date['value']])}}" role="tab"
                            aria-controls="home"  @if($date['selected']==true) aria-selected="true" @endif>{{ $date['label'] }}</a>
                    </li>
                    @endforeach
                    
                </ul>
                
                <div class="d-flex mb-2 mt-2 float-right">
                <div class="btn-toolbar d-none d-md-block float-right" role="toolbar" aria-label="Toolbar with buttons">

                    <a class="btn btn-warning text-decoration-none" href="{{ route('admin.delivery.returns.export',['date'=>$selected_date]) }}"
                        target="_blank">
                        <svg class="c-icon">
                            <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-pdf"></use>
                        </svg>
                        Télecharger la liste
                    </a>
                                   </div>
                </div>
                <table class="table table-sm table-responsive-sm table-striped">
                    <thead>
                        <tr>
                            <th>Commande</th>
                            <th>Produits</th>
                            <th>Client</th>
                            <th>Date reception</th>
                        </tr>
                    </thead>
                    <tbody>

                        @forelse ($returns as $booking)

                           

                            <tr>
                                <td>
                                    {{ $booking->code }}
                                </td>
                                <td>
                                    <ul>
                                    @foreach($booking->items as $item)
                                    <li><a href="{{ route('admin.products.show', $item->product) }}">{{ ProductService::generateRef($item->product) }}
                                        - {{ Str::limit($item->product->name, 40) }}</a>
                                    </li>
                                    @endforeach
                                    </ul>
                                </td>
                                

                                <td>
                                    {{ $booking->user->name }}<br>
                                    TEL: {{ $booking->user->phone }}
                                </td>
                                <td>
                                    {{ @$booking->start_date->translatedFormat('l d/m') }}
                                </td>
                                
                            </tr>
                        @empty
                            <tr>
                                <td colspan="5">
                                    <center><i>Pas de retour pour cette date</i></center>
                                </td>
                            </tr>

                        @endforelse

                    </tbody>
                </table>


            </div>
        </div>

    </div>

@endsection

@section('javascript')


@endsection
