<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->

    <style>
        html {
            font-family: sans-serif;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
            font-size: 30%;
            box-sizing: border-box;
        }

        body {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-size: 10px;
            line-height: 1.428571429;
            color: #333;
            background-color: #fff;
        }

        .container {
            margin-right: auto;
            margin-left: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        div {
            display: block;
        }

        .row {
            margin-left: -15px;
            margin-right: -15px;
        }

        .col-xs-12 {
            width: 100%;
            position: relative;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
            float: left;
        }

        h1,
        .h1,
        h2,
        .h2,
        h3,
        .h3 {
            margin-bottom: 10px;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-weight: 500;
            line-height: 1.1;
            color: inherit;

        }

        h2,
        .h2 {
            font-size: 24px;
        }

        h3,
        .h3 {
            font-size: 18px;
        }

        .pull-right {
            float: right !important;
        }

        hr {
            margin-top: 20px;
            margin-bottom: 20px;
            border: 0;
            border-top: 2px solid;
            -moz-box-sizing: content-box;
            box-sizing: content-box;
            height: 0;
        }

        .panel {
            margin-bottom: 20px;
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
            box-shadow: 0 1px 1px rgba(0, 0, 0, .05);
        }

        .table {
            width: 100%;
            margin-bottom: 20px;
            border-collapse: collapse;
            border-spacing: 0;
            max-width: 100%;
            background-color: transparent;
            padding: 5px;
            line-height: 1.428571429;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        .table>thead>tr>th,
        .table>tbody>tr>th,
        .table>tfoot>tr>th,
        .table>thead>tr>td,
        .table>tbody>tr>td,
        .table>tfoot>tr>td {
            padding: 5px;
            line-height: 1.428571429;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        thead {
            display: table-header-group;
            vertical-align: middle;
            border-color: inherit;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        tbody {
            display: table-row-group;
            vertical-align: middle;
            border-color: inherit;
        }

        tr {
            display: table-row;
            vertical-align: inherit;
            border-color: inherit;
        }

        .panel-body {
            padding: 15px;
        }


        .table>tbody>tr>.no-line {
            border-top: none;
        }

        .table>thead>tr>.no-line {
            border-bottom: none;
        }

        .table>tbody>tr>.thick-line {
            border-top: 2px solid;
        }

    </style>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <table style="width: 100%">

                    <tr>
                        <td class=" " style="width:50%;font-size:200%"><strong>Liste des retours</strong></td>
                        <td class="text-right" style="font-size:150%"><strong>Date:</strong>
                            {{ Carbon::parse($selected_date)->translatedFormat('l d/m/Y') }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                    <tr>
                                        <td><strong>Commande</strong></td>
                                        <td class="text-center"><strong>Produits</strong></td>
                                        <td class="text-center"><strong>Nom client</strong></td>
                                        <td class="text-right"><strong>Date reception</strong></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($returns as $booking)
                                        <tr>
                                            <td>{{ $booking->code }}</td>
                                            <td class="text-left">
                                                <ul>
                                                    @foreach ($booking->items as $item)
                                                        <li>{{ ProductService::generateRef($item->product) }} -
                                                            {{ $item->product->name }}</li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                            <td class="text-center"> {{ $booking->user->name }}<br>
                                                TEL: {{ $booking->user->phone }}</td>
                                            <td class="text-right">
                                                {{ $booking->start_date->translatedFormat('l d/m') }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td class="thick-line text-center"><strong>TOTAL</strong></td>
                                        <td class="thick-line ">{{ $returns->count() }}</td>
                                        <td class="thick-line"></td>
                                        <td class="thick-line"></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/php">
        if (isset($pdf)) {
                    $text = "Page {PAGE_NUM} / {PAGE_COUNT}";
                    $size = 7;
                    $font = $fontMetrics->getFont("Verdana");
                    $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
                    $x = ($pdf->get_width() - $width) -30;
                    $y = $pdf->get_height() - 35;
                    $pdf->page_text($x, $y, $text, $font, $size);
                }
            </script>
</body>

</html>
