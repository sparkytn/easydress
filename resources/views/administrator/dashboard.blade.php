@extends('administrator.layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">
            <div class="row">

                <div class="col-6 col-lg-3">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <div class="bg-gradient-primary p-3 mfe-3">
                                <svg class="c-icon c-icon-xl">
                                    <use
                                        xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-cart') }}">
                                    </use>
                                </svg>
                            </div>
                            <div>
                                <div class="text-value text-primary">{{ $stats['new_bookings'] }}</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Commandes a confirmer</div>
                            </div>
                        </div>
                        <div class="card-footer px-3 py-2"><a
                                class="btn-block text-muted d-flex justify-content-between align-items-center"
                                href="{{ route('admin.bookings.index', ['status' => '1']) }}"><span
                                    class="small font-weight-bold">Voir</span>
                                <svg class="c-icon">
                                    <use
                                        xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-chevron-right') }}">
                                    </use>
                                </svg></a></div>
                    </div>
                </div>

                <div class="col-6 col-lg-3">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <div class="bg-gradient-info p-3 mfe-3">
                                <svg class="c-icon c-icon-xl">
                                    <use
                                        xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-view-quilt') }}">
                                    </use>
                                </svg>
                            </div>
                            <div>
                                <div class="text-value text-info">{{ $stats['tobe_returned'] }}</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Produits en retour</div>
                            </div>
                        </div>
                        <div class="card-footer px-3 py-2"><a
                                class="btn-block text-muted d-flex justify-content-between align-items-center"
                                href="{{ route('admin.bookings.index', ['status' => '4']) }}"><span
                                    class="small font-weight-bold">Voir</span>
                                <svg class="c-icon">
                                    <use
                                        xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-chevron-right') }}">
                                    </use>
                                </svg></a></div>
                    </div>
                </div>

                <div class="col-6 col-lg-3">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <div class="bg-gradient-success p-3 mfe-3">
                                <svg class="c-icon c-icon-xl">
                                    <use
                                        xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-view-quilt') }}">
                                    </use>
                                </svg>
                            </div>
                            <div>
                                <div class="text-value text-success">{{ $stats['tobe_delivered'] }}</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Produits à expedier</div>
                            </div>
                        </div>
                        <div class="card-footer px-3 py-2"><a
                                class="btn-block text-muted d-flex justify-content-between align-items-center"
                                href="{{ route('admin.delivery') }}"><span
                                    class="small font-weight-bold">Voir</span>
                                <svg class="c-icon">
                                    <use
                                        xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-chevron-right') }}">
                                    </use>
                                </svg></a></div>
                    </div>
                </div>

                <div class="col-6 col-lg-3">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <div class="bg-gradient-warning p-3 mfe-3">
                                <svg class="c-icon c-icon-xl">
                                    <use
                                        xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-share-boxed') }}">
                                    </use>
                                </svg>
                            </div>
                            <div>
                                <div class="text-value text-warning">{{ $stats['tobe_refund'] }}</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Caution a rendre</div>
                            </div>
                        </div>
                        <div class="card-footer px-3 py-2"><a
                                class="btn-block text-muted d-flex justify-content-between align-items-center"
                                href="{{ route('admin.bookings.index') }}"><span
                                    class="small font-weight-bold">Voir</span>
                                <svg class="c-icon">
                                    <use
                                        xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-chevron-right') }}">
                                    </use>
                                </svg></a></div>
                    </div>
                </div>

                <div class="col-6 col-lg-3">
                    <div class="card">
                        <div class="card-body p-3 d-flex align-items-center">
                            <div class="bg-gradient-danger p-3 mfe-3">
                                <svg class="c-icon c-icon-xl">
                                    <use
                                        xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-money') }}">
                                    </use>
                                </svg>
                            </div>
                            <div>
                                <div class="text-value text-danger">{{ $stats['commissions'] }}</div>
                                <div class="text-muted text-uppercase font-weight-bold small">Commission a payer</div>
                            </div>
                        </div>
                        <div class="card-footer px-3 py-2"><a
                                class="btn-block text-muted d-flex justify-content-between align-items-center"
                                href="{{ route('admin.commissions.index') }}"><span
                                    class="small font-weight-bold">Voir</span>
                                <svg class="c-icon">
                                    <use
                                        xlink:href="{{ asset('admin_assets/assets/icons/coreui/free-symbol-defs.svg#cui-chevron-right') }}">
                                    </use>
                                </svg></a></div>
                    </div>
                </div>

            </div>
            <div class="card">

                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5>Dernieres commandes</h5>
                            <table class="table table-sm table-hover table-outline mb-0">
                                <thead class="thead-light">
                                    <tr>
                                        <th class="text-center">Code</th>
                                        <th>Etat</th>
                                        <th>Client</th>
                                        <th>Produits</th>
                                        <th>Montant</th>
                                        <th>Periode</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($last_bookings as $booking)
                                        <tr>
                                            <td class="">
                                                <a href="{{ route('admin.bookings.details', $booking) }}"><b>#{{ $booking->code }}</b></a>
                                                <br>
                                                <small>{{ $booking->created_at }}</small>
                                            </td>
                                            <td class="">
                                                {!! $booking_badge->generateStatusBadge($booking) !!}
                                            </td>
                                            <td> {{ $booking->user->name }}</td>
                                            <td>
                                                @foreach($booking->items as $item)
                                                - {{ Str::limit($item->product->name, 15) }}
                                                <br>
                                                @endforeach
                                            </td>
                                            <td>{{ $booking->price }}DT</td>
                                            <td> <span class="small">
                                                    {{ $booking->start_date->format('Y-m-d') }}
                                                    -
                                                    {{ $booking->end_date->translatedFormat('Y-m-d') }}
                                                    <br>
                                                    {{$booking->start_date->diffForHumans()}} - Pour {{ $booking->start_date->diffInDays($booking->end_date->addHours(24)) }} Jours
                                                    </span>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5">
                                                <center><i>Pas de données</i></center>
                                            </td>
                                        </tr>

                                    @endforelse
                                </tbody>
                            </table>

                            <h5 class="mt-4">Dernieres produits ajoutés</h5>
                            <table class="table table-sm table-hover table-outline mb-0">
                                <thead class="thead-light">
                                    <tr>
                                        <th class="text-center">Nom</th>
                                        <th>Proprio.</th>
                                        <th>Date</th>
                                        <th>Validé?</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($last_submited as $submited)
                                        <tr>
                                            <td><a href="{{ route('admin.submitedproducts.show', $submited) }}">
                                                    {{ Str::limit($submited->name, 15) }}
                                                </a>
                                            </td>
                                            <td>{{ $submited->user->name }}</td>
                                            <td>{{ $submited->created_at->translatedFormat('d/m/Y H:i') }}
                                            </td>
                                            <td>
                                                @if ($submited->is_accepted)
                                                    <span class="badge badge-success">Validé</span>
                                                @else
                                                    <span class="badge badge-secondary">Pas encore validé</span>
                                                @endif
                                                @if ($submited->migrated)
                                                    <br><span class="badge badge-info">MIGRE</span>
                                                @endif
                                            </td>

                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5">
                                                <center><i>Pas de données</i></center>
                                            </td>
                                        </tr>

                                    @endforelse
                                </tbody>
                            </table>
                        </div>

                        <div class="col">
                            <h5>Expeditions et Retour</h5>
                            <div id="calendar" class="border-light p-2"></div>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection

@section('javascript')

@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('admin_assets/js/fullcalendar/main.min.js') }}"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');
            var calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: 'dayGridWeek',
                locale: 'fr',
                eventSources: [{
                        url: "{{ route('admin.dashboard.calendar_fill') }}",
                        color: 'blue',
                        display : 'html'
                    },
                    {{--{--}}
                    {{--    url: "{{ route('admin.dashboard.calendar_fill2') }}",--}}
                    {{--    color: 'orange'--}}
                    {{--}--}}

                ],
                eventClick: function(info) {
                    var eventObj = info.event;

                    if (eventObj.url) {

                        window.open(eventObj.url);

                        info.jsEvent.preventDefault(); // prevents browser from following link in current tab.
                    } else {
                        alert('Clicked ' + eventObj.title);

                    }
                },
                eventContent: function(arg, createElement) {
                    let innerText = [];
                    innerText.push(createElement('b', {}, arg.event.extendedProps.code));
                    innerText.push(createElement('div', {}, arg.event.title));
                    innerText.push(createElement('div', {'class':'small'}, arg.event.extendedProps.price));
                    innerText.push(createElement('small', {'class':'font-weight-bold'}, arg.event.extendedProps.status));

                    return innerText
                }
            });
            calendar.render();
        });
    </script>



@endpush

@push('styles')
    <link rel="stylesheet" href="{{ asset('admin_assets/js/fullcalendar/main.min.css') }}" />
@endpush
