<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('admin_assets/css/brand.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('admin_assets/css/flag.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('admin_assets/css/free.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('admin_assets/css/style.css') }}" />

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body class="c-app flex-row align-items-center">

        @yield('content')

        <!-- Scripts -->
        <script src="{{ asset('admin_assets/js/coreui.bundle.min.js') }}" defer></script>
        <script src="{{ asset('admin_assets/js/coreui-utils.js') }}" defer></script>
        <script src="{{ asset('admin_assets/js/app.js') }}" defer></script>
    </body>
</html>
