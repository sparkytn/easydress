<header class="bg-white dark:bg-gray-800 py-4 shadow-md z-10">
    <div class="px-6 container dark:text-purple-300 flex h-full items-center justify-between mx-auto text-purple-600">
        <!-- Mobile hamburger -->
        <button class="md:hidden -ml-1 focus:outline-none focus:shadow-outline-purple mr-5 p-1 rounded-md" @click="toggleSideMenu" aria-label="Menu">
            <svg aria-hidden="true" class="h-6 w-6" fill="currentColor" viewBox="0 0 20 20">
                <path d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd" fill-rule="evenodd"></path>
            </svg>
        </button>
        <!-- Search input -->
        <div class="flex flex-1 justify-center lg:mr-32">
            <div class="w-full focus-within:text-purple-500 max-w-xl mr-6 relative">
                <div class="flex items-center absolute inset-y-0 pl-2">
                    <svg aria-hidden="true" class="h-4 w-4" fill="currentColor" viewBox="0 0 20 20">
                        <path d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd" fill-rule="evenodd"></path>
                    </svg>
                </div>
                <input aria-label="Search" class="w-full text-sm rounded-md bg-gray-100 border-0 dark:bg-gray-700 dark:focus:placeholder-gray-600 dark:focus:shadow-outline-gray dark:placeholder-gray-500 dark:text-gray-200 focus:bg-white focus:border-purple-300 focus:outline-none focus:placeholder-gray-500 focus:shadow-outline-purple form-input pl-8 placeholder-gray-600 pr-2 text-gray-700" placeholder="Search for projects" type="text" />
            </div>
        </div>
        <ul class="flex items-center flex-shrink-0 space-x-6">
            <!-- Theme toggler -->
            <li class="flex">
                <button class="rounded-md focus:outline-none focus:shadow-outline-purple" @click="toggleTheme" aria-label="Toggle color mode">
                    <template x-if="!dark">
                        <svg aria-hidden="true" class="h-5 w-5" fill="currentColor" viewBox="0 0 20 20">
                            <path d="M17.293 13.293A8 8 0 016.707 2.707a8.001 8.001 0 1010.586 10.586z"></path>
                        </svg>
                    </template>
                    <template x-if="dark">
                        <svg aria-hidden="true" class="h-5 w-5" fill="currentColor" viewBox="0 0 20 20">
                            <path d="M10 2a1 1 0 011 1v1a1 1 0 11-2 0V3a1 1 0 011-1zm4 8a4 4 0 11-8 0 4 4 0 018 0zm-.464 4.95l.707.707a1 1 0 001.414-1.414l-.707-.707a1 1 0 00-1.414 1.414zm2.12-10.607a1 1 0 010 1.414l-.706.707a1 1 0 11-1.414-1.414l.707-.707a1 1 0 011.414 0zM17 11a1 1 0 100-2h-1a1 1 0 100 2h1zm-7 4a1 1 0 011 1v1a1 1 0 11-2 0v-1a1 1 0 011-1zM5.05 6.464A1 1 0 106.465 5.05l-.708-.707a1 1 0 00-1.414 1.414l.707.707zm1.414 8.486l-.707.707a1 1 0 01-1.414-1.414l.707-.707a1 1 0 011.414 1.414zM4 11a1 1 0 100-2H3a1 1 0 000 2h1z" clip-rule="evenodd" fill-rule="evenodd"></path>
                        </svg>
                    </template>
                </button>
            </li>
            <!-- Notifications menu -->
            <li class="relative">
                <button class="relative align-middle focus:outline-none focus:shadow-outline-purple rounded-md" @click="toggleNotificationsMenu" aria-haspopup="true" @keydown.escape="closeNotificationsMenu" aria-label="Notifications">
                    <svg aria-hidden="true" class="h-5 w-5" fill="currentColor" viewBox="0 0 20 20">
                        <path d="M10 2a6 6 0 00-6 6v3.586l-.707.707A1 1 0 004 14h12a1 1 0 00.707-1.707L16 11.586V8a6 6 0 00-6-6zM10 18a3 3 0 01-3-3h6a3 3 0 01-3 3z"></path>
                    </svg>
                    <!-- Notification badge -->
                    <span class="rounded-full -translate-y-1 absolute bg-red-600 border-2 border-white dark:border-gray-800 h-3 inline-block right-0 top-0 transform translate-x-1 w-3" aria-hidden="true"></span>
                </button>
                <template x-if="isNotificationsMenuOpen">
                    <ul class="bg-white shadow-md absolute border border-gray-100 dark:bg-gray-700 dark:border-gray-700 dark:text-gray-300 mt-2 p-2 right-0 rounded-md space-y-2 text-gray-600 w-56" aria-label="submenu" x-transition:leave="transition ease-in duration-150" x-transition:leave-end="opacity-0" x-transition:leave-start="opacity-100" @click.away="closeNotificationsMenu" @keydown.escape="closeNotificationsMenu">
                        <li class="flex">
                            <a class="w-full text-sm duration-150 items-center transition-colors dark:hover:text-gray-200 font-semibold hover:text-gray-800 inline-flex dark:hover:bg-gray-800 hover:bg-gray-100 px-2 py-1 rounded-md justify-between" href="#">
                                <span>Messages</span>
                                <span class="rounded-full bg-red-100 dark:bg-red-600 dark:text-red-100 font-bold inline-flex items-center justify-center leading-none px-2 py-1 text-red-600 text-xs">13</span>
                            </a>
                        </li>
                        <li class="flex">
                            <a class="w-full text-sm duration-150 items-center transition-colors dark:hover:text-gray-200 font-semibold hover:text-gray-800 inline-flex dark:hover:bg-gray-800 hover:bg-gray-100 px-2 py-1 rounded-md justify-between" href="#">
                                <span>Sales</span>
                                <span class="rounded-full bg-red-100 dark:bg-red-600 dark:text-red-100 font-bold inline-flex items-center justify-center leading-none px-2 py-1 text-red-600 text-xs">2</span>
                            </a>
                        </li>
                        <li class="flex">
                            <a class="w-full text-sm duration-150 items-center transition-colors dark:hover:text-gray-200 font-semibold hover:text-gray-800 inline-flex dark:hover:bg-gray-800 hover:bg-gray-100 px-2 py-1 rounded-md justify-between" href="#">
                                <span>Alerts</span>
                            </a>
                        </li>
                    </ul>
                </template>
            </li>
            <!-- Profile menu -->
            <li class="relative">
                <button class="rounded-full align-middle focus:outline-none focus:shadow-outline-purple" @click="toggleProfileMenu" aria-haspopup="true" @keydown.escape="closeProfileMenu" aria-label="Account">
                    <img alt="" aria-hidden="true" class="rounded-full h-8 object-cover w-8" src="https://images.unsplash.com/photo-1502378735452-bc7d86632805?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&s=aa3a807e1bbdfd4364d1f449eaa96d82">
                </button>
                <template x-if="isProfileMenuOpen">
                    <ul class="bg-white shadow-md absolute border border-gray-100 dark:bg-gray-700 dark:border-gray-700 dark:text-gray-300 mt-2 p-2 right-0 rounded-md space-y-2 text-gray-600 w-56" aria-label="submenu" x-transition:leave="transition ease-in duration-150" x-transition:leave-end="opacity-0" x-transition:leave-start="opacity-100" @click.away="closeProfileMenu" @keydown.escape="closeProfileMenu">
                        <li>
                            <div class="px-2 py-1 rounded-md">
                                <div class="font-medium text-base w-full">{{ Auth::guard('administrator')->user()->name }}</div>
                                <div class="text-sm  w-full opacity-50">{{ Auth::guard('administrator')->user()->email }}</div>
                            </div>
                        </li>
                        <li>
                            <hr class="opacity-25">
                        </li>
                        <li class="flex">
                            <a class="w-full text-sm duration-150 items-center transition-colors dark:hover:text-gray-200 font-semibold hover:text-gray-800 inline-flex dark:hover:bg-gray-800 hover:bg-gray-100 px-2 py-1 rounded-md" href="#">
                                <svg aria-hidden="true" class="h-4 w-4 mr-3" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                                    <path d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"></path>
                                </svg>
                                <span>Profile</span>
                            </a>
                        </li>
                        <li class="flex">
                            <a class="w-full text-sm duration-150 items-center transition-colors dark:hover:text-gray-200 font-semibold hover:text-gray-800 inline-flex dark:hover:bg-gray-800 hover:bg-gray-100 px-2 py-1 rounded-md" href="#">
                                <svg aria-hidden="true" class="h-4 w-4 mr-3" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                                    <path d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"></path>
                                    <path d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path>
                                </svg>
                                <span>Settings</span>
                            </a>
                        </li>
                        <li class="flex">
                            <form method="POST" class="w-full" action="{{ route('administrator.logout') }}">
                                @csrf
                                <a class="w-full text-sm duration-150 items-center transition-colors dark:hover:text-gray-200 font-semibold hover:text-gray-800 inline-flex dark:hover:bg-gray-800 hover:bg-gray-100 px-2 py-1 rounded-md"
                                   href="{{route('administrator.logout')}}"
                                   onclick="event.preventDefault(); this.closest('form').submit();">
                                    <svg aria-hidden="true" class="h-4 w-4 mr-3" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                                        <path d="M11 16l-4-4m0 0l4-4m-4 4h14m-5 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h7a3 3 0 013 3v1"></path>
                                    </svg>
                                    {{ __('Logout') }}
                                </a>
                            </form>
                        </li>
                    </ul>
                </template>
            </li>
        </ul>
    </div>
</header>
