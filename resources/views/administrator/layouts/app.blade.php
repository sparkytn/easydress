<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" :class="{ 'theme-dark': dark }" x-data="data()">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Admin panel - EasyDress</title>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('admin_assets/css/brand.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('admin_assets/css/flag.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('admin_assets/css/free.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('admin_assets/css/style.css') }}" />
        @stack('styles')

        @stack('scripts')

    </head>
    <body class="c-app">

        @include('administrator.partials.sidebar')

        <div class="c-wrapper c-fixed-components">

            @include('administrator.partials.nav')

            <div class="c-body">

                <main class="c-main">

                    @yield('content')

                </main>
                <footer class="c-footer">
                    <div><a href="#"></a> &copy; 2020 EasyDress.</div>
                    <div class="ml-auto">Powered by&nbsp;<a href="#">Sparky</a></div>
                </footer>
            </div>
        </div>

        <!-- Scripts -->
        <script src="{{ asset('admin_assets/js/coreui.bundle.min.js') }}" defer></script>
        <script src="{{ asset('admin_assets/js/coreui-utils.js') }}" defer></script>
        <script src="{{ asset('admin_assets/js/app.js') }}" defer></script>

        @yield('javascript')
    </body>
</html>
