@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')



    <div class="container-fluid">

        <div class="card">
            <div class="card-body">
                <div class="d-flex justify-content-between mb-3">
                    <div>
                        <h4 class="card-title mb-0">Clients </h4>

                    </div>

                </div>

                <hr>
                <nav class="navbar navbar-light bg-light">
                <form action="" method="GET">
                    @csrf
                    <div class="form-row">
                        <div class="col">
                            <input type="text" name='name' class="form-control" placeholder="Nom"
                                value="{{ request('name') }}">
                        </div>
                        <div class="col">
                            <input type="text" name='email' class="form-control" placeholder="Email"
                                value="{{ request('email') }}">
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-primary mb-2" name="filter">Chercher</button>
                            <button type="reset" class="btn btn-secondary mb-2" onclick="window.location.href='{{ route('admin.customers.index') }}'">Annuler</button>
                        </div>
                    </div>
                </form>
                </nav>

                <table class="table table-sm table-responsive-sm table-striped">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Commandes</th>
                            <th>Produits</th>
                            <th>Date insciption</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @forelse ($customers as $customer)
                        <tr>
                        <td><a href="{{ route('admin.customers.details',$customer) }}">{{ $customer->name }}</a></td>
                        <td>{{ $customer->email }}@if ($customer->email_verified_at != null)
                            <svg class="c-icon pl-1 text-green">
                                <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-check-circle"></use>
                            </svg>
                        @endif</td>
                        <td>{{ $customer->bookings_count }}</td>
                        <td>{{ $customer->products_count }}</td>
                        <td>{{ $customer->created_at->format('d/m/Y') }}</td>
                        <td></td>
                        </tr>

                        @empty
                            <tr>
                                <td colspan="6">
                                    <center><i>Pas de données</i></center>
                                </td>
                            </tr>

                        @endforelse

                    </tbody>
                </table>

                {{ $customers->links() }}
            </div>
        </div>

    </div>

@endsection

@push('scripts')

@endpush
@section('javascript')


@endsection
