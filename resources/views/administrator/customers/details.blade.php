@extends('administrator.layouts.app')

@section('css')

@endsection

@section('content')


    <div class="container-fluid">

        <div class="card">
            <div class="d-flex justify-content-between card-header">
                <div>
                    <h4 class="card-title mb-0">{{ $customer->name }}</h4>
                </div>
            </div>
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-3 border-bottom">Email</dt>
                    <dd class="col-sm-9 ">
                        {{ $customer->email }}
                        @if ($customer->email_verified_at != null)
                                        <svg class="c-icon pl-1 text-green">
                                            <use xlink:href="/admin_assets/icons/sprites/free.svg#cil-check-circle"></use>
                                        </svg>
                                    @endif
                    </dd>
                    <dt class="col-sm-3 border-bottom">Téléphone</dt>
                    <dd class="col-sm-9 ">{{ $customer->phone }}</dd>
                    <dt class="col-sm-3 border-bottom">Date inscription</dt>
                    <dd class="col-sm-9 ">{{ $customer->created_at->format('d/m/Y') }}</dd>
                </dl>
                <hr >
                <div class="nav-tabs-boxed">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home-1" role="tab"
                                aria-controls="home" aria-selected="true">Commandes</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile-1" role="tab"
                                aria-controls="profile" aria-selected="false">Produits ajoutés </a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#messages-1" role="tab"
                                aria-controls="messages" aria-selected="false">Adresses de livraison</a></li>
                    </ul>
                    <div class="tab-content ">
                        <div class="tab-pane active" id="home-1" role="tabpanel">
        
                            @include('administrator.partials.customers.bookings')  
        
                        </div>
                        <div class="tab-pane" id="profile-1" role="tabpanel">
                            @include('administrator.partials.customers.products')
                        </div>
                        <div class="tab-pane" id="messages-1" role="tabpanel">
                            @include('administrator.partials.customers.addresses')
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

@endsection

@section('javascript')


@endsection
