import './bootstrap';
import 'bootstrap-select/js/bootstrap-select';
import * as moment from 'moment'

require( 'air-datepicker/src/js/air-datepicker');
require( 'air-datepicker/src/js/i18n/datepicker.fr');
require( 'fullcalendar/dist/fullcalendar');
require( 'fullcalendar/dist/locale/fr');
require( 'xzoom');


const rentPeriods = [4,6,8];
window.momentDate = moment;

import SwiperCore, {EffectFade, Autoplay, Pagination} from 'swiper';


$(function () {
    SwiperCore.use([ EffectFade, Autoplay, Pagination ]);

    new SwiperCore('.hero-home .hero-slider', {
        speed: 1000,
        allowTouchMove: false,
        spaceBetween: 10,
        autoplay: {
            delay: 6000,
        },
        loop: true,
    });

    new SwiperCore('.home-products-carousel', {
        slidesPerView: 1,
        autoplay: {
            delay: 3000,
        },
        breakpoints: {
            520: {
                slidesPerView: 2,
            },
            // when window width is >= 480px
            768: {
                slidesPerView: 3,
            },
            // when window width is >= 640px
            1128: {
                slidesPerView: 4,
            }
        },

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },
    });

    //--------------------
    // Enable Select across the website
    $('select:not(.checkout-adr-city)').selectpicker({
        showSubtext: true,
    });

    //--------------------
    // Enable Date Picker
    var minDate = new Date();
    minDate.setDate(minDate.getDate() + 1);
    $('.date-input').datepicker({
        minDate: minDate,
        dateFormat: 'dd-mm-yyyy'
    });



    let $select_address = $('#checkout_delivery_address');

    if( $select_address.length > 0 ) {
        let $new_address_form = $('#new_checkout_delivery_address').html();

        if($select_address.val() !== 'add_new_adr' ){
            $('#new_checkout_delivery_address').html('');
        }else{
            $('#new_checkout_delivery_address').html($new_address_form);
        }

        $select_address.on('change', function () {
            $('.alert-address').remove();
            console.log($(this).val(),  $(this).val() === '')
            if($(this).val() === 'add_new_adr'){
                $('#new_checkout_delivery_address').html($new_address_form);
            }else{
                $('#new_checkout_delivery_address').html('');
            }
        })
    }

    if($('.xzoom').length){
        $(".xzoom, .xzoom-gallery").xzoom({tint: '#333', Xoffset: 15});
    }

    $(".filterSearchInput").on("keyup", function() {
        searchFilter(this);
    });
    $(".list-group-item.has-search").on("click", function(e) {
        $(this).find('input').val('');
        searchFilter($(this).find('input'));
    });

    function searchFilter(currentFilter) {
        let value = $(currentFilter).val().toLowerCase(),
            target = $(currentFilter).parent().parent().find('.list-group-item-action');
        console.log(value.length)
        if(value.length >=1)
            $(currentFilter).parent().addClass('dirty')
        else{
            $(currentFilter).parent().removeClass('dirty')
        }
        $(target).filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });

    }




    // let selectedDate = null;
    // let selectedPeriod = 3;
    // let $dayCell = $('#calendar .fc-day');
    // $('#calendar').fullCalendar({
    //     locale: 'fr',
    //     header: {
    //         left: 'prev',
    //         center: 'title',
    //         right: 'next'
    //     },
    //     events: [
    //         {
    //             title  : 'event1',
    //             start  : '2021-06-01',
    //             end    : '2021-06-04'
    //         },
    //         {
    //             title  : 'event2',
    //             start  : '2021-06-15',
    //             end    : '2021-06-20',
    //             allDay : true,
    //         },
    //     ],
    //     validRange: {
    //         start: '2021-05-25',
    //         end: '2021-11-25'
    //     },
    //     defaultDate: '2021-06-18',
    //     dayClick: function (date, jsEvent, view) {
    //         if ($('#modalRental .fc .fc-view .fc-bg tr td[data-date="' + date.format('YYYY-MM-DD') + '"]').hasClass('fc-inactive')) {
    //             return false;
    //         }
    //         $('#modalRental #form_startDate').val(date.format('YYYY-MM-DD'));
    //         //loadModalRental();
    //         selectedDate = date
    //         displayRentingDates(selectedDate, selectedPeriod);
    //         // $('#calendar').fullCalendar('next');
    //     },
    //     viewRender(view, element) {
    //         console.log(view,element);
    //         if(selectedDate !== null){
    //             displayRentingDates(selectedDate, selectedPeriod);
    //         }
    //     }
    // });
    //
    // var displayRentingDates = function (selectedDate,period = 3) {
    //     var startDate = moment(selectedDate, "YYYY-MM-DD");
    //     var daysNumber = period;
    //
    //     $('.fc .fc-view td').removeClass('bg-success bg-danger border-danger')
    //
    //     while (parseInt(daysNumber) > 0) {
    //         $('.fc .fc-view .fc-bg tr td[data-date="' + startDate.format('YYYY-MM-DD') + '"]').addClass('bg-success');
    //         startDate.add(1, 'days');
    //         daysNumber--;
    //     }
    //
    //     var packageReceptionDate = moment(selectedDate,'YYYY-MM-DD');
    //     $('.fc .fc-view .fc-bg tr td[data-date="' + packageReceptionDate.format('YYYY-MM-DD') + '"]').addClass('bg-success');
    //     $('.fc .fc-view .fc-bg tr td[data-date="' + packageReceptionDate.format('YYYY-MM-DD') + '"]').addClass('border-danger start ');
    //
    //     var packageResendDate = moment(selectedDate).add(2, 'days');
    //     $('.fc .fc-view .fc-bg tr td[data-date="' + packageResendDate.format('YYYY-MM-DD') + '"]').addClass('bg-success');
    //     $('.fc .fc-view .fc-bg tr td[data-date="' + packageResendDate.format('YYYY-MM-DD') + '"]').addClass('border-danger end');
    // };


});



