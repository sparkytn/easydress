<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Front\ShopComponent;
use App\Http\Controllers\Front\HomeController;
use \App\Http\Controllers\Front\Auth\SocialAuth;
use App\Http\Livewire\Front\ProductDetailComponent;
use App\Http\Controllers\Front\Account\MyShopController;
use App\Http\Controllers\Front\Account\BookingController;
use App\Http\Controllers\Front\Account\ProfileController;
use App\Http\Controllers\Front\Account\AdressesController;
use App\Http\Controllers\Front\Account\DashboardController;
use App\Http\Controllers\Front\PageController;
use App\Http\Controllers\Front\CartController;

/*
|--------------------------------------------------------------------------
| Front Routes
|--------------------------------------------------------------------------
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::post('/chercher', [HomeController::class,'filter'])->name('home.filter');

Route::get('/emprunter/tenus/{product}', ProductDetailComponent::class)->name('product.detail');
Route::get('/emprunter/{category?}', ShopComponent::class)->name('shop');
Route::get('/le-concept', [PageController::class, 'leConcept'])->name('le-concept');
Route::get('/faq', [PageController::class, 'faq'])->name('faq');
Route::get('/politique', [PageController::class,'politique'])->name('home.politique');

// Scoial Login
Route::get('/auth/redirect/{driver}', [SocialAuth::class, 'redirect'])->name('social.redirect');
Route::get('/auth/callback/{driver}', [SocialAuth::class, 'callback'])->name('social.callback');
Route::post('/auth/set-password', [SocialAuth::class, 'setPassword'])->name('social.set-password');

// Cart
Route::post('/panier/ajouter', [CartController::class,'add'])->name('addtocart');
Route::post('/panier/supprimer', [CartController::class,'delete'])->name('deletefromcart');
Route::get('/panier', [CartController::class,'show'])->name('showcart');
Route::match(['post','get'],'/panier/checkout',[CartController::class,'checkout'])->middleware(['auth', 'verified'])->name('checkout');
Route::post('/panier/enregistrer',[CartController::class,'save_order'])->middleware(['auth', 'verified'])->name('save_order');

Route::get('/videpanier',[CartController::class,'cart_flush'])->name('cartflush');


Route::group(['as' => 'account.', 'prefix' => 'mon-compte', 'middleware' => ['auth', 'verified']], function () {
    Route::get('/', [ProfileController::class, 'index'])->name('dashboard');

    Route::get('/profile', [ProfileController::class, 'index'])->name('profile');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');

    Route::get('/adresses', [AdressesController::class, 'index'])->name('addresses');
    Route::get('/adresses/ajouter', [AdressesController::class, 'add'])->name('address.add');
    Route::post('/adresses/enregistrer',[AdressesController::class, 'store'])->name('address.store');
    Route::get('/adresses/{id}', [AdressesController::class, 'edit'])->name('address.edit');
    Route::post('/adresses/{id}', [AdressesController::class, 'update'])->name('address.update');
    Route::delete('/adresses/supprimer/{id}', [AdressesController::class, 'destroy'])->name('address.destroy');
    Route::get('/historique-des-achats', [DashboardController::class, 'orderHistory'])->name('order.history');
    Route::get('/moyens-de-paiement', [DashboardController::class, 'paymentMethods'])->name('payment.methods');


    Route::get('/ma-boutique', [MyShopController::class, 'index'])->name('my-shop');
    Route::get('/ma-boutique/ajouter-produit', [MyShopController::class, 'add_product'])->name('my-shop.add_product');
    Route::post('/ma-boutique/enregistrer-produit', [MyShopController::class, 'store_product'])->name('my-shop.store_product');
    Route::get('/ma-boutique/supprimer-produit/{id}', [MyShopController::class, 'delete_product'])->name('my-shop.delete_product');
    Route::get('/ma-boutique/mes-commissions', [MyShopController::class , 'my_commissions'])->name('my-shop.my-commissions');
});
