<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
|  Administrator Routes
|--------------------------------------------------------------------------
*/

Route::multiauth('Administrator', 'administrator',['register'=> false,'confirm'=> false,'verify'=> false,'reset'=> false]);
