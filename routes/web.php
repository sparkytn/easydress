<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});
//
//Route::get('/dashboard', function () {
//    return view('dashboard');
//})->middleware(['auth'])->name('dashboard');


/*
 * Frontend Routes
 */
Route::group(['as' => 'front.'], function () {
    require __DIR__.'/front.php';
});
require __DIR__ . '/auth.front.php';



/*
 * Backend Routes
 *
 * These routes can only be accessed by users with type `admin`
 */
Route::group(['prefix' => 'administrator', 'as' => 'admin.', 'middleware' => 'auth:administrator'], function () {
    require __DIR__.'/administrator.php';
});
require __DIR__ . '/auth.administrator.php';
