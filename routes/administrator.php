<?php

use Illuminate\Support\Facades\Route;
use App\Notifications\BookingStatusNotification;
use App\Notifications\CommissionPaidNotification;
use \App\Http\Controllers\Administrator\BrandsController;
use App\Http\Controllers\Administrator\DeliveryController;
use \App\Http\Controllers\Administrator\BookingsController;
use \App\Http\Controllers\Administrator\ProductsController;
use App\Http\Controllers\Administrator\CustomersController;
use \App\Http\Controllers\Administrator\DashboardController;
use \App\Http\Controllers\Administrator\HelloWordController;
use \App\Http\Controllers\Administrator\AttributesController;
use \App\Http\Controllers\Administrator\CategoriesController;
use App\Http\Controllers\Administrator\CommissionsController;
use \App\Http\Controllers\Administrator\AttributeValuesController;
use App\Http\Controllers\Administrator\SubmitedProductsController;

/*
|--------------------------------------------------------------------------
|  Administrator Routes
|--------------------------------------------------------------------------
*/

Route::get('/dashboard', [DashboardController::class, 'index'])
    ->name('dashboard');
Route::get('/dashboard/calendarfill', [DashboardController::class, 'calendarFill'])
    ->name('dashboard.calendar_fill');
Route::get('/dashboard/calendarfill2', [DashboardController::class, 'calendarFill2'])
    ->name('dashboard.calendar_fill2');

Route::get('/account', [HelloWordController::class, 'index'])
    ->name('account');


Route::resource('categories', CategoriesController::class);
Route::resource('brands', BrandsController::class);
Route::resource('attributes', AttributesController::class);
Route::resource('attributes.attribute_values', AttributeValuesController::class);

/* PRODUCTS*/
Route::match(['post', 'get'], 'products/create_new', [ProductsController::class, 'create_new'])
    ->name('products.create_new');
Route::get('products/load_gallery/{product}', [ProductsController::class, 'load_gallery'])
    ->name('products.load_gallery');
Route::post('products/upload_images/', [ProductsController::class, 'upload_images'])
    ->name('products.upload_images');
Route::get('products/reorder_images/{new_order}', [ProductsController::class, 'reorder_images'])
    ->name('products.reorder_images');
Route::get('products/delete_image/{image_id}/{product_id}', [ProductsController::class, 'delete_image'])
    ->name('products.delete_image');
Route::resource('products', ProductsController::class);
/*///////////////////////// */

/* BOOKINGS */
Route::get('bookings', [BookingsController::class, 'index'])
    ->name('bookings.index');
Route::get('bookings/{booking_id}', [BookingsController::class, 'details'])
    ->name('bookings.details');
Route::post('bookings/change_status', [BookingsController::class, 'change_status'])
    ->name('bookings.change_status');
Route::post('bookings/payment_recieved', [BookingsController::class, 'payment_recieved'])
    ->name('bookings.payment_recieved');
Route::post('bookings/deposit_refunded', [BookingsController::class, 'deposit_refunded'])
    ->name('bookings.deposit_refunded');
/*///////////////////////// */

/* CUSTOMERS */
Route::get('customers', [CustomersController::class, 'index'])
    ->name('customers.index');
Route::get('customers/{customer_id}', [CustomersController::class, 'details'])
    ->name('customers.details');
/*///////////////////////// */

/* SUBMITED PRODUCTS */
Route::get('submitedproducts',[SubmitedProductsController::class,'index'])
    ->name('submitedproducts.index');
Route::get('submitedproducts/{product_id}',[SubmitedProductsController::class,'show'])
    ->name('submitedproducts.show');
Route::post('submitedproducts/accept',[SubmitedProductsController::class,'accept'])
    ->name('submitedproducts.accept');
Route::post('submitedproducts/migrate',[SubmitedProductsController::class,'migrate'])
    ->name('submitedproducts.migrate');
/*///////////////////////// */

/* DELIVERIES */
Route::get('delivery', [DeliveryController::class, 'delivery'])
    ->name('delivery');
Route::get('delivery/export_list',[DeliveryController::class,'export_list'])
    ->name('delivery.export_list');
Route::get('delivery/export_invoices',[DeliveryController::class,'export_invoices'])
    ->name('delivery.export_invoices');
Route::get('delivery/returns', [DeliveryController::class, 'returns'])
    ->name('delivery.returns');
Route::get('delivery/export_return_list', [DeliveryController::class, 'export_returns_list'])
    ->name('delivery.returns.export');
/*//////////////////////////*/

/* COMMISSIONS */
Route::get('commissions', [CommissionsController::class, 'index'])
    ->name('commissions.index');
Route::get('commissions/pay/{commission_id}', [CommissionsController::class, 'pay'])
    ->name('commissions.pay');
// TEST EMAIL NOTIFICATION
Route::get('commissions/test', function () {
        $commission = App\Models\Commission::find(1);
    
        return (new CommissionPaidNotification($commission))
        ->toMail($commission->user);
    });
/*///////////////////////// */

Route::get('booking/test', function () {
    $booking = App\Models\Booking::all()->random(1)->first();;
    $notification = $booking->generateStatusNotification();
    return (new BookingStatusNotification($notification['subject'],$notification['msg']))
    ->toMail($booking->user);
});

