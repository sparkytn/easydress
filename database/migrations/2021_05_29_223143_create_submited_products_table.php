<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubmitedProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submited_products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->on('users')->onDelete('no action');
            $table->string('name');
            $table->foreignId('brand_id')->nullable()->on('brands');
            $table->foreignId('category_id')->on('categories');
            $table->integer('rent_price')->index();
            $table->integer('value_price');
            $table->string('note')->nullable();
            $table->string('img1');
            $table->string('img2')->nullable()->default(NULL);
            $table->string('img3')->nullable()->default(NULL);
            $table->tinyInteger('is_accepted')->default(0);
            $table->integer('migrated')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_submited_products');
    }
}
