<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique('slug');
            $table->foreignId('user_id')->on('users');
            $table->foreignId('brand_id')->nullable()->on('brands');
            $table->foreignId('category_id')->on('categories');
            $table->integer('rent_price')->index();
            $table->integer('value_price');
            $table->tinyText('style_description');
            $table->tinyText('composition_description');
            $table->tinyText('size_description');
            $table->tinyInteger('is_online')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
