<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->foreignId('product_id')->references('id')->on('products');
            $table->foreignId('user_id')->references('id')->on('users');
            $table->foreignId('delivery_address_id')->references('id')->on('delivery_addresses');
            $table->foreignId('status_event_id')->references('id')->on('status_events');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('price');
            $table->integer('deposit');
            $table->tinyInteger('payment_recieved')->default(0);
            $table->text('payment_note');
            $table->tinyInteger('deposit_refunded')->default(0);
            $table->string('note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
