<?php

namespace Database\Factories;

use App\Models\BookingStatusEvent;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookingStatusEventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BookingStatusEvent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'booking_id' => $this->faker->numberBetween(11,20),
            'status_event_id' => $this->faker->numberBetween(1,5),
            'details' => $this->faker->sentence()
        ];
    }
}
