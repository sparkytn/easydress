<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->sentence();
        $price = $this->faker->numberBetween(20,80);

        return [
            'name' => $name,
            'slug' => Str::slug($name),
            'user_id' => $this->faker->numberBetween(1,5),
            'brand_id' => $this->faker->numberBetween(1,7),
            'category_id' => $this->faker->numberBetween(1,6),  
            'rent_price' => $price,
            'value_price' => (int)$price*5,
            'style_description'	=>$this->faker->text(250),
            'composition_description' =>$this->faker->text(250),
            'size_description'=>$this->faker->text(100),
            'is_online'=> $this->faker->numberBetween(0,1)
        ];
    }

 
}
