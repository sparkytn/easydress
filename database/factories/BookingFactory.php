<?php

namespace Database\Factories;

use App\Models\Booking;
use App\Models\StatusEvent;
use DateInterval;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Booking::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $date = $this->faker->dateTimeBetween('-3 days','+5 days');
        $start_date = $date->format('Y-m-d');
        $date->add(new DateInterval('P4D'));
        $end_date = $date->format('Y-m-d');

        return [
            'code' => $this->faker->regexify('[A-Z0-9]{5}'),
            'user_id' => $this->faker->numberBetween(1,5),
            'product_id' => $this->faker->numberBetween(1,20),
            'start_date' => $start_date,
            'end_date' => $end_date,
            'price' => $this->faker->numberBetween(45,95),
            'deposit' => $this->faker->numberBetween(100,250),
            'delivery_address_id' => $this->faker->numberBetween(1,2),
            'payment_recieved' => 1,
            'status_event_id' => '2'
        ];
    }

    public function faker(){
        return $this->faker;
    }
}
