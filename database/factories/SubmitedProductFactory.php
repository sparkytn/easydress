<?php

namespace Database\Factories;

use App\Models\SubmitedProduct;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class SubmitedProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubmitedProduct::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->sentence();
        $price = $this->faker->numberBetween(20,80);

        return [
            'name' => $name,
            'user_id' => $this->faker->numberBetween(1,5),
            'brand_id' => $this->faker->numberBetween(1,7),
            'category_id' => $this->faker->numberBetween(1,6),  
            'rent_price' => $price,
            'value_price' => (int)$price*5,
            'note'	=>$this->faker->text(30),
            'is_accepted'=> $this->faker->numberBetween(0,1),
            'migrated'=> $this->faker->numberBetween(0,1),
            'img1'=>''
        ];
    }


}
