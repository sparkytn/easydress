<?php

namespace Database\Seeders;

use App\Models\StatusEvent;
use Illuminate\Database\Seeder;

class StatusEventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            'En attente de confirmation',
            'Confirmée',
            'Livrée',
            'A retourner',
            'Retournée et terminée',
            'Annulée'
        ];

        foreach($status as $stat){
            StatusEvent::create(['name'=>$stat]);
        }
    }
}
