<?php

namespace Database\Seeders;

use Database\Seeders\AttributeSeeder;
use Database\Seeders\BrandSeeder;
use Database\Seeders\CategorySeeder;
use Database\Seeders\StatusEventSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(5)->create();
        //\App\Models\SubmitedProduct::factory(10)->create();
        $this->call([
            StatusEventSeeder::class,
            BrandSeeder::class,
            CategorySeeder::class,
            AttributeSeeder::class
        ]);
        //\App\Models\Product::factory(30)->create();

        //\App\Models\Booking::factory(20)->create();
        //\App\Models\BookingStatusEvent::factory()->count(30)->create();


    }
}
