<?php

namespace Database\Seeders;

use App\Models\Brand;
use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands =['ARMANI JEANS',
        'CHANEL',
        'DIOR',
        'DKNY',
        'ELIZA & ETHAN',
        'LOUIS VUITTON',
        'MADELEINE & MARIE'];

        foreach($brands as $brand){
            Brand::create(['name'=>$brand]);
        }
    }
}
