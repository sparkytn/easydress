<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $categories =['Robe',
                    'Manteau et veste',
                    'Jupe',
                    'Top et chemise',
                    'Combis et pantalons',
                    'Accessoires'];

            foreach($categories as $category){
                $slug = Str::slug($category);
                Category::create(['name'=>$category,'slug'=>$slug]);
            }

    }
}
